#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

#define RANDOM_NUMBERS 100
#define EMPTY_FILE -5
#define WRONG_ARGS_NUMBER -4
#define FILE_CREATE_ERROR -1
#define FILE_READ_ERROR -2
#define WRONG_CONSOLE_ARGS -3
#define OK 0

void write_down(FILE *const f_b)
{
    // Запись 100 чисел диапазоном от -100 до 100
    srand(time(NULL));
    int number;
    for (int i = 0; i < RANDOM_NUMBERS; i++)
    {
        number = rand() / 201 - 100;
        fwrite(&number, sizeof(number), 1, f_b);  
    }
}

void read_file(FILE *const f_b)
{
    int number;
    while (fread(&number, sizeof(number), 1, f_b) == 1)
        printf("%d\n", number);
}

int get_number_by_pos(FILE *const f_b, const int index)
{
    int number;
    fseek(f_b, index * sizeof(number), SEEK_SET);
    fread(&number, sizeof(number), 1, f_b);
    return number;
}

void set_number_by_pos(FILE *const f_b, const int index, const int value)
{
    fseek(f_b, index * sizeof(value), SEEK_SET);
    fwrite(&value, sizeof(value), 1, f_b);
}

int len_file(FILE *const f_b)
{
    int n = 0, number;
    while (fread(&number, sizeof(number), 1, f_b) == 1)
        n++;
    rewind(f_b);
    return n;
}

void swap(FILE *const f, const int i, const int j)
{
    int temp = get_number_by_pos(f, i);
    int value = get_number_by_pos(f, j);
    set_number_by_pos(f, j, temp);
    set_number_by_pos(f, i, value);
}

void q_sort(const int l, const int r, FILE *const f)
{
    if (l < r)
    {
        int mid = (l + r) / 2;
        int elem = get_number_by_pos(f, mid);
        int first = l;
        int last = r;
        while (first <= last)
        {
            while (get_number_by_pos(f, first) < elem)
                first++;
            while (get_number_by_pos(f, last) > elem)
                last--;
            if (first <= last)
            {
                swap(f, first, last);
                first++;
                last--;
            }
        }
        q_sort(l, last, f);
        q_sort(first, r, f);
    }
}

/*void sort_file(FILE *f_b)
{
    // Пузырек
    int n = len_file(f_b);
    for (int i = 0; i < n - 1; i++)
        for (int j = 0; j < n - 1; j++)
            if (get_number_by_pos(f_b, j) > get_number_by_pos(f_b, j + 1))
            {
                int temp = get_number_by_pos(f_b, j);
                int value = get_number_by_pos(f_b, j + 1);
                set_number_by_pos(f_b, j, value);
                set_number_by_pos(f_b, j + 1, temp);
            }
}*/

int main(const int argc, const char *const *const argv)
{
    FILE *f_b;
    if (argc != 3)
        return WRONG_ARGS_NUMBER;
    if (!strcmp(argv[1], "c"))
    {
        f_b = fopen(argv[2], "wb");
        if (f_b)
        {
            write_down(f_b);
            fclose(f_b);
            return OK;
        }
        else
            return FILE_CREATE_ERROR;
    }
    else if (!strcmp(argv[1], "p"))
    {
        f_b = fopen(argv[2], "rb");
        if (f_b && len_file(f_b))
        {
            read_file(f_b);
            fclose(f_b);
            return OK;
        }
        else if (!f_b)
            return FILE_READ_ERROR;
        else
            return EMPTY_FILE;
    }
    else if (!strcmp(argv[1], "s"))
    {
        f_b = fopen(argv[2], "rb+");
        if (f_b && len_file(f_b))
        {
            int n = len_file(f_b);
            q_sort(0, n - 1, f_b);
            fclose(f_b);
            return OK;
        }
        else if (!f_b)
            return FILE_READ_ERROR;
        else
            return EMPTY_FILE;
    }
    return WRONG_CONSOLE_ARGS;
}
