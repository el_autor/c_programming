#include <stdio.h>

#define N 10
#define ERROR 0
#define UP_LIMIT 10
#define DOWN_LIMIT 0
#define OK 1
#define ZERO 0
#define ALL_OK 0
#define WRONG_INPUT 1

// Модифицированный ввод без указания количества элементов
/*
#include <stdio.h>
#include <math.h>

#define N 10
#define INT_NUMBERS 15
#define WRONG_INPUT 0
#define INPUT_END 1
#define TRANSFORM 48
#define OK 1
	
int proper_element(char mas[], int local)
{
    int negative = 1;
    int number = 0;
    local--; // так как последний символ массива не цифра
    if (mas[0] == '-')
    {
        negative = -1;
        for (int i = 0; i < local - 1; i++)
            mas[i] = mas[i + 1];
        local -= 1;
    }
    for (int i = 0; i < local; i++)
    {
        number += ((mas[i] - TRANSFORM) * pow(10, local - i - 1));
    }
    return number * negative;
}

int parse_elem(int array[], char *temp, int *indicator, int elem_pos)
{
    char mas[INT_NUMBERS];
    mas[0] = *temp;
    int local = 1;
    while ((*temp >= '0' && *temp <= '9') || (*temp == '-' && local == 1))
    {
        *temp = getchar();
        mas[local] = *temp;
        local++;
    }
    if (*temp == '\n')
        (*indicator)++;
    else if (*temp == '-' || (mas[0] == '-' && local == 1))
        return WRONG_INPUT;
    else if (*temp != ' ')
        return WRONG_INPUT;
    array[elem_pos] = proper_element(mas, local);
    return OK;
}

int upgrade_input(int array[], int *elems_number)
{
    char temp;
    int indicator = 0;
    while (*elems_number < N)
    {
        temp = getchar();
        if (temp == '\n')
        {
            indicator++;
            if (indicator == 2)
                return INPUT_END;
            temp = getchar();
            if (temp == '\n')
                return INPUT_END;
            else if ((temp >= '0' && temp <= '9') || temp == '-')
            {
                indicator = 0;
                if (parse_elem(array, &temp, &indicator, *elems_number))
                {
                    (*elems_number)++;
                    continue;
                }
                else
                    return WRONG_INPUT;
            }
            else if (temp != ' ')
                return WRONG_INPUT;
            else
            {
                indicator = 0;
                continue;
            }
        }
        else if ((temp >= '0' && temp <= '9') || temp == '-')
        {
            indicator = 0;
            if (parse_elem(array, &temp, &indicator, *elems_number))
                (*elems_number)++;
            else
                return WRONG_INPUT;
        }
        else if (temp != ' ')
            return WRONG_INPUT;
        else
            continue;
    }
}
*/

typedef const int c_i;
typedef const float c_f;

// Функция ввода статического массива
int array_input(int *const mas, int *const elems_number)
{
    if (scanf("%d", elems_number) == 1 && *elems_number > DOWN_LIMIT && *elems_number <= UP_LIMIT)
    {
        char sep;
        for (int i = 0; i < *elems_number; i++)
            // Если перенос строки идет после кол-ва эл-тов массива или введеный элемент не число
            if ((i == 0 && scanf("%c", &sep) == 1 && sep != ' ') || scanf("%d", &mas[i]) != 1)
                return ERROR;
            // Если введено меньше элементов, чем ожидалось
            else if (i < *elems_number - 1 && scanf("%c", &sep) == 1 && sep == '\n')
                return ERROR;
            // Если введено больше элементов, чем ожидалось
            else if (i == *elems_number - 1 && scanf("%c", &sep) == 1 && sep != '\n')
            {
                while (sep == ' ')
                    scanf("%c", &sep);
                if (sep != '\n')
                    return ERROR;
            }
    }
    else
        return ERROR;
    return OK;
}

// Функция для проверки существования элементов, выше среднего арифметического
int existance(c_i *const array, c_i *const elems_number)
{
    if (*elems_number == 1)
        return ERROR;
    else
    {
        int local = array[0];
        for (int i = 0; i < *elems_number; i++)
            if (local != array[i])
                return OK;
    }
    return ERROR;
}

// Заполнение нового массива, возвращает количество элементов в новом массиве
int fill_array(c_i *const mas, int *const copy_mas, c_i *const elems_number, c_f *const middle)
{
    int k = ZERO;
    for (int i = 0; i < *elems_number; i++)
        if ((float)mas[i] > *middle)
        {
            copy_mas[k] = mas[i];
            k++;
        }
    return k;
}

// Расчет среднего арифметического
float calc_middle(c_i *const mas, c_i *const elems_number)
{
    int sum = ZERO;
    for (int i = 0; i < *elems_number; i++)
        sum += mas[i];
    return sum / (*elems_number);
}

// Функция вывода статического массива
void output_array(c_i *const mas, c_i *const elems_number)
{
    for (int i = 0; i < *elems_number; i++)
        if (i == (*elems_number) - 1)
            printf("%d", mas[i]);
        else
            printf("%d ", mas[i]);
}

int main()
{
    int array[N];
    int elems_number;
    if (array_input(array, &elems_number) && existance(array, &elems_number))
    {
        float middle = calc_middle(array, &elems_number);
        int up_middle_array[elems_number];
        int new_n = fill_array(array, up_middle_array, &elems_number, &middle);
        output_array(up_middle_array, &new_n);
        return ALL_OK;
    }
    else
    {
        puts("Wrong Input!");
        return WRONG_INPUT;
    }
}
