#include <stdio.h>

#define OK 0
#define NO_FILE 1
#define MAX_STR
#define WORD_SIZE 10
#define WORDS_NUMBER 215000000  

void read_matrix(FILE *f_p, int matr[][WORD_SIZE], int *size)
{
    char symb = fgetc(f_p);
    int count = 0;
    while (symb != '\n' || symb != EOF)
    {
        ungetc(f_p, symb);
        if (fscanf(f_p, "%s", matr[count]) == 1)
            count++;
    }
}

void revert_matrix(int matr[][WORD_SIZE])
{
    for (int i = 0; i < ; i++)	
}

int main()
{
    char matrix[WORDS_NUMBER][WORD_SIZE];
    FILE *f_p = fopen("in.txt", "r+");
    if (f_p)
    {
	while (!feof(f_p))
        {
	    int size = 0;	
            read_matrix(f_p, matrix, size);
	    revert_matrix(matrix);
	}
        fclose(f_p);
        return OK;
    }
    else
        return NO_FILE;    
}
