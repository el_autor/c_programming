#include <stdio.h>

#define N 20

void parse_number(int array[], int *size, int number)
{
    while(number > 0)
    {
        array[*size] = number % 10;
        (*size)++;
        number = number / 10;	
    }
}

int check_simple(int local)
{
    if (local == 0 || local == 1)
        return 0;
    for (int i = 2; i < local; i++)
        if (local % i == 0)
            return 0;
    return 1;
}

int main()
{
    int n;
    scanf("%d", &n);
    int size = 0;
    int array[N];
    parse_number(array, &size, n);
    for (int i = size - 1; i >= 1; i--)
    {
        int local = array[i] * 10 + array[i - 1];
        if (check_simple(local))
            printf("%d ", local);
    }
}
