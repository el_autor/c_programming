#ifndef _TESTING_H_

#define _TESTING_H_

#include <stdio.h>
#include <stdlib.h>

#include "type.h"
#include "return_codes.h"
#include "input.h"
#include "resize_actions.h"
#include "free.h"

#define TEST_FILE "check_file.txt"

#define ERRORS_DETECTED 1

#define NORMAL 1
#define ERROR 0

#endif