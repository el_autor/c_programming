#include <stdio.h>
#include <stdlib.h>

#include "resize_actions.h"
#include "input.h"
#include "free.h"

// Вывод сообщение об ошибке
int message(const int code)
{
    switch (code)
    {
        case WRONG_DIMENSION:
            printf("Неправильная размерность матрицы");
            break;
        case WRONG_MATRIX_INPUT:
            printf("Неправильно введены элементы матрицы");
            break;
        case MEMORY_NOT_ALLOCATED:
            printf("Память не была выделена");
            break;
        case WRONG_GRADES:
            printf("Неправильно введены степени");
            break;
    }

    return code;
}

int main()
{
    int a_rows, a_cols = 0, b_rows = 0, b_cols = 0, function_status = 0, result_size = 0;
    int a_grade = 0, b_grade = 0;
    type **first_matr = NULL, **second_matr = NULL, **multi = NULL;

    setbuf(stdin, NULL); // Отключаем буферизацию
    setbuf(stdout, NULL);

    if ((function_status = allocate_input_matrix(stdin, &a_rows, &a_cols, &first_matr)))
    {
        return message(function_status);
    }

    if ((function_status = allocate_input_matrix(stdin, &b_rows, &b_cols, &second_matr))) 
    {
        free_memory(&first_matr, a_rows, a_cols);
        return message(function_status);
    }

    result_size = predict_size(a_rows, a_cols, b_rows, b_cols); // Предсказываем размер маьрицы, которая получится в конце

    if (a_rows != a_cols || a_rows < result_size || a_cols < result_size)
    {
        make_square(&a_rows, &a_cols, result_size, &first_matr); // Делаем матрицу квадратной, согласно условию
    }
    
    if (b_cols != b_rows || b_rows < result_size || b_cols < result_size)
    {
        make_square(&b_rows, &b_cols, result_size, &second_matr);
    }

    add_elems(first_matr, a_rows, a_cols, result_size); // Добавка элементов
    add_elems(second_matr, b_rows, b_cols, result_size);    

    if ((function_status = input_matrix_dimensions(&a_grade, &b_grade)))
    {
        free_memory(&first_matr, result_size, result_size);
        free_memory(&second_matr, result_size, result_size);
        return message(function_status);
    }

    //show_matrix(first_matr, result_size, result_size);
    //show_matrix(second_matr, result_size, result_size);

    get_last_factor(first_matr, result_size, a_grade); // Возведение матрицы в степень

    get_last_factor(second_matr, result_size, b_grade); 

    allocate_matrix_memory(&multi, result_size, result_size);

    get_result_matrix(multi, first_matr, second_matr, result_size); // Результирующая матрица

    show_matrix(multi, result_size, result_size);

    /*
    show_matrix(first_matr, result_size, result_size);
    printf("first\n");
    show_matrix(second_matr, result_size, result_size);
    printf("second\n");
    */

    free_memory(&first_matr, result_size, result_size); 
    free_memory(&second_matr, result_size, result_size);
    free_memory(&multi, result_size, result_size);
    
    return OK;
}
