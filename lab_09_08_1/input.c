#include "input.h"
#include "free.h"

int input_matrix_dimensions(int *const a_dim, int *const b_dim)
{
    if (scanf("%d %d", a_dim, b_dim) == 2 && *a_dim >= 0 && *b_dim >= 0)
    {
        return OK;
    }
    return WRONG_GRADES;
}

int allocate_matrix_memory(type ***const matrix, const int rows, const int cols)
{
    *matrix = malloc(sizeof(type *) * rows);

    if (!*matrix)
    {
        return MEMORY_NOT_ALLOCATED;
    }

    for (type **i = *matrix; i < *matrix + rows; i++)
    {
        *i = malloc(sizeof(type) * cols);
        if (!*i)
        {
            return MEMORY_NOT_ALLOCATED;
        }
    }
    return OK;
}

int allocate_input_matrix(FILE *const stream, int *const rows, int *const cols, type ***const matrix)
{
    if (!(fscanf(stream, "%d %d", rows, cols) == 2))
    {
        return WRONG_DIMENSION;
    }

    if (*cols <= 0 || *rows <= 0)
    {
        return WRONG_DIMENSION;
    }

    if (*matrix == NULL)
    {
        if (allocate_matrix_memory(matrix, *rows, *cols))
        {
            return MEMORY_NOT_ALLOCATED;
        }
    }

    for (type **i = *matrix; i < *matrix + *rows; i++)
    {
        for (type *j = *i; j < *i + *cols; j++)
        {
            if (!fscanf(stream, "%lli", j))
            {
                free_memory(matrix, *rows, *cols);
                return WRONG_MATRIX_INPUT;
            }
        }
    }

    return OK;
}