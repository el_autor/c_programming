#!/bin/zsh

make ../app.exe

for ((i = 1; i <= 13; i++))
do
    ../app.exe < in_$i.txt > output.txt
    if (diff -w out_$i.txt output.txt)
    then 
        echo "Test $i.....PASSED"
    else
        echo "Test $i.....FAIL"
    fi
done
