#include "input.h"

void free_memory(type **const *const matrix, const int rows, const int cols)
{
    for (type **i = *matrix; i < *matrix + rows; i++)
    {
        free(*i);
    }

    free(*matrix);
}