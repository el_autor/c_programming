#include "resize_actions.h"

// Расчет размера конечных квадратных матриц
int predict_size(const int first_rows, const int first_cols, const int second_rows, const int second_cols)
{
    int min_first = (first_rows < first_cols) ? first_rows : first_cols;
    int min_second = (second_rows < second_cols) ? second_rows : second_cols;

    return (min_first > min_second) ? min_first : min_second;      
}

void show_matrix(type **const matrix, const int rows, const int cols)
{
    for (type **i = matrix; i < matrix + rows; i++)
    {
        for (type *j = *i; j < *i + cols; j++)
        {
            printf("%lli ", *j);
        }
        printf("\n");
    }
}

type get_elem(type *const *const second_matr, type *const *const row, const int col, const int size)
{
    long elem = 0;
    int k = 0;

    for (type *i = *row; i < *row + size; i++)
    {
        elem += *i * *(*(second_matr + k) + col);
        k++;
    }

    return elem;
}

void copy_matrix(type **const matrix_to, type **const matrix_from, const int size)
{
    for (type **i_to = matrix_to, **i_from = matrix_from; i_to < matrix_to + size; i_to++, i_from++)
    {
        for (type *j_to = *i_to, *j_from = *i_from; j_to < *i_to + size; j_to++, j_from++)
        {
            *j_to = *j_from;
        }
    }
}

void get_result_matrix(type **const result, type **const first_factor, type **const second_factor, const int size)
{
    for (type **i = result, **k = first_factor; i < result + size; i++, k++)
    {
        for (type *j = *i; j < *i + size; j++)
        {
            *j = get_elem(second_factor, k, j - *i, size);
        }
    }
}

int get_last_factor(type **const matrix, const int size, int grade)
{
    int function_status = 0;
    type **matrix_copy = NULL, **local_result = NULL;

    if ((function_status = allocate_matrix_memory(&matrix_copy, size, size)))
    {
        return function_status;
    }

    if ((function_status = allocate_matrix_memory(&local_result, size, size)))
    {
        return function_status;
    }

    copy_matrix(matrix_copy, matrix, size);

    if (grade == 0)
    {
        for (type **i = matrix; i < matrix + size; i++)
        {
            for (type *j = *i; j < *i + size; j++)
            {
                if (*i + size - j == matrix + size - i)
                {
                    *j = 1;
                }
                else
                {
                    *j = 0;
                }
            }
        }

        free_memory(&matrix_copy, size, size);
        free_memory(&local_result, size, size);
        return OK;
    }

    while (grade != 1)
    {
        get_result_matrix(local_result, matrix, matrix_copy, size);
        copy_matrix(matrix, local_result, size);
        grade--;
    }

    free_memory(&matrix_copy, size, size);
    free_memory(&local_result, size, size);

    return OK;
}

type calc_middle_geom(const type mult, const int grade)
{
    double middle_geom = pow(mult, 1 / (double) grade);

    return (type) middle_geom;
}

void search_min(int *const row_min, int *const col_min, const int rows, const int cols, type **const matrix)
{
    type min = **matrix;

    for (int i = 0; i < cols; i++)
    {
        for (type **j = matrix; j < matrix + rows; j++)
        {
            if (*((*j) + i) <= min)
            {
                min = *((*j) + i);
                *row_min = (int) (j - matrix);
                *col_min = i;
            }
        }
    }
}

void delete_row(type **const *const matrix, const int del_row, const int total_rows)
{
    type *local = *(*matrix + del_row);

    for (type **i = *matrix + del_row; i < *matrix + total_rows - 1; i++)
    {
        *i = *(i + 1);
    }

    *(*matrix + total_rows - 1) = local;
}

// Выделяем память под матрицу конечного размера
void make_result_size(const int result_size, const int rows, const int cols, type ***const matrix)
{
    if (result_size != rows)
    {
        if (rows > result_size)
        {
            for (type **i = *matrix + result_size; i < *matrix + rows; i++)
            {
                free(*i);
            }
        }

        *matrix = realloc(*matrix, (sizeof(type *) * result_size)); // Размер может только увиличиться

        for (type **i = *matrix + rows; i < *matrix + result_size; i++)
        {
            *i = NULL;
        }
    }

    if (result_size != cols)
    {
        for (type **i = *matrix; i < *matrix + result_size; i++)
        {
            if (*i)
            {
                *i = realloc(*i, (sizeof(type) * result_size));
            }
            else
            {
                *i = malloc(sizeof(type) * result_size);
            }            
        }
    }
}

void delete_col(type **const *const matrix, int col_min, const int rows, const int cols)
{
    for (type **i = *matrix; i < *matrix + rows; i++)
    {
        for (type *j = *i + col_min; j < *i + cols - 1; j++)
        {
            *j = *(j + 1);
        }
    }
}

void make_square(int *const rows, int *const cols, const int result_size, type ***const matrix)
{
    int row_min = 0, col_min = 0, start_rows = *rows, start_cols = *cols;

    // Удаление ряда с минимальным элементом
    while (*rows > *cols)
    {
        //printf("here\n");
        search_min(&row_min, &col_min, *rows, *cols, *matrix);
        //printf("rowmin: %d\n", row_min);
        delete_row(matrix, row_min, *rows);
        (*rows)--;
    }

    while (*cols > *rows)
    {
        search_min(&row_min, &col_min, *rows, *cols, *matrix);
        delete_col(matrix, col_min, *rows, *cols);
        (*cols)--;
    }

    make_result_size(result_size, start_rows, start_cols, matrix);   
}

void add_elems(type **const matrix, const int rows, const int cols, const int result_size)
{
    if (rows == cols && result_size == rows)
    {
        return;
    }

    //printf("%d - rows  %d - cols %d - result size\n", rows, cols, result_size);
    // Заполнение новых строк
    for (int i = 0; i < cols; i++)
    {
        type mult = 1L;
        for (type **j = matrix; j < matrix + rows; j++)
        {
            mult *= abs(*((*j) + i));
        }

        type middle_geom = calc_middle_geom(mult, rows);

        for (type **j = matrix + rows; j < matrix + result_size; j++)
        {
            *((*j) + i) = middle_geom;
        }
    }

    for (type **i = matrix; i < matrix + result_size; i++)
    {
        type min = **i;

        for (type *j = *i; j < *i + cols; j++)
        {
            if (*j < min)
            {
                min = *j;
            }
        }

        for (type *j = *i + cols; j < *i + result_size; j++)
        {
            *j = min;
        }        
    }
}
