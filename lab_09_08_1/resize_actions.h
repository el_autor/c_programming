#ifndef _RESIZE_ACTONS_H_

#define _RESIZE_ACTIONS_H_

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "type.h"
#include "input.h"
#include "return_codes.h"
#include "free.h"

void make_square(int *rows, int *cols, int result_size, type ***matrix);
void add_elems(type **matrix, int rows, int cols, int result_size);
void show_matrix(type **const matrix, const int rows, const int cols);
void get_result_matrix(type **const result, type **const first_factor, type **const second_factor, const int size);
int get_last_factor(type **const matrix, const int size, int grade);
int predict_size(const int first_rows, const int first_cols, const int second_rows, const int second_cols);

#endif