#include "testing.h"

int matrix_are_equal(type *const *const matrix_1, type *const *const matrix_2, const int rows, const int cols)
{
    for (type *const *i_1 = matrix_1, *const *i_2 = matrix_2; i_1 < matrix_1 + rows; i_1++, i_2++)
    {
        for (type *j_1 = *i_1, *j_2 = *i_2; j_1 < *i_1 + cols; j_1++, j_2++)
        {
            if (*j_1 != *j_2)
            {
                return ERROR;
            }
        }
    }

    return NORMAL;
}

int fill_file(const char *const filename, const char *const message)
{
    FILE *file = fopen(filename, "w");
    if (file)
    {
        fprintf(file, "%s", message);
        fclose(file);
        return NORMAL;
    }

    return ERROR; 
}

int input_testing()
{
    char errors_counter = 0;
    FILE* input_file = NULL;
    int rows = 0, cols = 0, check_rows = 0, check_cols = 0;

    long long int **matrix;

    long long int **check_matrix;

    matrix = NULL;
    check_matrix = NULL;

    printf("1) input testing:\n");

    // Тест 1
    if (fill_file(TEST_FILE, "5 0"))
    {
        if ((input_file = fopen(TEST_FILE, "r")))
        {
            if (allocate_input_matrix(input_file, &rows, &cols, &matrix) == WRONG_DIMENSION)
            {
                printf("Test 1.....PASSED\n");
            }
            else
            {
                printf("Test 1.....FAIL\n");
                errors_counter++;
            }
            fclose(input_file);
        }
        else
        {
            printf("Test 1.....FAIL\n");
            errors_counter++;
        }
    }
    else
    {
        printf("Test 1.....FAIL\n");
        errors_counter++;
    }

    // Тест 2

    if (fill_file(TEST_FILE, "-a 5"))
    {
        if ((input_file = fopen(TEST_FILE, "r")))
        {
            if (allocate_input_matrix(input_file, &rows, &cols, &matrix) == WRONG_DIMENSION)
            {
                printf("Test 2.....PASSED\n");
            }
            else
            {
                printf("Test 2.....FAIL\n");
                errors_counter++;
            }
            fclose(input_file);
        }
        else
        {
            printf("Test 2.....FAIL\n");
            errors_counter++;
        }
    }
    else
    {
        printf("Test 2.....FAIL\n");
        errors_counter++;
    }

    // Тест 3
    
    check_rows = 2;
    check_cols = 2;

    if (fill_file(TEST_FILE, "2 2\n 1 2 3 4"))
    {
        if ((input_file = fopen(TEST_FILE, "r")))
        {
            if (allocate_input_matrix(input_file, &rows, &cols, &matrix) == OK && allocate_matrix_memory(&check_matrix, rows, cols) == OK)
            {   
                **check_matrix = 1;
                *(*check_matrix + 1) = 2;
                **(check_matrix + 1) = 3;
                *(*(check_matrix + 1) + 1) = 4;

                if (rows == check_rows && cols == check_cols && matrix_are_equal(matrix, check_matrix, rows, cols))
                {
                    printf("Test 3.....PASSED\n");
                }
                else
                {
                    printf("Test 3.....FAIL\n");
                    errors_counter++;
                }

                free_memory(&matrix, rows, cols);
                free_memory(&check_matrix, check_rows, check_cols);
            }
            else
            {
                printf("Test 3.....FAIL\n");
                errors_counter++;
            }
            
            fclose(input_file);
        }
        else
        {
            printf("Test 3.....FAIL\n");
            errors_counter++;
        }
    }
    else
    {
        printf("Test 3.....FAIL\n");
        errors_counter++;
    }
    
    // Тест 4

    matrix = NULL;

    if (fill_file(TEST_FILE, "2 2\n 1 2 4 f"))
    {
        if ((input_file = fopen(TEST_FILE, "r")))
        {
            if (allocate_input_matrix(input_file, &rows, &cols, &matrix) == WRONG_MATRIX_INPUT)
            {
                printf("Test 4.....PASSED\n");
            }
            else
            {
                printf("Test 4.....FAIL\n");
                errors_counter++;
            }

            fclose(input_file);
        }
        else
        {
            printf("Test 4.....FAIL\n");
            errors_counter++;
        }
    }
    else
    {
        printf("Test 4.....FAIL\n");
        errors_counter++;
    }
    
    return errors_counter;
}

// Расчитываем размер выходящей матрицы
int predict_size_testing()
{
    char errors_counter = 0;
    int rows_a = 0, cols_a = 0, rows_b = 0, cols_b = 0;

    printf("2) predict_size testing:\n");

    // Тест 1

    rows_a = 5; cols_a = 5; rows_b = 6; cols_b = 6;

    if (predict_size(rows_a, cols_a, rows_b, cols_b) == rows_b)
    {
        printf("Test 1.....PASSED\n");
    }
    else
    {
        printf("Test 1.....FAIL\n");
        errors_counter++;
    }

    // Тест 2

    rows_a = 4, cols_a = 7, rows_b = 10, cols_b = 9;

    if (predict_size(rows_a, cols_a, rows_b, cols_b))
    {
        printf("Test 2.....PASSED\n");
    }
    else
    {
        printf("Test 2.....FAIL\n");
        errors_counter++;
    }
    
    return errors_counter;
}

int make_square_testing()
{
    long long int **matrix = NULL;
    long long int **check_matrix = NULL;
    FILE* input_file = NULL; 

    char errors_counter = 0;

    int rows = 0, cols = 0, start_rows = 0, start_cols = 0, check_size = 0;

    printf("3) make_square testing:\n");

    // Тест 1

    if (fill_file(TEST_FILE, "2 3\n 1 2 3\n 4 5 6\n 2 2\n 2 3\n 5 6 \n"))
    {
        if ((input_file = fopen(TEST_FILE, "r")) && allocate_input_matrix(input_file, &start_rows, &start_cols, &matrix) == OK)
        {
            rows = start_rows;
            cols = start_cols;

            if (allocate_input_matrix(input_file, &check_size, &check_size, &check_matrix) == OK)
            {
                make_square(&rows, &cols, check_size, &matrix);

                if (rows == check_size && cols == check_size && matrix_are_equal(matrix, check_matrix, check_size, check_size))
                {
                    printf("Test 1.....PASSED\n");
                }
                else
                {
                    printf("Test 1.....FAIL\n");
                    errors_counter++;
                }

                free_memory(&matrix, check_size, check_size);
                free_memory(&check_matrix, check_size, check_size);
            }
            else
            {
                free_memory(&matrix, rows, cols);
                printf("Test 1.....FAIL\n");
                errors_counter++;
            }
        }
        else
        {
            printf("Test 1.....FAIL\n");
            errors_counter++;
        }

        fclose(input_file);
    }
    else
    {
        printf("Test 1.....FAIL\n");
        errors_counter++;
    }

    // Тест 2

    matrix = NULL;
    check_matrix = NULL;

    if (fill_file(TEST_FILE, "3 2\n 2 2\n 3 3\n 4 4\n    2 2\n 3 3\n 4 4\n"))
    {
        if ((input_file = fopen(TEST_FILE, "r")) && allocate_input_matrix(input_file, &start_rows, &start_cols, &matrix) == OK)
        {
            rows = start_rows;
            cols = start_cols;

            if (allocate_input_matrix(input_file, &check_size, &check_size, &check_matrix) == OK)
            {
                make_square(&rows, &cols, check_size, &matrix);

                if (rows == check_size && cols == check_size && matrix_are_equal(matrix, check_matrix, check_size, check_size))
                {
                    printf("Test 2.....PASSED\n");
                }
                else
                {
                    printf("Test 2.....FAIL\n");
                    errors_counter++;
                }
                
                free_memory(&matrix, check_size, check_size);
                free_memory(&check_matrix, check_size, check_size);
            }
            else
            {
                free_memory(&matrix, rows, cols);
                printf("Test 2.....FAIL\n");
                errors_counter++;
            }
        }
        else
        {
            printf("Test 2.....FAIL\n");
            errors_counter++;
        }

        fclose(input_file);
    }
    else
    {
        printf("Test 1.....FAIL\n");
        errors_counter++;
    }

    
    // Тест 3

    matrix = NULL;
    check_matrix = NULL;

    if (fill_file(TEST_FILE, "4 2\n 1 1\n 2 2\n 3 3\n 4 4\n    2 2\n 3 3\n 4 4\n"))
    {
        if ((input_file = fopen(TEST_FILE, "r")) && allocate_input_matrix(input_file, &start_rows, &start_cols, &matrix) == OK)
        {
            rows = start_rows;
            cols = start_cols;

            if (allocate_input_matrix(input_file, &check_size, &check_size, &check_matrix) == OK)
            {
                make_square(&rows, &cols, check_size, &matrix);

                if (rows == check_size && cols == check_size && matrix_are_equal(matrix, check_matrix, check_size, check_size))
                {
                    printf("Test 3.....PASSED\n");
                }
                else
                {
                    printf("Test 3.....FAIL\n");
                    errors_counter++;
                }

                free_memory(&matrix, check_size, check_size);
                free_memory(&check_matrix, check_size, check_size);
            }
            else
            {
                free_memory(&matrix, rows, cols);
                printf("Test 3.....FAIL\n");
                errors_counter++;
            }
        }
        else
        {
            printf("Test 3.....FAIL\n");
            errors_counter++;
        }

        fclose(input_file);
    }
    else
    {
        printf("Test 3.....FAIL\n");
        errors_counter++;
    }

    // Тест 4

    matrix = NULL;
    check_matrix = NULL;

    if (fill_file(TEST_FILE, "4 2\n 1 1\n 2 1\n 3 1\n 4 1\n    2 2\n 1 1\n 2 1\n"))
    {
        if ((input_file = fopen(TEST_FILE, "r")) && allocate_input_matrix(input_file, &start_rows, &start_cols, &matrix) == OK)
        {
            rows = start_rows;
            cols = start_cols;

            if (allocate_input_matrix(input_file, &check_size, &check_size, &check_matrix) == OK)
            {
                make_square(&rows, &cols, check_size, &matrix);

                if (rows == check_size && cols == check_size && matrix_are_equal(matrix, check_matrix, check_size, check_size))
                {
                    printf("Test 4.....PASSED\n");
                }
                else
                {
                    printf("Test 4.....FAIL\n");
                    errors_counter++;
                }

                free_memory(&matrix, check_size, check_size);
                free_memory(&check_matrix, check_size, check_size);
            }
            else
            {
                free_memory(&matrix, rows, cols);
                printf("Test 4.....FAIL\n");
                errors_counter++;
            }
        }
        else
        {
            printf("Test 4.....FAIL\n");
            errors_counter++;
        }

        fclose(input_file);
    }
    else
    {
        printf("Test 4.....FAIL\n");
        errors_counter++;
    }

    // Тест 5

    matrix = NULL;
    check_matrix = NULL;

    if (fill_file(TEST_FILE, "2 3\n 2 2 1\n 2 1 2\n   2 2\n 2 2\n 2 1\n"))
    {
        if ((input_file = fopen(TEST_FILE, "r")) && allocate_input_matrix(input_file, &start_rows, &start_cols, &matrix) == OK)
        {
            rows = start_rows;
            cols = start_cols;

            if (allocate_input_matrix(input_file, &check_size, &check_size, &check_matrix) == OK)
            {
                make_square(&rows, &cols, check_size, &matrix);

                if (rows == check_size && cols == check_size && matrix_are_equal(matrix, check_matrix, check_size, check_size))
                {
                    printf("Test 5.....PASSED\n");
                }
                else
                {
                    printf("Test 5.....FAIL\n");
                    errors_counter++;
                }

                free_memory(&check_matrix, check_size, check_size);
            }
            else
            {
                free_memory(&matrix, rows, cols);
                printf("Test 5.....FAIL\n");
                errors_counter++;
            }

            free_memory(&matrix, check_size, check_size);
        }
        else
        {
            printf("Test 5.....FAIL\n");
            errors_counter++;
        }

        fclose(input_file);
    }
    else
    {
        printf("Test 5.....FAIL\n");
        errors_counter++;
    }

    return errors_counter;
}

int add_elems_testing()
{
    char errors_counter = 0;
    int full_size = 0, cut_size = 0;
    FILE* input_file = NULL;
    long long int **matrix = NULL;
    long long int **check_matrix = NULL;

    printf("4) add_elems testing\n");

    // Тест 1

    full_size = 4;

    if (fill_file(TEST_FILE, "2 2\n 1 1\n 2 2\n 4 4\n 1 1 1 1\n 2 2 2 2\n 1 1 1 1\n 1 1 1 1\n"))
    {
        if ((input_file = fopen(TEST_FILE, "r")) && allocate_matrix_memory(&matrix, full_size, full_size) == OK)
        {
            if (allocate_input_matrix(input_file, &cut_size, &cut_size, &matrix) == OK && 
                allocate_input_matrix(input_file, &full_size, &full_size, &check_matrix) == OK)
            {
                add_elems(matrix, cut_size, cut_size, full_size);
                if (matrix_are_equal(matrix, check_matrix, full_size, full_size))
                {
                    printf("Test 1.....PASSED\n");
                }
                else
                {
                    printf("Test 1.....FAIL\n");
                    errors_counter++;
                }

                free_memory(&check_matrix, full_size, full_size);
            }
            else
            {
                printf("Test 1.....FAIL\n");
                errors_counter++;
            }

            free_memory(&matrix, full_size, full_size);
            fclose(input_file);
        }
        else
        {
            printf("Test 1.....FAIL\n");
            errors_counter++;
        }
    }
    else
    {
        printf("Test 1.....FAIL\n");
        errors_counter++;
    }

    // Тест 2

    matrix = NULL;
    check_matrix = NULL;

    if (fill_file(TEST_FILE, "2 2\n -1 -1\n -2 -2\n 4 4\n -1 -1 -1 -1\n -2 -2 -2 -2\n 1 1 1 1\n 1 1 1 1\n"))
    {
        if ((input_file = fopen(TEST_FILE, "r")) && allocate_matrix_memory(&matrix, full_size, full_size) == OK)
        {
            if (allocate_input_matrix(input_file, &cut_size, &cut_size, &matrix) == OK && 
                allocate_input_matrix(input_file, &full_size, &full_size, &check_matrix) == OK)
            {
                add_elems(matrix, cut_size, cut_size, full_size);
                if (matrix_are_equal(matrix, check_matrix, full_size, full_size))
                {
                    printf("Test 2.....PASSED\n");
                }
                else
                {
                    printf("Test 2.....FAIL\n");
                    errors_counter++;
                }

                free_memory(&check_matrix, full_size, full_size);
            }
            else
            {
                printf("Test 2.....FAIL\n");
                errors_counter++;
            }

            free_memory(&matrix, full_size, full_size);
            fclose(input_file);
        }
        else
        {
            printf("Test 2.....FAIL\n");
            errors_counter++;
        }
    }
    else
    {
        printf("Test 2.....FAIL\n");
        errors_counter++;
    }

    return errors_counter;
}

int get_last_factor_testing()
{
    char errors_counter = 0;

    long long int **matrix = NULL;
    long long int **result_matrix = NULL;

    FILE* input_file = NULL;
    int size = 0, grade = 0;

    printf("5) get last factor testing:\n");

    // Тест 1

    grade = 2;

    if (fill_file(TEST_FILE, "2 2\n 1 1 1 1 2 2\n 2 2 2 2"))
    {
        if ((input_file = fopen(TEST_FILE, "r")))
        {
            if (allocate_input_matrix(input_file, &size, &size, &matrix) == OK && allocate_input_matrix(input_file, &size, &size, &result_matrix) == OK)
            {
                get_last_factor(matrix, size, grade);
                if (matrix_are_equal(matrix, result_matrix, size, size))
                {
                    printf("Test 1.....PASSED\n");
                }
                else
                {
                    printf("Test 1.....FAIL\n");
                    errors_counter++;
                }
                
                free_memory(&matrix, size, size);
                free_memory(&result_matrix, size, size);
            }
            else
            {
                printf("Test 1.....FAIL\n");
                errors_counter++;
            }
        }
        else
        {
            printf("Test 1.....FAIL\n");
            errors_counter++;
        }

        fclose(input_file);
    }
    else
    {
        printf("Test 1.....FAIL\n");
        errors_counter++;
    }

    // Тест 2

    matrix = NULL;
    result_matrix = NULL;
    grade = 1;

    if (fill_file(TEST_FILE, "2 2\n 1 1 1 1 2 2\n 1 1 1 1"))
    {
        if ((input_file = fopen(TEST_FILE, "r")))
        {
            if (allocate_input_matrix(input_file, &size, &size, &matrix) == OK && allocate_input_matrix(input_file, &size, &size, &result_matrix) == OK)
            {
                get_last_factor(matrix, size, grade);
                if (matrix_are_equal(matrix, result_matrix, size, size))
                {
                    printf("Test 2.....PASSED\n");
                }
                else
                {
                    printf("Test 2.....FAIL\n");
                    errors_counter++;
                }
                
                free_memory(&matrix, size, size);
                free_memory(&result_matrix, size, size);
            }
            else
            {
                printf("Test 2.....FAIL\n");
                errors_counter++;
            }
        }
        else
        {
            printf("Test 2.....FAIL\n");
            errors_counter++;
        }

        fclose(input_file);
    }
    else
    {
        printf("Test 2.....FAIL\n");
        errors_counter++;
    }

    // Тест 3

    matrix = NULL;
    result_matrix = NULL;
    grade = 5;

    if (fill_file(TEST_FILE, "2 2\n 1 1 1 1 2 2\n 16 16 16 16"))
    {
        if ((input_file = fopen(TEST_FILE, "r")))
        {
            if (allocate_input_matrix(input_file, &size, &size, &matrix) == OK && allocate_input_matrix(input_file, &size, &size, &result_matrix) == OK)
            {
                get_last_factor(matrix, size, grade);
                if (matrix_are_equal(matrix, result_matrix, size, size))
                {
                    printf("Test 3.....PASSED\n");
                }
                else
                {
                    printf("Test 3.....FAIL\n");
                    errors_counter++;
                }
                
                free_memory(&matrix, size, size);
                free_memory(&result_matrix, size, size);
            }
            else
            {
                printf("Test 3.....FAIL\n");
                errors_counter++;
            }
        }
        else
        {
            printf("Test 3.....FAIL\n");
            errors_counter++;
        }

        fclose(input_file);
    }
    else
    {
        printf("Test 3.....FAIL\n");
        errors_counter++;
    }

    return errors_counter;
}

int main()
{
    int errors_flag = 0;

    errors_flag += input_testing();
    errors_flag += predict_size_testing();
    errors_flag += make_square_testing();
    errors_flag += add_elems_testing();
    errors_flag += get_last_factor_testing();

    if (errors_flag)
    {
        printf("%d - errors detected\n", errors_flag);
        return ERRORS_DETECTED;
    }
    
    printf("Success\n");
    return OK;
}