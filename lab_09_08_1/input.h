#ifndef _INPUT_H_

#define _INPUT_H_

#include <stdio.h>
#include <stdlib.h>

#include "type.h"
#include "return_codes.h"

int allocate_input_matrix(FILE *const stream, int *const cols, int *const rows, type ***const matrix);
int input_matrix_dimensions(int *const a_dim, int *const b_dim);
int allocate_matrix_memory(type ***const matrix, const int rows, const int cols);

#endif