#ifndef _RETURN_CODES_H_

#define _RETURN_CODES_H

#define WRONG_DIMENSION 1
#define WRONG_MATRIX_INPUT 2
#define MEMORY_NOT_ALLOCATED 3
#define WRONG_GRADES 4
#define OK 0

#endif