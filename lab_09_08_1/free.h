#ifndef _FREE_H_

#define _FREE_H_

#include <stdlib.h>
#include <stdio.h>

#include "type.h"

void free_memory(type **const *const matrix, const int rows, const int cols);

#endif 