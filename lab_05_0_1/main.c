#include <stdio.h>

#define N 10
#define WRONG_INPUT 1
#define OK 0
#define ERROR 0
#define CORRECT 1

typedef const int c_i;
typedef int matr[N][N];

int input_array(int arr[][N], c_i *const rows, c_i *const cols)
{
    if (*rows > 10 || *cols > 10 || *cols <= 0 || *rows <= 0)
        return ERROR;
    else
    {
        int inputed = 0;
        char symb;
        for (int i = 0; i < *rows; i++)
            for (int j = 0; j < *cols; j++)
            {
                if (scanf("%d", &arr[i][j]))
                    inputed++;
                else
                    return ERROR;
                scanf("%c", &symb);
                if (inputed < (*rows) * (*cols) && inputed % (*cols) == 0 && symb == '\n')
                    continue;
                else if (inputed < (*rows) * (*cols) && symb == '\n')
                    return ERROR;
            }
        return CORRECT;
    }
}

void fill_new(matr arr, c_i *const rows, c_i *const cols, int *const new)
{
    for (int j = 0; j < *cols; j++)
        if (*rows > 1)
        {
            for (int i = 0; i < (*rows) - 1; i++)
            {
                if (arr[i][j] <= arr[i + 1][j])
                {
                    new[j] = 0;
                    break;
                }
                new[j] = 1;
            }
        }
        else
            new[j] = 0;
}

void array_output(c_i *const array, c_i *const cols)
{
    for (int i = 0; i < *cols; i++)
        printf("%d ", array[i]);
    printf("\n");
}

int main()
{
    int array[N][N];
    int rows, cols;
    if (scanf("%d %d", &rows, &cols) == 2 && input_array(array, &rows, &cols))
    {
        int new_array[cols];
        fill_new(array, &rows, &cols, new_array);
        array_output(new_array, &cols);
    }
    else
        return WRONG_INPUT;
    return OK;
}
