set number
syntax on
set tabstop=4
set backspace=indent,eol,start
set autoindent
set ruler
set mousehide
set shiftwidth=4
set expandtab
set smartindent
