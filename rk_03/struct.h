#ifndef _STRUCT_H_

#define _STRUCT_H_

typedef struct
{
    char **uniq_words;
    size_t size;
} uniq_t;

typedef struct
{
    char **sentence;
    size_t size;
    int *tags;
} sentence_t;

#endif