#include "app.h"

int main(int argc, char **argv)
{
    uniq_t uniq = { NULL, 0 };
    sentence_t *sentences = NULL;
    int sentences_memory = 0, sentences_inputed = 0;

    get_uniq(argv[1], &uniq);

    make_uniq(&uniq);

    bubble_sort(&uniq);

    sentences = input_text(argv[1], &sentences_inputed, &sentences_memory, uniq);

    file_output(argv[2], sentences , sentences_inputed, uniq.size);

    get_closest(argv[2], sentences, sentences_inputed, uniq.size);

    return OK;
}