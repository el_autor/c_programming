#ifndef _ACTIONS_H_

#define _ACTIONS_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "struct.h"
#include "app.h"

void get_uniq(char *filename, uniq_t *uniq);
void make_uniq(uniq_t *uniq);
sentence_t *input_text(char *filename, int *sentences_inputed, int *sentences_memory, uniq_t uniq_struct);
void file_output(char *filename, sentence_t *sentences, int sentences_size, int uniq_size);
void closest(char *filename, sentence_t *sentences, int sentences_size);
void bubble_sort(uniq_t *uniq);
void get_closest(char *filename, sentence_t *sentences, int total_sentences, int binary_matr_size);

#endif