#include "actions.h"

char *get_word(FILE *stream)
{
    char *word = malloc(sizeof(char));
    int cur_size = 1, inputed_symbs = 0;
    char symb = 0;

    if ((symb = fgetc(stream)) == EOF || symb == '\n')
    {
        free(word);
        return NULL;
    }
    else
    {
        ungetc(symb, stream);
    }

    while ((symb = fgetc(stream)) != ' ' && symb != '\n' && symb != EOF)
    {
        if (inputed_symbs == cur_size - 1)
        {
            word = realloc(word, sizeof(char) * cur_size * 2);
            cur_size *= 2;
        }

        word[inputed_symbs] = symb;
        inputed_symbs++;
    }

    word[inputed_symbs] = '\0';
    return word;
}

void add_word(char *word, uniq_t *uniq)
{
    uniq->uniq_words[uniq->size] = word;
    (uniq->size)++;
}

void get_uniq(char *filename, uniq_t *uniq)
{
    FILE *stream = fopen(filename, "r");
    uniq->uniq_words = malloc(sizeof(char *));
    int cur_size = 1;
    char *word = NULL;

    while ((word = get_word(stream)))
    {
        if (cur_size == uniq->size)
        {
            uniq->uniq_words = realloc(uniq->uniq_words, sizeof(char *) * cur_size * 2);
            cur_size *= 2;
        }

        add_word(word, uniq);
    }

    fclose(stream);
}

void remove_repeats(uniq_t *uniq, int index)
{
    for (int i = index; i < uniq->size - 1; i++)
    {
        uniq->uniq_words[i] = uniq->uniq_words[i + 1];
    }

    (uniq->size)--;
}

void make_uniq(uniq_t *uniq)
{
    int repeats = 0;

    for (int i = 0; i < uniq->size; i++)
    {
        repeats = 0;

        for (int j = 0; j < uniq->size; j++)
        {
            if (i != j && !strcmp(uniq->uniq_words[i], uniq->uniq_words[j]))
            {
                remove_repeats(uniq, j);
                repeats++;
                j--;
            }            
        }

        if (repeats)
        {
            remove_repeats(uniq, i);
            i--;
        }
    }
}

char *get_word_line(FILE *stream)
{
    char *word = malloc(sizeof(char));
    int cur_size = 1, inputed_symbs = 0;
    char symb = 0;

    while ((symb = fgetc(stream)) == ' ');

    if (symb == '\n' || symb == EOF)
    {
        free(word);
        return NULL;
    }
    else
    {
        ungetc(symb, stream);
    }

    while ((symb = fgetc(stream)) != ' ' && symb != '\n' && symb != EOF)
    {
        if (inputed_symbs == cur_size - 1)
        {
            word = realloc(word, sizeof(char) * cur_size * 2);
            cur_size *= 2;
        }

        word[inputed_symbs] = symb;
        inputed_symbs++;
    }

    ungetc(symb, stream);
    word[inputed_symbs] = '\0';
    return word;
}

char **get_sentence(FILE *stream, int *sentence_size)
{
    char **sentence = malloc(sizeof(char *));
    int alloced_size = 1;
    char *word = NULL;

    while ((word = get_word_line(stream)))
    {
        if (alloced_size == (*sentence_size))
        {
            sentence = realloc(sentence, sizeof(char *) * alloced_size * 2);
            alloced_size *= 2;
        }

        sentence[(*sentence_size)] = word;
        (*sentence_size)++;

        //printf("%s ", word);
    }
    //printf("\n");

    return sentence;
}

int *get_tags(char **sentence, int sentence_size, uniq_t uniq_words)
{
    int *tags = malloc(sizeof(int) * uniq_words.size);

    for (int i = 0; i < uniq_words.size; i++)
    {
        for (int j = 0; j < sentence_size; j++)
        {
            if (!strcmp(sentence[j], uniq_words.uniq_words[i]))
            {
                tags[i] = 1;
                break;
            }

            tags[i] = 0;
        }
    }

    return tags;
}

sentence_t *input_text(char *filename, int *sentences_inputed, int *sentences_memory, uniq_t uniq_struct)
{
    sentence_t *sentences = malloc(sizeof(sentence_t));
    FILE *stream = fopen(filename, "r");
    (*sentences_inputed) = 0;
    (*sentences_memory) = 1;
    char **sentence = NULL;
    int sentence_size = 0;
    char symb = 0;

    while ((sentence = get_sentence(stream, &sentence_size)))
    {
        if (*sentences_inputed == *sentences_memory)
        {
            sentences = realloc(sentences, sizeof(sentence_t) * (*sentences_memory) * 2);
            (*sentences_memory) *= 2;
        }

        sentences[(*sentences_inputed)].sentence = sentence;
        sentences[(*sentences_inputed)].tags = get_tags(sentence, sentence_size, uniq_struct);
        sentences[(*sentences_inputed)].size = sentence_size;
        (*sentences_inputed)++;
        sentence_size = 0;

        if ((symb = fgetc(stream)) == EOF)
        {
            break;
        }
        else
        {
            ungetc(symb, stream);
        }
    }

    fclose(stream);
    return sentences;
}

void file_output(char *filename, sentence_t *sentences, int sentences_size, int uniq_size)
{
    FILE *stream = fopen(filename, "w");

    for (int i = 0; i < sentences_size; i++)
    {
        for (int j = 0; j < uniq_size; j++)
        {
            fprintf(stream, "%d ", sentences[i].tags[j]);
        }

        fprintf(stream, "\n");
    }

    fclose(stream);
}

void bubble_sort(uniq_t *uniq)
{
    char *temp = NULL;

    for (int i = 0; i < uniq->size - 1; i++)
    {
        for (int j = 0; j < uniq->size - 1; j++)
        {
            if (strcmp(uniq->uniq_words[j], uniq->uniq_words[j + 1]) >= 0)
            {
                temp = uniq->uniq_words[j];
                uniq->uniq_words[j] = uniq->uniq_words[j + 1];
                uniq->uniq_words[j + 1] = temp;
            }
        }
    }
}

int get_scalar(int *i_row, int *j_row, int size)
{
    int scalar = 0;

    for (int i = 0; i < size; i++)
    {
        scalar += i_row[i] * j_row[i];  
    }

    return scalar;
}

void get_closest(char *filename, sentence_t *sentences, int total_sentences, int binary_matr_size)
{
    FILE *stream = fopen(filename, "a");
    int max = 0, first_index = 0, second_index = 1, local_weight = 0;

    for (int i = 0; i < total_sentences; i++)
    {
        for (int j = i + 1; j < total_sentences; j++)
        {
            local_weight = get_scalar(sentences[i].tags, sentences[j].tags, binary_matr_size);

            if (local_weight > max)
            {
                max = local_weight;
                first_index = i;
                second_index = j;
            }
        }
    }

    for (int i = 0; i < sentences[first_index].size; i++)
    {
        fprintf(stream, "%s ", sentences[first_index].sentence[i]);
    }

    fprintf(stream, "\n");

    for (int i = 0; i < sentences[second_index].size; i++)
    {
        fprintf(stream, "%s ", sentences[second_index].sentence[i]);
    }

    fprintf(stream, "\n");

    fclose(stream);
}