#include <stdio.h>

#define NOT_ENOUGH_DATA -1
#define OK 0

int process(FILE *const fp, int *const max_1, int *const max_2)
{
    int number;
    if (fscanf(fp, "%d %d", max_1, max_2) == 2)
        while (fscanf(fp, "%d", &number) == 1)
        {
            if (number >= *max_1)
            {
                *max_2 = *max_1;
                *max_1 = number;
            }
            else if (number > *max_2)
                *max_2 = number;
        }
    else
        return NOT_ENOUGH_DATA;
    return OK;
}

int main()
{
    int max_1, max_2;
    int value = process(stdin, &max_1, &max_2);
    if (!value)
        printf("%d %d", max_1, max_2);    
    return value;
}
