#include <stdio.h>
#define N 10
#define UP_LIMIT 10
#define DOWN_LIMIT 0
#define ERROR 0
#define OK 1
#define ALL_OK 0
#define WRONG_INPUT 1

typedef const int c_i;

// Функция ввода статического массива
int array_input(int *const mas, int *const elems_number)
{
    if (scanf("%d", elems_number) == 1 && *elems_number > DOWN_LIMIT && *elems_number <= UP_LIMIT)
    {
        char sep;
        for (int i = 0; i < *elems_number; i++)
            // Если перенос строки идет после кол-ва эл-тов массива или введеный элемент не число
            if ((i == 0 && scanf("%c", &sep) == 1 && sep != ' ') || scanf("%d", &mas[i]) != 1)
                return ERROR;
            // Если введено меньше элементов, чем ожидалось
            else if (i < *elems_number - 1 && scanf("%c", &sep) == 1 && sep == '\n')
                return ERROR;
            // Если введено больше элементов, чем ожидалось
            else if (i == *elems_number - 1 && scanf("%c", &sep) == 1 && sep != '\n')
            {
                while (sep == ' ')
                    scanf("%c", &sep);
                if (sep != '\n')
                    return ERROR;
            }
    }
    else
        return ERROR;
    return OK;
}


// Замена двух элементов
void replace(int *const first, int *const second)
{
    int temp = *first;
    *first = *second;
    *second = temp;
}

// Функция пузырьковой сортировки
void bubble_sort(int *const mas, c_i *const elems_number)
{
    for (int i = 0; i < (*elems_number) - 1; i++)
        for (int j = 0; j < (*elems_number) - 1; j++)
            if (mas[j] > mas[j + 1])
                replace(&mas[j], &mas[j + 1]);
}

// Вывод массива
void output_array(c_i *const mas, c_i *const elems_number)
{
    for (int i = 0; i < (*elems_number); i++)
        if (i == (*elems_number) - 1)
            printf("%d", mas[i]);
        else
            printf("%d ", mas[i]);
}

int main()
{
    int array[N];
    int elems_number;
    if (array_input(array, &elems_number))
    {
        bubble_sort(array, &elems_number);
        output_array(array, &elems_number);
        return ALL_OK;
    }
    else
    {
        puts("Wrong Input!");
        return WRONG_INPUT;
    }
}
