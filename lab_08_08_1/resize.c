#include "resize.h"

double *resize_array(double *const arr, double *const endian, const int delta)
{
    double *array = NULL;
    size_t len = endian - arr;

    array = (double *) realloc(arr, (size_t) (len + delta) * sizeof(double));

    if (!array)
    {
        return NULL;
    }

    return array;
}
