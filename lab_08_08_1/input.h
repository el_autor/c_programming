#ifndef _INPUT_H_

#define _INPUT_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int my_errno;

double *array_alloc(FILE *const input, const int n);
bool array_input(FILE *const input, double *const memory, const int size);
double *program_input(int *const n, int *const pos, FILE *const input);

#endif
