#include "input.h"
#include "errors.h"

bool array_input(FILE *const input, double *const memory, const int size)
{
    for (double *i = memory; i < memory + size; i++)
    {
        if (fscanf(input, "%lf", i) != 1)
        {
            return false;
        }
    }

    return true;
}

double *array_alloc(FILE *const input, const int n)
{
    double *memory = NULL;

    memory = (double *) malloc(sizeof(double) * n); // Выделение памяти под массив

    if (!memory)
    {
        return NULL;
    }

    return memory;
}

double *program_input(int *const n, int *const pos, FILE *const input)
{
    double *array = NULL;

    if (fscanf(input, "%d", n) != 1 || *n < 2)
    {
        my_errno = WRONG_INPUT;
        return NULL;
    }

    if (!(array = array_alloc(input, *n)) != OK)
    {
        my_errno = MEMORY_ALLOC_ERROR;
        return NULL;
    }

    if (!array_input(input, array, *n))
    {
        free(array);
        my_errno = WRONG_INPUT;
        return NULL;
    }

    if (fscanf(input, "%d", pos) != 1 || *pos > *n || *pos < 0)
    {
        free(array);
        my_errno = WRONG_INPUT;
        return NULL;
    }

    return array;
}