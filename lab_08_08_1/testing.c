#include "testing.h"

#define TEST_FILE "check_input.txt"
#define INPUT_LEN 30

#define EPS 0.000001

#define OK 0
#define ERR_DETECTED 1

#define NORMAL 1
#define ERROR 0

bool arrays_are_equal(const double *const arr_1, const double *const arr_2, const int size)
{
    for (int i = 0; i < size; i++)
    {
        if (fabs(arr_1[i] - arr_2[i]) > EPS)
        {
            printf("%lf \n", arr_1[i] - arr_2[i]);
            return false;
        }
    }

    return true;
}

int fill_file(const char *const filename, const char *const message)
{
    FILE *file = fopen(filename, "w");
    if (file)
    {
        fprintf(file, "%s", message);
        fclose(file);
        return NORMAL;
    }

    return ERROR; 
}

char input_testing()
{
    int n, pos;
    double *arr = NULL;
    char flag = 0;
    FILE *input_file;

    printf("1) Input testing\n");

    // Tест 1

    if (fill_file(TEST_FILE, "1"))
    {
        if ((input_file = fopen(TEST_FILE, "r")))
        {
            if ((arr = program_input(&n, &pos, input_file)))
            {
                flag++;
                printf("Test 1.....FAIL\n");
            }
            else
            {
                printf("Test 1.....OK\n");
            }

            fclose(input_file);
        }
        else
        {
            return ERR_DETECTED;
        }
    }
    else
    {
        return ERR_DETECTED;
    }

    // Тест 2

    if (fill_file(TEST_FILE, "-5"))
    {
        if ((input_file = fopen(TEST_FILE, "r")))
        {
            if ((arr = program_input(&n, &pos, input_file)))
            {
                flag++;
                printf("Test 2.....FAIL\n");
            }
            else
            {
                printf("Test 2.....OK\n");
            }

            fclose(input_file);
        }
        else
        {
            return ERR_DETECTED;
        }
    }
    else
    {
        return ERR_DETECTED;
    }
    
    // Тест 3

    if (fill_file(TEST_FILE, "5 5.0 7.1 4.3 3.2 2.8 1"))
    {
        if ((input_file = fopen("check_input.txt", "r")))
        {
            double check[] = { 5.0, 7.1, 4.3, 3.2, 2.8 };

            if ((arr = program_input(&n, &pos, input_file)))
            {
                if (arrays_are_equal(arr, check, 5) && n == 5 && pos == 1)
                {
                    printf("Test 3.....OK\n");
                }
            }
            else
            {
                flag++;
                printf("Test 3.....FAIL\n");
            }

            fclose(input_file);
            free(arr);
        }
        else
        {
            return ERR_DETECTED;
        }
    }
    else
    {
        return ERR_DETECTED;    
    }
    
    return flag;
}

double *scan_arr(const char *const filename, const int size)
{
    FILE *file = fopen(filename, "r");
    double *arr = NULL;

    if (file)
    {
        if ((arr = malloc(sizeof(double) * size)))
        {
            for (int i = 0; i < size; i++)
            {
                fscanf(file, "%lf", arr + i);
            }
        }
        else
        {
            fclose(file);
            return NULL;
        }
            
        fclose(file);
    }
    else
    {
        return NULL;;
    }
    
    return arr;
}

char calc_u1_testing()
{
    char flag = 0;
    int size;
    double *arr = NULL;

    printf("2) Calculate u1 testing\n");

    // Тест 1

    size = 5;

    if (fill_file(TEST_FILE, "3 2 4 6 2"))
    {
        if ((arr = scan_arr(TEST_FILE, size)))
        {
            if (fabs(calc_u1(arr, arr + size) - 3.509091) <= EPS)
            {
                printf("Test 1.....OK\n");
            }
            else
            {
                flag++;
                printf("Test 1.....FAIL\n");
            }

            free(arr);
        }
        else
        {
            return ERR_DETECTED;
        }
    }    
    else
    {
        return ERR_DETECTED;
    }
    
    // Тест 2

    if (fill_file(TEST_FILE, "1 1 1 1 1"))
    {
        if ((arr = scan_arr(TEST_FILE, size)))
        {
            if (fabs(calc_u1(arr, arr + size) - 1.000000) <= EPS)
            {
                printf("Test 2.....OK\n");
            }
            else
            {
                flag++;
                printf("Test 2.....FAIL\n");
            }
            
            free(arr);
        }
        else
        {
            return ERR_DETECTED;
        }
    }
    else
    {
        return ERR_DETECTED;
    }

    return flag;
}

char delete_max_abs_testing()
{
    printf("3) Delete elem testing\n");

    char flag = 0;
    int size;
    double value;

    double *before_arr = NULL;
    double *after_arr = NULL;

    // Тест 1

    if (fill_file(TEST_FILE, "4 6 2 6 2"))
    {
        size = 5;
        value = 0;

        if ((before_arr = scan_arr(TEST_FILE, size)))
        {
            delete_max_abs(before_arr, before_arr + size, value);

            if (fill_file(TEST_FILE, "4 2 6 2") && (after_arr = scan_arr(TEST_FILE, size - 1)))
            {
                if (arrays_are_equal(before_arr, after_arr, size - 1))
                {
                    printf("Test 1.....OK\n");
                }
                else
                {
                    flag++;
                    printf("Test 1.....FAIL\n");
                }

                free(before_arr);
                free(after_arr);
            }
            else
            {
                free(before_arr);
                return ERR_DETECTED;
            }
        }
        else
        {
            return ERR_DETECTED;
        }
    }
    else
    {
        return ERR_DETECTED;
    }
    
    // Тест 2

    if (fill_file(TEST_FILE, "-5 -5 -6 -7 -7"))
    {
        value = -20;

        if ((before_arr = scan_arr(TEST_FILE, size)))
        {
            delete_max_abs(before_arr, before_arr + size, value);

            if (fill_file(TEST_FILE, "-5 -5 -6 -7") && (after_arr = scan_arr(TEST_FILE, size - 1)))
            {
                if (arrays_are_equal(before_arr, after_arr, size - 1))
                {
                    printf("Test 2.....OK\n");
                }
                else
                {
                    flag++;
                    printf("Test 2.....FAIL\n");
                }

                free(before_arr);
                free(after_arr);
            }
            else
            {
                free(before_arr);
                return ERR_DETECTED;
            }   
        }
        else
        {
            return ERR_DETECTED;
        }
    }
    else
    {
        return ERR_DETECTED;
    }
    
    return flag;
}

char calc_u2_testing()
{
    printf("4) Calculate u2 testing\n");

    char flag = 0;
    int size;
    double *arr = NULL;

    // Teст 1

    size = 3;
    if (fill_file(TEST_FILE, "3 3 2"))
    {
        if ((arr = scan_arr(TEST_FILE, size)))
        {
            if (fabs(calc_u2(arr, arr + size) - 2.666666) <= EPS)
            {
                printf("Test 1.....OK\n");
            }
            else
            {
                flag++;
                printf("Test 1.....FAIL\n");
            }

            free(arr);
        }
        else
        {
            return ERR_DETECTED;
        }
    }
    else
    {
        return ERR_DETECTED;
    }
    
    // Teст 2

    if (fill_file(TEST_FILE, "-1 -2 -3"))
    {
        if ((arr = scan_arr(TEST_FILE, size)))
        {
            if (fabs(calc_u2(arr, arr + size) + 2.000000) <= EPS)
            {
                printf("Test 2.....OK\n");
            }
            else
            {
                flag++;
                printf("Test 2.....FAIL\n");
            }

            free(arr);
        }
        else
        {
            return ERR_DETECTED;
        }
    }
    else
    {
        return ERR_DETECTED;
    }
    
    return flag;
}

char add_elem_testing()
{
    printf("5) Add elem testing\n");

    char flag = 0;
    int size, pos;
    double *arr = NULL, *check = NULL;
    double *result_arr = NULL;
    double *endian = NULL;

    // Тест 1

    size = 3;
    pos = 2;

    if (fill_file(TEST_FILE, "1 2 3") && (arr = scan_arr(TEST_FILE, size)))
    {
        endian = arr + size;

        if (!(check = resize_array(arr, endian, 3)))
        {
            free(arr);
            return ERR_DETECTED;
        }

        arr = check;
        endian = arr + size + 3;

        add_elems(arr, endian, pos, 4.5);

        if (fill_file(TEST_FILE, "4.5 1 2 4.5 3 4.5") && (result_arr = scan_arr(TEST_FILE, size + 3)))
        {
            if (arrays_are_equal(arr, result_arr, size + 3))
            {
                printf("Test 1.....OK\n");
            }
            else
            {
                flag++;
                printf("Test 1.....FAIL\n");
            }

            free(arr);
            free(result_arr);
        }
        else
        {
            free(arr);
            return ERR_DETECTED;
        }
    }
    else
    {
        return ERR_DETECTED;
    }
    
    return flag;
}

// Модульное тестирование программы
int main()
{
    char errors_flag = 0;

    printf("Start testing\n");

    errors_flag += input_testing();
    errors_flag += calc_u1_testing();
    errors_flag += delete_max_abs_testing();
    errors_flag += calc_u2_testing();
    errors_flag += add_elem_testing();

    if (errors_flag)
    {
        printf("%d errors detected", errors_flag);
        return ERR_DETECTED;
    }

    printf("Success");

    return OK;
}