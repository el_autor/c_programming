#ifndef _RESIZE_H_

#define _RESIZE_H_

#include <stdio.h>
#include <stdlib.h>

double *resize_array(double *const arr, double *const endian, const int delta);

#endif
