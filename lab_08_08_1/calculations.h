#ifndef _CALCULATIONS_H_

#define _CALCULATIONS_H_

#include <stdio.h>
#include <math.h>

double calc_u1(const double *const arr, const double *const endian);
double calc_u2(const double *const arr, const double *const endian);
void delete_max_abs(double *const arr, double *const endian, const double value);
void add_elems(double *const arr, double *const endian, const int pos, const double value);

#endif
