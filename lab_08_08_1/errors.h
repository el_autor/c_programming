#ifndef _ERRORS_H_

#define _ERRORS_H_

#define OK 0
#define WRONG_INPUT 1
#define INSERT_ERROR 2
#define MEMORY_ALLOC_ERROR 3

#endif