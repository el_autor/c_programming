#include "calculations.h"

double calc_u1(const double *const arr, const double *const endian)
{
    // Расчет арифметического взвешенного
    double sum_1 = 0, sum_2 = 0, common_part;

    for (const double *i = arr; i < endian; i++)
    {
        common_part = (i - arr + 1) * (i - arr + 1);
        sum_1 += common_part * (*i);
        sum_2 += common_part;
    }

    return (sum_1 / sum_2);
}

double calc_u2(const double *const arr, const double *const endian)
{
    double sum = 0;
    size_t len = endian - arr;

    for (const double *i = arr; i < endian; i++)
    {
        sum += *i / len;
    }

    return sum;
}

void remove_elem(double *const endian, double *const needed)
{ 
    for (double *i = needed; i < endian - 1; i++)
    {
        *i = *(i + 1);
    }
}

void add_elems(double *const arr, double *const endian, const int pos, const double value)
{
    // Вставка элементов
    *(endian - 1) = value;

    for (double *i = endian - 3; i > arr + pos - 1; i--)
    {
        *(i + 1) = *i;
    }

    *(arr + pos) = value;

    for (double *i = endian - 3; i >= arr; i--)
    {
        *(i + 1) = *(i);
    }

    *(arr) = value;
}

void delete_max_abs(double *const arr, double *const endian, const double value)
{
    // Удаление элемента
    double max_abs = fabs(*arr) - value;
    double *index = arr;

    for (double *i = arr + 1; i < endian; i++)
    {
        double local = fabs(*i) - value;
        if (local > max_abs)
        {
            max_abs = local;
            index = i; 
        }
    }

    remove_elem(endian, index); // Удаление элемента сдвигом
}
