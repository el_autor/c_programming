#include "input.h"
#include "calculations.h"
#include "resize.h"
#include "errors.h"

#define DELTA_SIZE 1 // Изменение размера динамического массива

int main()
{
    int n = 0, pos = 0;
    double *array = NULL, *check = NULL;

    setbuf(stdin, NULL);
    setbuf(stdout, NULL);   

    if (!(array = program_input(&n, &pos, stdin)))
    {
        return my_errno;
    }

    double *endian = array + n;
    double u1 = calc_u1(array, endian); // Расчет u1

    for (int i = 0; i < 2; i++)
    {
        delete_max_abs(array, endian, u1); // удаление элемента с максимальной разницей между своим модулем и u1 
        endian--;
    }

    if (!(check = resize_array(array, endian, DELTA_SIZE + 2)))
    {
        free(array);
        return MEMORY_ALLOC_ERROR;
    }

    array = check;
    endian = array + n + DELTA_SIZE;

    double u2 = calc_u2(array, endian - DELTA_SIZE - 2); // Расчет u2

    if (pos >= endian - array - 3)
    {   
        free(array);
        return INSERT_ERROR;
    }

    add_elems(array, endian, pos, u2); // добавление 3-х элементов в массив
    
    for (double *i = array; i < endian; i++)
    {
        printf("%lf ", *i);
    }

    free(array);
    return OK;
}
