#ifndef _STRUCT_H_

#define _STURCT_H_

#define MAX_WORD_SIZE 11

struct list
{
    char word[MAX_WORD_SIZE];
    struct list *next;
};

typedef struct list list_t;

#endif
