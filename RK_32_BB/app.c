#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "struct.h"

#define IN_FILE "in.txt"
#define OUT_FILE "out.txt"
#define OK 0
#define ERROR 1

void add_node(list_t *head, char *word)
{
    while (head->next)
    {
        head = head->next;
    }

    head->next = malloc(sizeof(list_t));
    head->next->next = NULL;
    strcpy(head->next->word, word);  
}

list_t *get_data(FILE *const stream)
{
    list_t *list = NULL;
    char local[MAX_WORD_SIZE];
    list = malloc(sizeof(list_t));
    list->next = NULL;

    while (fscanf(stream, "%s", local) == 1)
    {
        printf("%s\n", local);
        add_node(list, local);
    }

    return list;
}

list_t *get_reversed(list_t *head)
{
    list_t *new_head = NULL, *copy_head = head;
    new_head = malloc(sizeof(list_t));
    new_head->next = NULL;

    while (copy_head->next)
    {
        list_t *previous = NULL;

        while (copy_head->next)
        {
            previous = copy_head;
            copy_head = copy_head->next;
            add_node(new_head, copy_head->word);
        }
        
        // Освобождаем только что просмотренный элемент
        free(previous->next);
        previous->next = NULL;
        
        copy_head = head;
    }

    return new_head;
}

void show_list(char *filename, list_t *head)
{
    FILE *out_stream = fopen(filename, "w");

    while (head)
    {
        fprintf(out_stream, "%s\n", head->word);
        head = head->next;
    }
    
    fclose(out_stream);
}

int main()
{
    list_t *word = NULL;
    FILE *stream = NULL;
    
    if ((stream = fopen(IN_FILE, "r")))
    {
        printf("here\n");	    
        word = get_data(stream);
        printf("%s\n", word->next->word);
        word = get_reversed(word);
        show_list(OUT_FILE, word->next);
        fclose(stream);
    }
    else
    {
        return ERROR;
    }

    return OK;
}

