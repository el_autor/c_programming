#include <stdio.h>

#define SECS 60
#define HRS 3600
#define CORRECT_EXIT 0

int main()
{
    int hour, minute, secs;
    puts("Enter seconds:");
    scanf("%d", &secs);
    hour = secs / HRS;
    secs %= HRS;
    minute = secs / SECS;
    secs %= SECS;
    printf("%d - hours : %d - minuts : %d - secs", hour, minute, secs);
    return CORRECT_EXIT;
}
