#include <stdio.h>

#define FIRST_TERM 0
#define SECOND_TERM 1
#define SCANF_CORRECT 1
#define DOWN_LIMIT 0
#define UP_LIMIT 92
#define CORRECT_EXIT 0
#define WRONG_INPUT 1

long long int fib_count(int n)
{
    long long int first = FIRST_TERM;
    long long int second = SECOND_TERM;
    for (int i = 0; i < n; i++)
    {
        long long int temp = second;
        second = second + first;
        first = temp;
    }
    return first;
}

int main()
{
    int n;
    if (scanf("%d", &n) == SCANF_CORRECT && n >= DOWN_LIMIT && n <= UP_LIMIT)   
    {
        printf("%lli", fib_count(n));
        return CORRECT_EXIT;
    }
    return WRONG_INPUT;
}
