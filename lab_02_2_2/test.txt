Set1:
Input:
0 0  5 0  0 5  0 0
Output:
1

Set2:
Input:
0 0  5 0  0 5  0 4
Output:
1

Set3:
Input:
0 0  5 0  0 5  -1 -1
Output:
2

Set4:
Input:
0 0  5 0  0 5  1 1
Output:
0

Set5:
Input:
0 0  5 0  0 5  3 1
Output:
0

Set6:
Input:
0 0  5 0  0 5  3 3
Output:
2

Set7:
Input:
0 0  5 0  0 5  5.001 0
Output:
2

Set8:
Input:
0 0 ert ergty 8
Output:
(None)

Set 9:
Input:
1 1 1 1 1 1 2 2
Output:
(None)
