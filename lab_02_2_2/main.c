#include <stdio.h>
#include <math.h>
#include <stdbool.h>

#define CORRECT_SCANF 8
#define SQUARE 2
#define ZERO 0
#define POS 1
#define NEG 3
#define ON_LINE 2
#define CORRECT_EXIT 0
#define WRONG_INPUT 3
#define OUTSIDE 2
#define ON_DA_BOARD 1
#define INSIDE 0

float side(float x1, float y1, float x2, float y2)
{
    return sqrt(pow(x1 - x2, SQUARE) + pow(y1 - y2, SQUARE));
}

bool triangle_check(float xa, float ya, float xb, float yb, float xc, float yc)
{
    float ab = side(xa, ya, xb, yb);
    float bc = side(xb, yb, xc, yc);
    float ca = side(xc, yc, xa, ya);
    return (ab < bc + ca && bc < ab + ca && ca < ab + bc);
}

float proizv(float xa, float ya, float xb, float yb)
{
    return xa * yb - xb * ya;
}

int pseudo(float xa, float ya, float x, float y, float xb, float yb, float xc, float yc)
{
    float pseudomult1 = proizv(xb - xa, yb - ya, xc - xa, yc - ya);
    float pseudomult2 = proizv(xb - xa, yb - ya, x - xa, y - ya);
    if (pseudomult1 * pseudomult2 > ZERO)
        return POS;
    else if (pseudomult1 * pseudomult2 == ZERO)
        return ON_LINE;
    else
        return NEG;
}

int return_mode(int side_a, int side_b, int side_c)
{
    if (side_a == NEG || side_b == NEG || side_c == NEG)
        return OUTSIDE;
    else if (side_a == ON_LINE || side_b == ON_LINE || side_c == ON_LINE)
        return ON_DA_BOARD;
    else
        return INSIDE;
}

int is_point_in_triangle(int xa, int ya, int xb, int yb, int xc, int yc, int x, int y)
{
    int side_a = pseudo(xa, ya, x, y, xb, yb, xc, yc);
    int side_b = pseudo(xb, yb, x, y, xc, yc, xa, ya);
    int side_c = pseudo(xc, yc, x, y, xa, ya, xb, yb);
    return return_mode(side_a, side_b, side_c);
}

int main()
{
    float xa, ya, xb, yb, xc, yc, x, y;
    int ok = scanf("%f%f%f%f%f%f%f%f", &xa, &ya, &xb, &yb, &xc, &yc, &x, &y);
    if (ok == CORRECT_SCANF && triangle_check(xa, ya, xb, yb, xc, yc))
    {
        int situation = is_point_in_triangle(xa, ya, xb, yb, xc, yc, x, y);
        printf("%d", situation);
        return CORRECT_EXIT; 
    }                   
    return WRONG_INPUT;
}
