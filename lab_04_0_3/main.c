#include <stdio.h>
#include <math.h>

#define N 10
#define ERROR 0
#define OK 1
#define DOWN_LIMIT 0
#define UP_LIMIT 10
#define ZERO 0
#define NOT_PALINDROM 0
#define PALINDROM 1
#define NO_ELEMENTS 2
#define WRONG_INPUT 1
#define ALL_OK 0

typedef const int c_i;

 // Функция ввода статического массива
int array_input(int *const mas, int *const elems_number)
{
    if (scanf("%d", elems_number) == 1 && *elems_number > DOWN_LIMIT && *elems_number <= UP_LIMIT)
    {
        char sep;
        for (int i = 0; i < *elems_number; i++)
            // Если перенос строки идет после кол-ва эл-тов массива или введеный элемент не число
            if ((i == 0 && scanf("%c", &sep) == 1 && sep != ' ') || scanf("%d", &mas[i]) != 1)
                return ERROR;
            // Если введено меньше элементов, чем ожидалось
            else if (i < *elems_number - 1 && scanf("%c", &sep) == 1 && sep == '\n')
                return ERROR;
            // Если введено больше элементов, чем ожидалось
            else if (i == *elems_number - 1 && scanf("%c", &sep) == 1 && sep != '\n')
            {
                while (sep == ' ')
                    scanf("%c", &sep);
                if (sep != '\n')
                    return ERROR;
            }
    }
    else
        return ERROR;
    return OK;
}

// Проверка элемента на палиндром
int palindrom(int elem)
{
    int copy_elem = elem;
    int grade = ZERO;
    while (copy_elem > 9)
    {
        grade++;
        copy_elem /= 10;
    }
    while (grade > 0)
    {
        // если симметричные цифры в числе не совпадают, то не палиндром
        if (elem / (int)pow(10, grade) != elem % 10)
            return NOT_PALINDROM;
        else
        {
            elem %= (int)pow(10, grade);
            elem /= 10;
            grade -= 2;
        }
    }
    return PALINDROM;
}

// Удаление элемента, если найден палиндром
void del_palindroms(int *const mas, int *const elems_number)
{
    for (int i = 0; i < *elems_number; i++)
        if (mas[i] >= 0 && palindrom(mas[i]))
        {
            for (int j = i; j < *elems_number - 1; j++)
                mas[j] = mas[j + 1];
            (*elems_number)--;
            i--;
        }
}

// Функция вывода статического массива
void output_array(c_i *const mas, c_i *const elems_number)
{
    for (int i = 0; i < *elems_number; i++)
        if (i == (*elems_number) - 1)
            printf("%d", mas[i]);
        else
            printf("%d ", mas[i]);
}

int main()
{
    int array[N];
    int elems_number;
    if (array_input(array, &elems_number))
    {
        del_palindroms(array, &elems_number);
        if (!elems_number == ZERO)
	    {
            output_array(array, &elems_number);
	        return ALL_OK;
	    }
        else
            return NO_ELEMENTS;
    }
    else
    {
        puts("Wrong Input!");
        return WRONG_INPUT;
    }
}
