#include <stddef.h>
#include <string.h>
#include <stdio.h>

#define ALL_OK 0
#define SOMETHING_WRONG 1
#define OK 1
#define ERROR 0

char *my_strpbrk(char *str1, const char *str2)
{
    long int len2 = 0L;
    while (str2[len2] != '\0')
        len2++;
    while (*str1 != '\0')
    {
        for (long int j = 0L; j < len2; j++)
            if (*str1 == str2[j])
                return str1;
        str1++;
    }
    return NULL;
}

size_t my_strspn(const char *str1, const char *str2)
{
    size_t index = 0;
    long int len2 = 0L;
    while (str2[len2] != '\0')
        len2++;
    while (*str1 != '\0')
    {
        int ok = 0;
        for (long int j = 0L; j < len2; j++)
            if (*str1 == str2[j])
                ok = 1;
        if (ok)
            index++;
        else
            break;
        str1++;
    }
    return index;
}

size_t my_strcspn(const char *str1, const char *str2)
{
    size_t index = 0;
    long int len2 = 0L;
    while (str2[len2] != '\0')
        len2++;
    while (*str1 != '\0')
    {
        int ok = 0;
        for (long int j = 0L; j < len2; j++)
            if (*str1 == str2[j])
                ok = 1;
        if (!ok)
            index++;
        else
            break;
        str1++;
    }
    return index;
}

char *my_strchr(char *str, int ch)
{
    if (str == NULL)
        return NULL;
    if (ch > 255 || ch < 0)
        return NULL;
    while (*str != '\0')
    {
        if ((char) ch == *str)
            return str;
        str++;
    }
    if ((char) ch == *str)
        return str;
    return NULL;
}

char *my_strrchr(char *str, int ch)
{
    char *last = NULL;
    if (str == last)
        return last;
    if (ch > 255 || ch < 0)
        return last;
    while (*str != '\0')
    {
        if ((char) ch == *str)
            last = str;
        str++;
    }
    if ((char) ch == *str)
        last = str;
    return last;
}

int test_strpbrk()
{
    if (*(my_strpbrk("Something", "tn")) != *(strpbrk("Something", "tn")))
        return ERROR;

    if ((my_strpbrk("Something", "ab")) != (strpbrk("Something", "ab")))
        return ERROR;
    return OK;
}

int test_strcpn()
{
    if (my_strspn("Wassup", "sa") != strspn("Wassup", "sa"))
        return ERROR;

    if (my_strspn("Wassup", "o") != strspn("Wassup", "o"))
        return ERROR;
    return OK;
}

int test_strcspn()
{
    if (my_strcspn("Wassup", "up") != strcspn("Wassup", "up"))
        return ERROR;

    if (my_strcspn("Sand", "S") != strcspn("Sand", "S"))
        return ERROR;

    if (my_strcspn("Sand", "op") != strcspn("Sand", "op"))
        return ERROR;
    return OK;
}

int test_strchr()
{
    if (*(my_strchr("Dope", 'p')) != *(strchr("Dope", 'p')))
        return ERROR;

    if (my_strchr("Dope", 'u') != strchr("Dope", 'u'))
        return ERROR;
    return OK;
}

int test_strrchr()
{
    if (*(my_strrchr("abab", 'b')) != *(strrchr("abab", 'b')))
        return ERROR;

    if (my_strrchr("Dope", 'a') != strrchr("Dope", 'a'))
        return ERROR;
    return OK;
}

int main()
{
    int error_counter = 0;
    if (!test_strpbrk())
        error_counter++;

    if (!test_strcpn())
        error_counter++;

    if (!test_strcspn())
        error_counter++;

    if (!test_strchr())
        error_counter++;
    
    if (!test_strrchr())
        error_counter++;

    if (error_counter)
    {
        printf("Errors detected");
        return SOMETHING_WRONG;
    }
    printf("Tests passed");
    return ALL_OK;
}
