#include <stdio.h>

#define UNO 1
#define CORRECT_EXIT 0
#define ZERO 0

int main()
{
    float r1, r2, r3;
    puts("Enter resistances:");
    scanf("%f%f%f", &r1, &r2, &r3);
    if (r1 <= ZERO || r2 <= ZERO || r3 <= ZERO)
        printf("Invalid input!");
    else
        printf("%.5f", UNO / ((UNO / r1) + (UNO / r2) + (UNO / r3)));
    return CORRECT_EXIT;
}
