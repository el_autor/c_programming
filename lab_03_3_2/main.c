#include <stdio.h>
#include <math.h>

#define NO_FILENAME -1
#define NO_SUCH_FILE -2
#define INVALID_DATA -3
#define OK 0
#define ERROR -1
#define EMPTY_FILE 0
#define WRONG_SYMBOLS 0
#define SUCCESS 1
#define NO_RULE 0

int calk_average(FILE *const fp, double *const average)
{
    int amount = 0; 
    double number, sum = 0;
    int scanf_result = fscanf(fp, "%lf", &number);
    while (scanf_result == 1)
    {
        amount++;
        sum += number;
        scanf_result = fscanf(fp, "%lf", &number);
    }
    if (!scanf_result)
        return ERROR;
    if (amount)
        *average = sum / amount;
    return amount;
}

double calk_disp(FILE *const fp, const double avg, const int amount)
{
    double number, quadro_sum = 0;
    while (fscanf(fp, "%lf", &number) == 1)
        quadro_sum += pow(number - avg, 2);
    return quadro_sum / amount;
}

int is_rule(FILE *const fp, const double avg, const double sigma)
{
    double number;
    while (fscanf(fp, "%lf", &number) == 1)
        if (number > avg + 3 * sigma || number < avg - 3 * sigma)
            return NO_RULE;
    return SUCCESS;
}

int triple_sigma(FILE *const fp)
{
    double average, disp, sigma;
    int amount = calk_average(fp, &average);
    if (amount > 0)
    {
        rewind(fp);
        disp = calk_disp(fp, average, amount);
        sigma = sqrt(disp);
        rewind(fp);
        if (is_rule(fp, average, sigma))
            printf("%d", SUCCESS);
        else
            printf("%d", NO_RULE);
    }
    else if (amount == 0)    
        return EMPTY_FILE;
    else if (amount == -1)
        return WRONG_SYMBOLS;
    return SUCCESS;
}

int main(const int argc, const char *const *const argv)
{
    if (argc == 1)
        return NO_FILENAME;
    FILE *fp = fopen(argv[1], "r");
    if (fp)
    {
        if (triple_sigma(fp))
        {
            fclose(fp);
            return OK;
        }
        else
        {
            fclose(fp);
            return INVALID_DATA;
        }
    }
    else
        return NO_SUCH_FILE;
}
    
