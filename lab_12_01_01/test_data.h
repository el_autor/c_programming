#ifndef _TEST_DATA_H_

#define _TEST_DATA_H_

#define GET_VALUE_DATA_1 "2 2 2 1 5 0\n2"
#define GET_VALUE_DATA_2 "2 2 2 1 5 0\n-2"
#define GET_VALUE_DATA_3 "2 2 2 1 5 0\n0"
#define GET_VALUE_DATA_4 "1 2 1 1 1 0\n1.5"

#define GET_DDX_DATA_1 "4 5 2 2 2 1 1 0\n"
#define GET_DDX_DATA_2 "0 0\n"

#define GET_SUM_DATA_1 "4 5 4 3 -1 1 1 0\n\
                        1 6 3 4 3 3 1 1 -3 0\n"

#define GET_SUM_DATA_2 "\n\n\
                        3 3 2 2 1 1 1 0\n"

#define GET_SUM_DATA_3 "3 3 2 2 1 1 1 0\n"

#define GET_SUM_DATA_4 ""

#define GET_DIV_DATA_1 "2 3 2 2 2 1 2 0\n"
#define GET_DIV_DATA_2 "2 3 2 1\n"
#define GET_DIV_DATA_3 "2 2 2 0\n"
#define GET_DIV_DATA_4 ""
                        

#endif