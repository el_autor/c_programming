#include "input.h"

bool input_not_finished(FILE *const in_stream)
{
    char symb = 0;

    while ((symb = fgetc(in_stream)) == ' ');
        
    if (symb == '\n' || symb == EOF)
    {
        ungetc(symb, in_stream);
        return false;
    }

    ungetc(symb, in_stream);
    return true;
}   

list_t *input_polynom(FILE *const in_stream)
{
    list_t *list = NULL;
    bool input_flag = true;
    int previous_grade = INT_MAX;
    char symb = 0;

    // Create barrier
    if (create_sentinel(&list) != OK)
    {
        return NULL;
    }
    
    if ((symb = fgetc(in_stream)) != '\n')
    {
        ungetc(symb, in_stream);
    }

    if ((symb = fgetc(in_stream)) == '\n' || symb == EOF)
    {
        ungetc(symb, in_stream);
        return list;
    }

    ungetc(symb, in_stream);

    while (input_flag)
    {
        if (add_term(in_stream, list))
        {
            //printf("out\n");
            free_list(list);
            return NULL;
        }

        if (sequence_is_correct(&previous_grade, list) != OK)
        {
            free_list(list);
            return NULL;
        }

        input_flag = input_not_finished(in_stream);
    }

    return list;
}