#include "actions.h"

double get_value(FILE *const stream, const list_t *polynom)
{
    double value = 0, x = 0;

    if (fscanf(stream, "%lf", &x) != 1)
    {
        errno = WRONG_X_INPUT;
        return ERROR;
    }

    while (polynom != NULL)
    {
        double part = 1;
        part *= polynom->coefficient;

        for (int i = 0; i < polynom->grade; i++)
        {
            part *= x;
        }

        value += part;
        polynom = polynom->next;
    }

    return value;
}

void get_derivative(list_t *polynom)
{
    while (polynom->next)
    {
        if (polynom->next->grade == 0)
        {
            free(polynom->next);
            polynom->next = NULL;
        }
        else
        {
            polynom->next->coefficient *= polynom->next->grade;
            (polynom->next->grade)--;
            polynom = polynom->next;
        }
    }
}

list_t *make_sum(const list_t *term_1, const list_t *term_2)
{
    list_t *result = NULL;

    if (create_sentinel(&result) != OK)
    {
        return NULL;
    }

    while (term_1)
    {
        if (create_node(result, term_1->coefficient, term_1->grade) != OK)
        {
            free_list(result);
            return NULL;
        }

        term_1 = term_1->next;
    }

    while (term_2)
    {
        if (insert_term(result, term_2->coefficient, term_2->grade) != OK)
        {
            free_list(result);
            return NULL;    
        }

        term_2 = term_2->next;
    }

    return result; 
}

int make_separation(const list_t *source, list_t **const even, list_t **const odd)
{
    if ((create_sentinel(even) || create_sentinel(odd)) != OK) 
    {
        return ERROR;
    }

    while (source->next)
    {
        if (source->next->grade % 2 == 0)
        {
            if (create_node(*even, source->next->coefficient, source->next->grade) != OK)
            {
                return ERROR;
            }
        }
        else
        {
            if (create_node(*odd, source->next->coefficient, source->next->grade) != OK)
            {
                return ERROR;
            }            
        }

        source = source->next;        
    }

    return OK;
}