#ifndef _TEST_H_

#define _TEST_H_

#include "app.h"
#include "test_data.h"

#define MAX_STR_SIZE 100
#define EPS 0.000001

#define SOURCE_FILE "source.txt"
#define RESULT_FILE "result.txt"

#define FILE_NOT_FILLED 50
#define FILE_NOT_OPENED 51
#define DATA_MISMATCH 52

#endif