#include "list_framework.h"

int sequence_is_correct(int *const previous_value, const list_t *polynom)
{
    while (polynom->next)
    {
        polynom = polynom->next;
    }

    if (polynom->grade > *previous_value)
    {
        errno = WRONG_GRADE_SEQUENCE;
        return ERROR;
    }
    else
    {
        *previous_value = polynom->grade;
    }

    return OK;
}

int create_node(list_t *head, const int coef, const int grade)
{
    list_t *local = NULL;
    
    if (coef == 0)
    {
        return OK;
    }

    if ((local = malloc(sizeof(list_t))) == NULL)
    {
        errno = MEM_NOT_ALLOCED;
        return ERROR;
    }

    while (head->next)
    {
        head = head->next;
    }

    local->coefficient = coef;
    local->grade = grade;
    local->next = NULL;
    head->next = local;

    return OK;
}

int add_term(FILE *const stream, list_t *const head)
{
    int coef = 0, grade = 0;

    if (fscanf(stream, "%d %d", &coef, &grade) != 2)
    {
        errno = WRONG_TERM_INPUT;
        return ERROR;
    }

    if (grade < 0)
    {
        errno = NEGATIVE_GRADE;
        return ERROR;
    }

    if (create_node(head, coef, grade) != OK)
    {
        return ERROR;
    }

    return OK;
}

void free_list(list_t *polynom)
{
    list_t *local = NULL;

    while (polynom)
    {
        local = polynom->next;
        free(polynom);
        polynom = local;
    }
}

void show_list(FILE *const stream, const list_t *polynom)
{
    if (!polynom)
    {
        fprintf(stream, "0 0 L\n");
    }
    else
    {
        while (polynom)
        {
            fprintf(stream, "%d %d ", polynom->coefficient, polynom->grade);
            polynom = polynom->next;
        }

        fprintf(stream, "L\n");
    }
}

int create_sentinel(list_t **const list)
{
    if ((*list = malloc(sizeof(list_t))) == NULL)
    {
        errno = MEM_NOT_ALLOCED;
        return ERROR;
    }

    (*list)->coefficient = 0;
    (*list)->grade = 0;
    (*list)->next = NULL;

    return OK;
}

int insert_term(list_t *head, const int coefficient, const int grade)
{
    list_t *node = NULL;

    while (head->next && head->next->grade > grade)
    {
        head = head->next;
    }

    if (head->next && grade == head->next->grade)
    {
        head->next->coefficient += coefficient;
    }
    else
    {
        if ((node = malloc(sizeof(list_t))) == NULL)
        {
            errno = MEM_NOT_ALLOCED;
            return ERROR;
        }

        node->coefficient = coefficient;
        node->grade = grade; 
        node->next = head->next;
        head->next = node;
    }
    
    if (head->next->coefficient == 0)
    {
        node = head->next;
        head->next = head->next->next; 
        free(node);
    }

    return OK;
}