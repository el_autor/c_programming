#ifndef _STRUCT_H_

#define _STRUCT_H_

struct list
{
    int coefficient;
    int grade;
    struct list *next;
};

typedef struct list list_t;

#endif