#ifndef _ACTIONS_H_

#define _ACTIONS_H

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <stdbool.h>
#include "struct.h"
#include "errors.h"
#include "list_framework.h"

double get_value(FILE *const stream, const list_t *polynom);
void get_derivative(list_t *polynom);
list_t *make_sum(const list_t *term_1, const list_t *term_2);
int make_separation(const list_t *source, list_t **const even, list_t **const odd);

#endif