#ifndef _LIST_FRAMEWORK_H_

#define _LIST_FRAMEWORK_H_

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "struct.h"
#include "errors.h"

int add_term(FILE *const stream, list_t *const head);
void free_list(list_t *polynom);
void show_list(FILE *const stream, const list_t *polynom);
int create_sentinel(list_t **const list);
int create_node(list_t *head, const int coef, const int grade);
int sequence_is_correct(int *const previous_value, const list_t *polynom);
int insert_term(list_t *head, const int coefficient, const int grade);

#endif