#ifndef _APP_H_

#define _APP_H_

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include "struct.h"
#include "errors.h"
#include "input.h"
#include "list_framework.h"
#include "actions.h"

#define KEY_SIZE 4

#endif