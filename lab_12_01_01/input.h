#ifndef _INPUT_H_

#define _INPUT_H_

#include <errno.h>
#include <stdio.h>
#include <stdbool.h>
#include <limits.h>
#include "errors.h"
#include "struct.h"
#include "list_framework.h"

list_t *input_polynom(FILE *const in_stream);

#endif