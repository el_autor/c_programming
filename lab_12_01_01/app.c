#include "app.h"

int main()
{
    list_t *polynom_a = NULL, *polynom_b = NULL, *polynom_c = NULL;
    char key[KEY_SIZE];

    if (fscanf(stdin, "%s", key) != 1)
    {
        return WRONG_KEY;
    }

    if (!strcmp("val", key)) 
    {
        // Подсчет значения при введенном значении переменной
        if ((polynom_a = input_polynom(stdin)) == NULL)
        {
            return errno;
        }

        fprintf(stdout, "%lf\n", get_value(stdin, polynom_a));
    }
    else if (!strcmp("ddx", key))
    {
        // Взятие производной
        if ((polynom_a = input_polynom(stdin)) == NULL)
        {
            return errno;
        }

        get_derivative(polynom_a);
        show_list(stdout, polynom_a->next);
    }
    else if (!strcmp("sum", key))
    {   
        // Сумма многочленов
        if ((polynom_a = input_polynom(stdin)) == NULL)
        {
            return errno;
        }

        if ((polynom_b = input_polynom(stdin)) == NULL)
        {
            free_list(polynom_a);
            return errno;
        }

        if ((polynom_c = make_sum(polynom_a->next, polynom_b->next)) == NULL)
        {
            free_list(polynom_a);
            free_list(polynom_b);
            return errno;
        }

        show_list(stdout, polynom_c->next);
    }
    else if (!strcmp("dvd", key))
    {
        // Разделения на полиномы четных и нечетных степеней
        if ((polynom_a = input_polynom(stdin)) == NULL)
        {
            return errno;
        }

        if (make_separation(polynom_a, &polynom_b, &polynom_c) != OK)
        {
            free(polynom_a);
            free(polynom_b);
            free(polynom_c);
            return errno;
        }
        
        // Вывод четных
        show_list(stdout, polynom_b->next); 
        // Вывод нечетных
        show_list(stdout, polynom_c->next);
    }
    else
    {
        return WRONG_KEY;
    }

    free_list(polynom_a);
    free_list(polynom_b);
    free_list(polynom_c);
    return OK;
}