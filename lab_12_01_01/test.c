#include "test.h"

void show_test_error(FILE *const out_stream, const int err_code)
{
    if (err_code == OK)
    {
        return;
    }

    switch (err_code)
    {
        case FILE_NOT_FILLED:
            fprintf(out_stream, "file not filled with test data\n");
            break;
        case FILE_NOT_OPENED:
            fprintf(out_stream, "file can be opened\n");
            break;
        case DATA_MISMATCH:
            fprintf(out_stream, "data_mismatch\n");
            break; 
    }
}

int test_result(FILE *const out_stream, const bool inverse, const int test_number)
{
    bool flag;
    show_test_error(out_stream, errno);

    if (inverse == false)
    {
        if (errno)
        {
            printf("TEST %d.....FAILED\n", test_number);
            flag = true;
        }
        else
        {
            printf("TEST %d.....PASSED\n", test_number);
            flag = false;
        }
    }
    else
    {
        if (!errno)
        {
            printf("TEST %d.....FAILED\n", test_number);
            flag = true;
        }
        else
        {
            printf("TEST %d.....PASSED\n", test_number);
            flag = false;
        }
    }

    errno = OK;
    return flag;
}


int fill_file(const char *const filename, const char *const message)
{
    FILE *file = fopen(filename, "w");

    if (file)
    {
        fprintf(file, "%s", message);
        fclose(file);
        return OK;
    }

    return ERROR; 
}

void template_get_value_test(const char *const filename, const char *const data, const double proper_value)
{
    FILE *stream_in = NULL;
    list_t *polynom = NULL;

    if (fill_file(filename, data) == OK)
    {
        if ((stream_in = fopen(filename, "r")) != NULL)
        {
            if ((polynom = input_polynom(stream_in)) == NULL);
            else if (abs(proper_value - get_value(stream_in, polynom)) >= EPS)
            {
                errno = DATA_MISMATCH;
            }

            fclose(stream_in);
        }
        else
        {
            errno = FILE_NOT_OPENED;
        }
    }
    else
    {
        errno = FILE_NOT_FILLED;
    }

    free_list(polynom);
}

int positive_x(FILE *const out_stream)
{
    double proper_value = 17; 

    template_get_value_test(SOURCE_FILE, GET_VALUE_DATA_1, proper_value);

    return test_result(out_stream, false, 1);
}

int negative_x(FILE *const out_stream)
{
    double proper_value = 9;

    template_get_value_test(SOURCE_FILE, GET_VALUE_DATA_2, proper_value);

    return test_result(out_stream, false, 2);
}

int zero_x(FILE *const out_stream)  
{
    double proper_value = 5;

    template_get_value_test(SOURCE_FILE, GET_VALUE_DATA_3, proper_value);

    return test_result(out_stream, false, 3);
}

int double_x(FILE *const out_stream)
{
    double proper_value = 4.25;

    template_get_value_test(SOURCE_FILE, GET_VALUE_DATA_4, proper_value);

    return test_result(out_stream, false, 4);    
}

int compare_answers(const char *const proper_answer, FILE *const stream)
{
    char line[MAX_STR_SIZE], string[MAX_STR_SIZE];
    fseek(stream, SEEK_SET, 0);
    line[0] = '\0';

    while (fgets(string, MAX_STR_SIZE, stream))
    {
        strcat(line, string);
    }

    if (strcmp(line, proper_answer))
    {
        printf("%s\n", line);
        printf("%s\n", proper_answer);
        return ERROR;
    }

    return OK;
}

void template_get_ddx_test(const char *const src_filename, const char *const dst_filename, const char *const data, const char *const proper_answer)
{
    FILE *stream_in = NULL, *stream_out = NULL;
    list_t *polynom = NULL;

    if (fill_file(src_filename, data) == OK)
    {
        if ((stream_in = fopen(src_filename, "r")) && (stream_out = fopen(dst_filename, "w+")))
        {
            if ((polynom = input_polynom(stream_in)))
            {
                get_derivative(polynom);
                show_list(stream_out, polynom->next);

                if (compare_answers(proper_answer, stream_out) != OK)
                {
                    errno = DATA_MISMATCH;
                }
            }

            fclose(stream_in);
            fclose(stream_out);
        }
        else
        {
            errno = FILE_NOT_OPENED;
        }
    }
    else
    {
        errno = FILE_NOT_FILLED;
    }

    free_list(polynom);
}

int random_derivative(FILE *const out_stream)
{
    char proper_answer[] = "20 4 4 1 2 0 L\n";

    template_get_ddx_test(SOURCE_FILE, RESULT_FILE, GET_DDX_DATA_1, proper_answer);

    return test_result(out_stream, false, 1);
}

int zero_derivative(FILE *const out_stream)
{
    char proper_answer[] = "0 0 L\n";

    template_get_ddx_test(SOURCE_FILE, RESULT_FILE, GET_DDX_DATA_2, proper_answer);

    return test_result(out_stream, false, 2);
}

void template_get_sum(const char *const src_filename, const char *const dst_filename, const char *const data, const char *const proper_answer)
{
    FILE *stream_in = NULL, *stream_out = NULL;
    list_t *polynom_a = NULL, *polynom_b = NULL, *polynom_c = NULL;

    if (fill_file(src_filename, data) == OK)
    {
        if ((stream_in = fopen(src_filename, "r")) && (stream_out = fopen(dst_filename, "w+")))
        {
            if ((polynom_a = input_polynom(stream_in)) && (polynom_b = input_polynom(stream_in)))
            {
                polynom_c = make_sum(polynom_a->next, polynom_b->next);
                show_list(stream_out, polynom_c->next);

                if (compare_answers(proper_answer, stream_out) != OK)
                {
                    errno = DATA_MISMATCH;
                }
            }
            
            fclose(stream_in);
            fclose(stream_out);
        }
        else
        {
            errno = FILE_NOT_OPENED;
        }
    }
    else
    {
        errno = FILE_NOT_FILLED;
    }

    free_list(polynom_a);
    free_list(polynom_b);
    free_list(polynom_c);
}

int random_sum(FILE *const out_stream)
{
    char proper_answer[] = "1 6 4 5 3 4 7 3 -2 0 L\n";

    template_get_sum(SOURCE_FILE, RESULT_FILE, GET_SUM_DATA_1, proper_answer);

    return test_result(out_stream, false, 1);
}

int first_is_empty_sum(FILE *const out_stream)
{
    char proper_answer[] = "3 3 2 2 1 1 1 0 L\n";

    template_get_sum(SOURCE_FILE, RESULT_FILE, GET_SUM_DATA_2, proper_answer);

    return test_result(out_stream, false, 2);
}

int second_is_empty_sum(FILE *const out_stream)
{
    char proper_answer[] = "3 3 2 2 1 1 1 0 L\n";

    template_get_sum(SOURCE_FILE, RESULT_FILE, GET_SUM_DATA_3, proper_answer);

    return test_result(out_stream, false, 3);
}

int both_are_empty_sum(FILE *const out_stream)
{
    char proper_answer[] = "0 0 L\n";

    template_get_sum(SOURCE_FILE, RESULT_FILE, GET_SUM_DATA_4, proper_answer);

    return test_result(out_stream, false, 4);
}

void template_get_division(const char *const src_filename, const char *const dst_filename, const char *const data, const char *const proper_answer)
{
    FILE *stream_in = NULL, *stream_out = NULL;
    list_t *polynom_result = NULL, *polynom_even = NULL, *polynom_odd = NULL;

    if (fill_file(src_filename, data) == OK)
    {
        if ((stream_in = fopen(src_filename, "r")) && (stream_out = fopen(dst_filename, "w+")))
        {
            if ((polynom_result = input_polynom(stream_in)))
            {
                make_separation(polynom_result, &polynom_even, &polynom_odd);
                show_list(stream_out, polynom_even->next);
                show_list(stream_out, polynom_odd->next);

                if (compare_answers(proper_answer, stream_out) != OK)
                {
                    errno = DATA_MISMATCH;
                }
            }

            fclose(stream_in);
            fclose(stream_out);
        }
        else
        {
            errno = FILE_NOT_OPENED;
        }
    }
    else
    {
        errno = FILE_NOT_FILLED;
    }

    free_list(polynom_result);
    free_list(polynom_even);
    free_list(polynom_odd);
}

int random_division(FILE *const out_stream)
{
    char proper_answer[] = "2 2 2 0 L\n2 3 2 1 L\n";

    template_get_division(SOURCE_FILE, RESULT_FILE, GET_DIV_DATA_1, proper_answer);

    return test_result(out_stream, false, 1);
}

int no_even_division(FILE *const out_stream)
{
    char proper_answer[] = "0 0 L\n2 3 2 1 L\n";

    template_get_division(SOURCE_FILE, RESULT_FILE, GET_DIV_DATA_2, proper_answer);

    return test_result(out_stream, false, 2);
}

int no_odd_division(FILE *const out_stream)
{
    char proper_answer[] = "2 2 2 0 L\n0 0 L\n";

    template_get_division(SOURCE_FILE, RESULT_FILE, GET_DIV_DATA_3, proper_answer);

    return test_result(out_stream, false, 3);
}

int zero_division(FILE *const out_stream)
{
    char proper_answer[] = "0 0 L\n0 0 L\n";

    template_get_division(SOURCE_FILE, RESULT_FILE, GET_DIV_DATA_4, proper_answer);

    return test_result(out_stream, false, 4);
}

int test_get_value(FILE *const out_stream)
{
    int errs_counter = 0;

    fprintf(out_stream, "\n1) get_value_testing\n\n");

    fprintf(out_stream, "positive x:\n");
    errs_counter += positive_x(out_stream);

    fprintf(out_stream, "negative x:\n");
    errs_counter += negative_x(out_stream);

    fprintf(out_stream, "zero x:\n");
    errs_counter += zero_x(out_stream);

    fprintf(out_stream, "double x:\n");
    errs_counter += double_x(out_stream);

    return errs_counter;
}

int test_get_derivative(FILE *const out_stream)
{
    int errs_counter = 0;
    
    fprintf(out_stream, "\n2) get_value_testing\n\n");

    fprintf(out_stream, "random polynom:\n");
    errs_counter += random_derivative(out_stream);
    
    fprintf(out_stream, "zero polynom:\n");
    errs_counter += zero_derivative(out_stream);

    return errs_counter;
}

int test_make_sum(FILE *const out_stream)
{
    int errs_counter = 0;

    fprintf(out_stream, "\n3) make_sum_testing\n\n");    

    fprintf(out_stream, "two random polynoms:\n");
    errs_counter += random_sum(out_stream);

    fprintf(out_stream, "first is empty:\n");
    errs_counter += first_is_empty_sum(out_stream);

    fprintf(out_stream, "second is empty:\n");
    errs_counter += second_is_empty_sum(out_stream);

    fprintf(out_stream, "both are empty:\n");
    errs_counter += both_are_empty_sum(out_stream);

    return errs_counter;
}

int test_make_separation(FILE *const out_stream)
{
    int errs_counter = 0;

    fprintf(out_stream, "\n4) make_separation_testing\n\n");

    fprintf(out_stream, "random polynom:\n");
    errs_counter += random_division(out_stream);

    fprintf(out_stream, "no even term:\n");
    errs_counter += no_even_division(out_stream);

    fprintf(out_stream, "no odd term:\n");
    errs_counter += no_odd_division(out_stream);

    fprintf(out_stream, "zero polynom:\n");
    errs_counter += zero_division(out_stream);

    return errs_counter;
}

int main()
{
    int errs_counter = 0;

    errs_counter += test_get_value(stdout);
    errs_counter += test_get_derivative(stdout);
    errs_counter += test_make_sum(stdout);
    errs_counter += test_make_separation(stdout);

    if (!errs_counter)
    {
        fprintf(stdout, "module testing passed!\n");
        return OK;
    }

    fprintf(stdout, "%d - errors detected!\n", errs_counter);

    return ERROR;
}   