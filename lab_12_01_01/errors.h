#ifndef _ERRORS_H_

#define _ERRORS_H_

#include <stdio.h>

#define OK 0
#define ERROR 1
#define WRONG_TERM_INPUT 200
#define MEM_NOT_ALLOCED 201
#define WRONG_KEY 202
#define WRONG_X_INPUT 203
#define UNDEFINED_TERMS 204
#define NEGATIVE_GRADE 205
#define WRONG_GRADE_SEQUENCE 206

int show_error(FILE *const stream, const int err_code);

#endif