#include <stdio.h>

#define N 10
#define NUM 100
#define WRONG_INPUT 1
#define OK 0
#define ERROR 0
#define CORRECT 1
#define START 1
#define NOT_START 0
#define END 1
#define NOT_END 0

typedef const int c_i;
typedef int matr[N][N];

int input_array(int arr[][N], c_i *const rows, c_i *const cols)
{
    if (*rows > 10 || *cols > 10 || *cols <= 0 || *rows <= 0)
        return ERROR;
    else
    {
        int inputed = 0;
        char symb;
        for (int i = 0; i < *rows; i++)
            for (int j = 0; j < *cols; j++)
            {
                if (scanf("%d", &arr[i][j]))
                    inputed++;
                else
                    return ERROR;
                scanf("%c", &symb);
                if (inputed < (*rows) * (*cols) && inputed % (*cols) == 0 && symb == '\n')
                    continue;
                else if (inputed < (*rows) * (*cols) && symb == '\n')
                    return ERROR;
            }
        return CORRECT;
    }
}

int starting(int element, c_i *const num)
{
    if (element < 0)
        element *= -1;
    while (element > 9)
        element /= 10;
    if (element == *num)
        return START;
    else
        return NOT_START;
}

int ending(int element, c_i *const num)
{
    if (element < 0)
        element *= -1;
    if (element % 10 == *num)
        return END;
    else
        return NOT_END;
}

void add_number(int arr[][N], c_i *const rows, c_i *const cols, c_i *const index, const int number)
{
    for (int i = (*rows) - 1; i > *index; i--)
        for (int j = 0; j < *cols; j++)
            arr[i][j] = arr[i - 1][j];
    for (int i = 0; i < *cols; i++)
        arr[(*index) + 1][i] = number;
}

void add_rows(int arr[][N], int *const rows, c_i *const cols, c_i *const num)
{
    for (int i = 0; i < *rows; i++)
    {
        int num_start = 0;
        int num_end = 0;
        for (int j = 0; j < *cols; j++)
        {
            if (starting(arr[i][j], num))
                num_start++;
            if (ending(arr[i][j], num))
                num_end++;
        }
        if (num_end == num_start && num_start != 0)
        {
            (*rows)++;
            add_number(arr, rows, cols, &i, NUM);
        }
    }
}

void array_output(matr arr, c_i *const rows, c_i *const cols)
{
    for (int i = 0; i < *rows; i++)
    {
        for (int j = 0; j < *cols; j++)
            printf("%d ", arr[i][j]);
        printf("\n");
    }
}

int ok(c_i *const num)
{
    if (*num <= 9 && *num >= 0)
        return CORRECT;
    else 
        return ERROR;
}

int main()
{
    int array[2 * N][N];
    int rows, cols;
    int num;
    if (scanf("%d %d", &rows, &cols) == 2 && input_array(array, &rows, &cols) 
        && scanf("%d", &num) == 1 && ok(&num))
    {
        add_rows(array, &rows, &cols, &num);
        array_output(array, &rows, &cols);
    }
    else
        return WRONG_INPUT;
    return OK;
}
