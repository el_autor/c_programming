#include <stdio.h>

#define N 7

void array_input(int first_array[])
{
    for (int i = 0; i < N; i++)
        scanf("%d", &first_array[i]);
}	

void array_output(int array[], int size)
{
    for (int i = 0; i < size; i++)
    	printf("%d ", array[i]);	    
}

int main()
{
    int first_array[N];
    int second_array[N];
    array_input(first_array);
    if (N % 2 == 0)
    { 
        for (int i = 0; i < N / 2; i++)
	        second_array[i] = first_array[i] * first_array[N - i - 1];
        array_output(second_array, N / 2);  
    }
    else
    {
        for (int i = 0; i < N / 2; i++)
	        second_array[i] = first_array[i] * first_array[N - i - 1]; 
        second_array[N / 2] = first_array[N / 2];
        array_output(second_array, N / 2 + 1);
    }  
    return 0;
}
