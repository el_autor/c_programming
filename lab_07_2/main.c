#include <stdio.h>
#include <string.h>

#define UNKNOWN_ARGS 53
#define OK 0
#define FILE_OPEN_ERROR -1
#define EMPTY_FILE -2
#define NOT_FOUND -3

#define NAME_SIZE 31
#define MFR_SIZE 16
#define SIZE 1000
#define NO 0
#define YES 1

typedef unsigned int u_i;
typedef struct
{
    char name[NAME_SIZE];
    char mfr[MFR_SIZE];
    u_i cost;
    u_i number;
} product;

void fill_structures_txt(FILE *const f, product *const array, int *const size)
{
    *size = 0;
    while (fscanf(f, "%s", array[*size].name) == 1)
    {
        fscanf(f, "%s", array[*size].mfr);
        fscanf(f, "%u", &array[*size].cost);
        fscanf(f, "%u", &array[*size].number);
        (*size)++;
    }
}

void swap_txt(product *const struct_1, product *const struct_2)
{
    product temp = *struct_1;
    *struct_1 = *struct_2;
    *struct_2 = temp;
}

void sort_structs_txt(product *const array, const int size)
{
    for (int i = 0; i < size - 1; i++)
        for (int j = 0; j < size - 1; j++)
            if (array[j].cost < array[j + 1].cost)
                swap_txt(&array[j], &array[j + 1]);
            else if (array[j].cost == array[j + 1].cost && 
                array[j].number < array[j + 1].number)
                swap_txt(&array[j], &array[j + 1]);
}

void file_output_txt(const product *const array, const int size, FILE *const f)
{
    for (int i = 0; i < size; i++)
    {
        fprintf(f, "%s\n", array[i].name);
        fprintf(f, "%s\n", array[i].mfr);
        fprintf(f, "%u\n", array[i].cost);
        fprintf(f, "%u\n", array[i].number);
    }
}

void sort_txt(FILE *const f_in, FILE *const f_out)
{
    product info[SIZE];
    int arr_size;
    fill_structures_txt(f_in, info, &arr_size);
    sort_structs_txt(info, arr_size);
    file_output_txt(info, arr_size, stdout);
    file_output_txt(info, arr_size, f_out);
}

product get_struct(FILE *const f, const int index)
{
    product cur;
    fseek(f, index * sizeof(product), SEEK_SET);
    fread(&cur, sizeof(product), 1, f);
    return cur;
}

void set_struct(FILE *const f, const int index, const product cur)
{
    fseek(f, index * sizeof(product), SEEK_SET);
    fwrite(&cur, sizeof(product), 1, f);
}

int len_file(FILE *const f)
{
    product cur;
    int len = 0;
    while (fread(&cur, sizeof(product), 1, f) == 1)
        len++;
    return len;
}

void swap_bin(FILE *const f, const int pos_1, const int pos_2)
{
    product temp = get_struct(f, pos_1);
    set_struct(f, pos_1, get_struct(f, pos_2));
    set_struct(f, pos_2, temp);
}

void sorting_bin(FILE *const f, const int size)
{
    for (int i = 0; i < size - 1; i++)
        for (int j = 0; j < size - 1; j++)
            if (get_struct(f, j).cost < get_struct(f, j + 1).cost)
                swap_bin(f, j, j + 1);
            else if (get_struct(f, j).cost == get_struct(f, j + 1).cost &&
                get_struct(f, j).number < get_struct(f, j + 1).number)
                swap_bin(f, j, j + 1);
}

void output_bin(FILE *const f_in, const int size, FILE *const f_out)
{
    for (int i = 0; i < size; i++)
    {
        product cur = get_struct(f_in, i);
        fwrite(&cur, sizeof(product), 1, f_out);
    }
}

void sort_bin(FILE *const f_in, FILE *const f_out)
{
    int size = len_file(f_in);
    sorting_bin(f_in, size);
    output_bin(f_in, size, f_out);
}

int is_ending(const char *const string, const char *const substr)
{
    int len_1 = 0, len_2 = 0;
    while (string[len_1++] != '\0');
    while (substr[len_2++] != '\0');
    if (len_2 > len_1)
        return NO;
    for (int i = len_2 - 1, j = len_1 - 1; i >= 0; i--, j--)
        if (substr[i] != string[j])
            return NO;
    return YES;
}

void search_txt(FILE *const f, const char *const substr)
{
    product cur;
    while (fscanf(f, "%s", cur.name) == 1)
    {
        fscanf(f, "%s", cur.mfr);
        fscanf(f, "%u", &cur.cost);
        fscanf(f, "%u", &cur.number);
        if (is_ending(cur.name, substr))
            printf("%s\n%s\n%u\n%u\n", cur.name, cur.mfr, cur.cost, cur.number);
    }
}

void search_bin(FILE *const f, const char *const substr, int *const flag)
{
    product cur;
    while (fread(&cur, sizeof(product), 1, f) == 1)
        if (is_ending(cur.name, substr))
        {
            *flag = 1;
            printf("%s\n%s\n%u\n%u\n", cur.name, cur.mfr, cur.cost, cur.number);
        }
}

product input_new()
{
    product cur;
    memset(&cur, 0, sizeof(product));
    scanf("%s %s %u %u", cur.name, cur.mfr, &cur.cost, &cur.number);
    return cur;
}

void add_txt(const product new, product *const array, int *const size)
{
    int changed = NO;
    for (int i = 0; i < *size; i++)
        if (array[i].cost < new.cost)
        {
            for (int j = *size; j > i; j--)
                array[j] = array[j - 1];
            array[i] = new;
            changed = YES;
            break;
        }
        else if (array[i].cost == new.cost &&
            array[i].number < new.number)
        {
            for (int j = *size; j > i; j--)
                array[j] = array[j - 1];
            array[i] = new;
            changed = YES;
            break;
        }
    if (!changed)
        array[*size] = new;
    (*size)++;
}

void append_new(FILE *const f)
{
    product array[SIZE];
    int size;
    fill_structures_txt(f, array, &size);
    product new = input_new();
    add_txt(new, array, &size);
    rewind(f);
    file_output_txt(array, size, f);
}

/*
void create_binary(char *name)
{
    FILE *f = fopen(name, "wb");
    product cur = {"pon", "china", 200, 100};
    product cur_1 = {"son", "china", 300, 100};
    fwrite(&cur, sizeof(product), 1, f);
    fwrite(&cur_1, sizeof(product), 1, f);
    fclose(f);
}
*/

void add_struct_bin(FILE *const f, const product new)
{
    int size = len_file(f);
    set_struct(f, size, new);
    size++;
    for (int i = 0; i < size - 1; i++)
        if (get_struct(f, i).cost < new.cost)
        {
            for (int j = size - 1; j > i; j--)
                set_struct(f, j, get_struct(f, j - 1));
            set_struct(f, i, new);
            break;
        }
        else if (get_struct(f, i).cost == new.cost &&
            get_struct(f, i).number < new.number)
        {
            for (int j = size - 1; j > i; j--)
                set_struct(f, j, get_struct(f, j - 1));
            set_struct(f, i, new);
            break;
        }
}

void add_bin(FILE *const f)
{
    product new = input_new();
    add_struct_bin(f, new);
}

int main(const int argc, const char *const *const argv)
{
    //char check[NAME_SIZE];
    product current;
    //if (!strcmp(argv[1], "cb"))
    //    create_binary(argv[2]);
    /*if (!strcmp(argv[1], "st") && argc == 4)
    {
        FILE *f_in = fopen(argv[2], "r");
        FILE *f_out = fopen(argv[3], "w");
        if (f_in && f_out)
        {
            if (fscanf(f_in, "%s", check) != EOF)
            {
                rewind(f_in);
                sort_txt(f_in, f_out);
                fclose(f_in);
                fclose(f_out);
                return OK;
            }
            fclose(f_in);
            fclose(f_out);
            return EMPTY_FILE;
        }
        return FILE_OPEN_ERROR;
    }*/
    if (argc < 3)
        return UNKNOWN_ARGS;
    else if (!strcmp(argv[1], "sb") && argc == 4)
    {
        FILE *f_in = fopen(argv[2], "rb+");
        if (f_in)
        {
            if (fread(&current, sizeof(product), 1, f_in) == 1)
            {
                FILE *f_out = fopen(argv[3], "wb");
                rewind(f_in);
                sort_bin(f_in, f_out);
                fclose(f_in);
                fclose(f_out);
                return OK;
            }
            fclose(f_in);
            return EMPTY_FILE;
        }
        return FILE_OPEN_ERROR;
    }
    /*else if (!strcmp(argv[1], "ft") && argc == 4)
    {
        FILE *f_in = fopen(argv[2], "r");
        if (f_in)
        {
            if (fscanf(f_in, "%s", check) != EOF)
            {
                rewind(f_in);
                search_txt(f_in, argv[3]);
                fclose(f_in);
                return OK;
            }
            fclose(f_in);
            return EMPTY_FILE;
        }
        return FILE_OPEN_ERROR;
    }*/
    else if (!strcmp(argv[1], "fb") && argc == 4)
    {
        FILE *f_in = fopen(argv[2], "rb");
        int flag = 0;
        if (f_in)
        {
            if (fread(&current, sizeof(product), 1, f_in) == 1)
            {
                rewind(f_in);
                search_bin(f_in, argv[3], &flag);
                fclose(f_in);
                if (flag)
                    return OK;
                else
                    return NOT_FOUND;
            }
            fclose(f_in);
            return EMPTY_FILE;
        }
        return FILE_OPEN_ERROR;
    }
    /*else if (!strcmp(argv[1], "at") && argc == 3)
    {
        FILE *f_in = fopen(argv[2], "r+");
        if (f_in)
        {
            if (fscanf(f_in, "%s", check) != EOF)
            {
                rewind(f_in);
                append_new(f_in);
                fclose(f_in);
                return OK;
            }
            fclose(f_in);
            return EMPTY_FILE;
        }
        return FILE_OPEN_ERROR;
    }*/
    else if (!strcmp(argv[1], "ab") && argc == 3)
    {
        FILE *f_in = fopen(argv[2], "rb+");
        if (f_in)
        {
            add_bin(f_in);
            fclose(f_in);
            return OK;
        }
        return FILE_OPEN_ERROR;
    }
    else 
        return UNKNOWN_ARGS;
}
