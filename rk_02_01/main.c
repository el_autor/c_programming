#include <stdio.h>

#define NO_FILE 1
#define OK 0
#define SIZE 100

void get(FILE *f, int *size, int *points)
{
    int width;
    fscanf(f, "%d %d %d", size, &width, points);
}

void fill_matrix(FILE *f, int matr[][SIZE])
{
    int i, j, value;
    while (fscanf(f, "%d", &i) != EOF)
    {
        fscanf(f, "%d", &j);
        fscanf(f, "%d", &value);
        matr[i - 1][j - 1] = value;
    }
}

int check_sub(int matr[][SIZE], int str, int col, int size)
{
    int size_cur = 1;
    int max_size = size - str;
    if ((size - col) < max_size)
        max_size = size - col;
    while (size_cur <= max_size)
    {
        for (int i = str; i < str + size_cur; i++)
            for (int j = col; j < col + size_cur; j++)
                if (matr[i][j] != 0)
                    return size_cur - 1;
        size_cur++;
    }
    return size_cur - 1;
}

void filling(int matr[][SIZE], int str, int col, int size, int points)
{
    for (int i = str; i < str + size ; i++)
        for (int j = col; j < col + size; j++)
            matr[i][j] = points;
}

void search_matrix(int matr[][SIZE], int size, int points, int arr[][2], int *pts_size)
{
    int max_empty = 0;
    for (int i = 0; i < size; i++)
        for (int j = 0; j < size; j++)
            if (check_sub(matr, i, j, size) > max_empty)
                max_empty = check_sub(matr, i, j, size);
    for (int i = 0; i < size; i++)
        for (int j = 0; j < size; j++)
            if (check_sub(matr, i, j, size) == max_empty)
            {
                arr[*pts_size][0] = i;
                arr[*pts_size][1] = j;
                (*pts_size)++;
            }
    printf("%d\n", max_empty);
    for (int i = 0; i < *pts_size; i++)
        filling(matr, arr[i][0], arr[i][1], max_empty, points);
}

void file_output(FILE *f, int matr[][SIZE], int size)
{
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
            fprintf(f, "%d", matr[i][j]);
        fprintf(f, "\n");
    }
}

int main(int argc, char **argv)
{
    FILE *f = fopen(argv[1], "r");
    FILE *out = fopen(argv[2], "w");
    if (f && out)
    {
        int pts[SIZE * SIZE][2];
        int points_size = 0;
        int matrix[SIZE][SIZE];
        for (int i = 0; i < SIZE; i++)
            for (int j = 0; j < SIZE; j++)
                matrix[i][j] = 0;
        int size, points;
        get(f, &size, &points);
        fill_matrix(f, matrix);
	//file_output(out, matrix, size);
        search_matrix(matrix, size, points, pts, &points_size);
        file_output(out, matrix, size);
        fclose(f);
        fclose(out);
        return OK;
    }
    return NO_FILE;
}
