#include <stdio.h>

#define OK 0
#define ALL_SEP 1
#define OVERFLOW 2

#define PUNC_START 1
#define NORMAL_START 0
#define MAX_STR 256
#define MAX_WORD 16
#define ALL_SIGNS 8
#define SIGN 1
#define NOT_SIGN 0
#define EQUAL 1
#define NOT_EQUAL 0
#define WORD_PARSED 1
#define NO_WORD 0

typedef const char c_h;

int get_size(const char *const word)
{
    int size = 0;
    while (word[size] != '\0')
    {
        size++;
        if (size == MAX_WORD)
            break;
    }
    return size;
}

int input_str(char *const arr)
{
    int size = 0;
    char symb = getchar();
    while (symb != '\n')
    {
        arr[size] = symb;
        size++;
        symb = getchar();
        if (size == MAX_STR - 1 && symb != '\n')
            return 0;
    }
    arr[size] = '\0';
    return size;
}

int is_sign(const char *const mas, const char symb)
{
    if (symb == '\0')
        return SIGN;
    for (int i = 0; i < ALL_SIGNS; i++)
        if (mas[i] == symb)
            return SIGN;
    return NOT_SIGN;
}

int parse_word(c_h *const str, char *const word, c_h *const punc, const int size, int *const index)
{
    int word_size = 0;
    // Перед словом могут оставаться знаки пунктуации, убираем их
    while (is_sign(punc, str[*index]) && (*index) < size)
        (*index)++;
    if ((*index) < size)
    {
        while (!is_sign(punc, str[*index]))
        {
            word[word_size] = str[*index];
            (*index)++;
            word_size++;
        }
        word[word_size] = '\0';
        return WORD_PARSED;
    }
    else
        return NO_WORD;
}

int equal(c_h *const word1, c_h *const word2)
{
    int len1 = 0;
    int len2 = 0;
    while (word1[len1] != '\0')
        len1++;
    while (word2[len2] != '\0')
        len2++;
    if (len1 != len2)
        return NOT_EQUAL;
    for (int i = 0; i < len1; i++)
        if (word1[i] != word2[i])
            return NOT_EQUAL;
    return EQUAL;
}

int calculate_same(c_h *const str, char *const word, c_h *const punc, const int size)
{
    char check_word[MAX_WORD];
    int total_number = 0;
    int i = 0;
    while (i < size)
    {
        if (parse_word(str, check_word, punc, size, &i))
            if (equal(word, check_word))
                total_number++;
    }
    return total_number;
}

void parse_str(c_h *const arr, c_h *const punctuation, const int size)
{
    char word[MAX_WORD];
    int i = 0;
    while (i < size)
    {       
        if (parse_word(arr, word, punctuation, size, &i) 
            && calculate_same(arr, word, punctuation, i - get_size(word) - 1) == 0)
        {
            int same_count = calculate_same(arr, word, punctuation, size);
            printf("\n%s %d", word, same_count);
        }
    }
}

int check_start(c_h *const str, c_h *const punctuation, const int size)
{
    for (int i = 0; i < size; i++)
        if (!is_sign(punctuation, str[i]))
            return NORMAL_START;
    return PUNC_START;
}

int main()
{
    char str[MAX_STR];
    char punctuation[ALL_SIGNS] = " ,;:-.!?";
    int size = input_str(str);
    if (check_start(str, punctuation, size))
        return ALL_SEP;
    if (size < MAX_STR)
    {
        printf("Result:");
        parse_str(str, punctuation, size);
        return OK;
    }
    else
        return OVERFLOW;
}
