#include <stdio.h>

#define N 10
#define WRONG_INPUT 1
#define OK 0
#define ERROR 0
#define CORRECT 1

typedef const int c_i;
typedef int matr[N][N];

int input_array(int arr[][N], c_i *const rows, c_i *const cols)
{
    if (*rows > 10 || *cols > 10 || *cols <= 0 || *rows <= 0)
        return ERROR;
    else
    {
        int inputed = 0;
        char symb;
        for (int i = 0; i < *rows; i++)
            for (int j = 0; j < *cols; j++)
            {
                if (scanf("%d", &arr[i][j]))
                    inputed++;
                else
                    return ERROR;
                scanf("%c", &symb);
                if (inputed < (*rows) * (*cols) && inputed % (*cols) == 0 && symb == '\n')
                    continue;
                else if (inputed < (*rows) * (*cols) && symb == '\n')
                    return ERROR;
            }
        return CORRECT;
    }
}

int sum(matr arr, c_i *const cols, c_i index)
{
    int total = 0;
    for (int i = 0; i < *cols; i++)
        total += arr[index][i];
    return total;
}

void replace(int arr[][N], c_i *const cols, c_i *const first, c_i second)
{
    for (int i = 0; i < *cols; i++)
    {
        int temp = arr[*first][i];
        arr[*first][i] = arr[second][i];
        arr[second][i] = temp;
    }
}

void sort_array(int arr[][N], c_i *const rows, c_i *const cols)
{
    // Сотрировка пузырьком
    for (int i = 0; i < (*rows) - 1; i++)
        for (int j = 0; j < (*rows) - 1; j++)
            if (sum(arr, cols, j) > sum(arr, cols, j + 1))
                replace(arr, cols, &j, (j + 1));
}

void array_output(matr array, c_i *const rows, c_i *const cols)
{
    for (int i = 0; i < *rows; i++)
    {
        for (int j = 0 ; j < *cols; j++)
            printf("%d ", array[i][j]);
        printf("\n");
    }
}

int main()
{
    int array[N][N];
    int rows, cols;
    if (scanf("%d %d", &rows, &cols) == 2 && input_array(array, &rows, &cols))
    {
        sort_array(array, &rows, &cols);
        array_output(array, &rows, &cols);
    }
    else
        return WRONG_INPUT;
    return OK;
}
