#include <stdio.h>

#define N 10
#define ERROR 0
#define OK 1
#define ZERO 0
#define NO_EVEN 2
#define WRONG_INPUT 1
#define DOWN_LIMIT 0
#define UP_LIMIT 10
#define ALL_OK 0

typedef const int c_i;

// Функция ввода статического массива
int array_input(int *const mas, int *const elems_number)
{
    if (scanf("%d", elems_number) == 1 && *elems_number > DOWN_LIMIT && *elems_number <= UP_LIMIT)
    {
        char sep;
        for (int i = 0; i < *elems_number; i++)
            // Если перенос строки идет после кол-ва эл-тов массива или введеный элемент не число
            if ((i == 0 && scanf("%c", &sep) == 1 && sep != ' ') || scanf("%d", &mas[i]) != 1)
                return ERROR;
            // Если введено меньше элементов, чем ожидалось
            else if (i < *elems_number - 1 && scanf("%c", &sep) == 1 && sep == '\n')
                return ERROR;
            // Если введено больше элементов, чем ожидалось
            else if (i == *elems_number - 1 && scanf("%c", &sep) == 1 && sep != '\n')
            {
                while (sep == ' ')
                    scanf("%c", &sep);
                if (sep != '\n')
                    return ERROR;
            }
    }
    else
        return ERROR;
    return OK;
}

// Функция, для проверки наличия четных чисел
int even_is_here(c_i *const mas, c_i *const elems_number)
{
    for (int i = 0; i < *elems_number; i++)
        if (mas[i] % 2 == 0)
            return OK;
    return ERROR;
}

// Функция подсчета суммы четных элементов
int calc(c_i *const mas, c_i *const elems_number)
{
    int sum = ZERO;
    for (int i = 0; i < *elems_number; i++)
        if (mas[i] % 2 == 0)
            sum += mas[i];
    return sum;
}

int main()
{
    int array[N];
    int elems_number;
    if (array_input(array, &elems_number))
    {
        if (even_is_here(array, &elems_number))
    	{
            printf("%d", calc(array, &elems_number));
	    return ALL_OK;
    	}
        else
        {
            puts("No even elements");
            return NO_EVEN;
        }
    }
    else
    {
        puts("Wrong Input!");
        return WRONG_INPUT;
    }
}
