#include "snprintf.h"

void add_symbs(const char *format, size_t *const format_index, input_params *const situation)
{
    while (format[*format_index] != '%' && format[*format_index] != '\0')
    {
        if (situation->capacity > situation->inputed_symbs)
        {
            *(situation->symb) = format[*format_index];
            (situation->symb)++;
        }

        situation->inputed_symbs++; 
        (*format_index)++;
    }

    if (format[*format_index] == '%')
    {
        (*format_index)++;
    }
}

void change(char *const elem_1, char *const elem_2)
{
    char temp = *elem_1;
    *elem_1 = *elem_2;
    *elem_2 = temp;
}

void make_reverse(char *const start, char *const end)
{
    for (char *i = start; i < start + (end - start) / 2 ; i++)
    {
        change(i, end - (i - start) - 1);
    }
}

void add_int(int value, input_params *const situation)
{
    char *start = situation->symb, *end = NULL;

    while (value)
    {
        if (situation->capacity > situation->inputed_symbs)
        {
            *(situation->symb) = (value % 10) + ASCII_DELTA;
            (situation->symb)++;
        }

        value /= 10;
        situation->inputed_symbs++;
    }

    end = (situation->symb);
    make_reverse(start, end);
}

void add_x_int(unsigned int value, input_params *const situation)
{
    char *start = situation->symb, *end = NULL;
    char hex_numbers[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

    while (value)
    {
        if (situation->capacity > situation->inputed_symbs)
        {
            (*situation->symb) = hex_numbers[value % 16];
            (situation->symb)++;
        }

        value /= 16; 
        situation->inputed_symbs++;
    }

    end = (situation->symb);
    make_reverse(start, end);
}

void get_h_specifier(va_list *const args, const char *format, size_t *const format_index, input_params *const situation)
{
    short int short_value = 0;

    if (format[*format_index] == 'i')
    {
        short_value = va_arg(*args, int);
        add_int(short_value, situation);
        (*format_index)++;
    }
    else if (format[*format_index] == 'x')
    {
        short_value = va_arg(*args, int);
        add_x_int(short_value, situation);
        (*format_index)++;
    }
}

void get_with_specifier(va_list *const args, const char *format, size_t *const format_index, input_params *const situation)
{
    int int_value = 0;
    unsigned int uint_value = 0;

    if (format[*format_index] == 'c')
    {
        int_value = va_arg(*args, int);

        if (situation->capacity > situation->inputed_symbs)
        {
            *(situation->symb) = int_value;
            (situation->symb)++;
        }

        situation->inputed_symbs++;
        (*format_index)++;
    }
    else if (format[*format_index] == 'h')
    {
        (*format_index)++;
        get_h_specifier(args, format, format_index, situation);
    }
    else if (format[*format_index] == 'i')
    {
        int_value = va_arg(*args, int);
        add_int(int_value, situation);
        (*format_index)++; 
    }
    else if (format[*format_index] == 'x')
    {
        uint_value = va_arg(*args, unsigned int);
        add_x_int(uint_value, situation);
        (*format_index)++;
    }
}

int make_result_string(input_params *const situation, char *const source, char *const source_copy)
{
    if (situation->capacity == 0)
    {
        free(source_copy);
        return situation->inputed_symbs;
    }

    if (situation->capacity <= situation->inputed_symbs)
    {
        source_copy[situation->capacity - 1] = '\0';
    }
    else
    {
        *(situation->symb) = '\0';
    }

    strcpy(source, source_copy);
    free(source_copy);
    return situation->inputed_symbs;
}

int my_snprintf(char *source, size_t n, const char *format, ...)
{
    va_list args;
    size_t format_index = 0;
    char *source_copy = malloc(sizeof(char) * n);
    input_params cur_situation = { 0, source_copy, n };

    va_start(args, format);

    while (format[format_index])
    {
        add_symbs(format, &format_index, &cur_situation);

        if (!format[format_index])
        {
            va_end(args);
            return make_result_string(&cur_situation, source, source_copy);
        }

        get_with_specifier(&args, format, &format_index, &cur_situation);
    }

    va_end(args);
    return make_result_string(&cur_situation, source, source_copy);
}