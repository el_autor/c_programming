#include "testing.h"

void test_result(FILE *const stream, const bool inverse, const int test_number)
{
    if (inverse == false)
    {
        if (errno)
        {
            fprintf(stream, "TEST %d.....FAILED\n", test_number);
        }
        else
        {
            fprintf(stream, "TEST %d.....PASSED\n", test_number);
        }
    }
    else
    {
        if (!errno)
        {
            fprintf(stream, "TEST %d.....FAILED\n", test_number);
        }
        else
        {
            fprintf(stream ,"TEST %d.....PASSED\n", test_number);
        }
    }

    errno = OK;
}

int allocate_memory(strings *const data)
{
    int errs_counter = 0;

    if (!(data->dest_my = malloc(MEMORY_SIZE * sizeof(char))))
    {
        show_error(ALLOC_ERROR);
        return ++errs_counter;
    }

    if (!(data->dest_origin = malloc(MEMORY_SIZE * sizeof(char))))
    {
        free(data->dest_my);
        show_error(ALLOC_ERROR);
        return ++errs_counter;
    }

    if (!(data->format = malloc(MEMORY_SIZE * sizeof(char))))
    {
        free(data->dest_my);
        free(data->dest_origin);
        show_error(ALLOC_ERROR);
        return ++errs_counter;
    }

    return errs_counter;
}

void free_struct(const strings data) 
{
    free(data.format);
    free(data.dest_my);
    free(data.dest_origin);
}

void show_error(const int code)
{
    switch (code)
    {
        case WRONG_RETURN_CODE:
            printf("Несовпадение кода возврата с библиотечной функцией\n");
            break;
        case DATA_MISMATCH:
            printf("Результат отличается от библиотечной функции\n");
            break;
        case ALLOC_ERROR:
            printf("Память не была выделена\n");
            break;
    }
}

int is_valid_result(const strings data)
{   
    int errs_counter = 0;

    if (data.return_my != data.return_origin)
    {
        show_error(errno = WRONG_RETURN_CODE);
        errs_counter++;
    }

    if (strcmp(data.dest_my, data.dest_origin))
    {
        show_error(errno = DATA_MISMATCH);
        errs_counter++;
    }

    return errs_counter;
}

int one_c_spec(strings data)
{  
    int errs_counter = 0;

    data.symb = 'I';
    strcpy(data.format, "mama%cname");
    data.return_my = my_snprintf(data.dest_my, 50, data.format, data.symb);
    data.return_origin = snprintf(data.dest_origin, 50, data.format, data.symb);
    //printf("%d - %d\n", return_my, return_origin);
    //printf("%s - %s\n", dest_my, dest_origin);
    errs_counter += is_valid_result(data);
    test_result(stdout, false, 1);

    return errs_counter;
}

int flangs_c_spec(strings data)
{
    int errs_counter = 0;

    data.symb = 'I';
    strcpy(data.format, "%cmamaname%c");
    data.return_my = my_snprintf(data.dest_my, 50, data.format, data.symb, data.symb + 1);
    data.return_origin = snprintf(data.dest_origin, 50, data.format, data.symb, data.symb + 1);
    errs_counter += is_valid_result(data);
    test_result(stdout, false, 2);

    return errs_counter;
}

int split_c_spec(strings data)
{
    int errs_counter = 0;

    data.symb = 'I';
    strcpy(data.format, "mama%c%c%cname");
    data.return_my = my_snprintf(data.dest_my, 50, data.format, data.symb, data.symb + 1, data.symb + 2);
    data.return_origin = snprintf(data.dest_origin, 50, data.format, data.symb, data.symb + 1, data.symb + 2);
    errs_counter += is_valid_result(data);
    test_result(stdout, false, 3);


    return errs_counter;
}

int zero_capability_c_spec(strings data)
{
    int errs_counter = 0;

    data.symb = 'I';
    strcpy(data.format, "%cmamaname");
    data.return_my = my_snprintf(data.dest_my, 0, data.format, data.symb);
    data.return_origin = snprintf(data.dest_origin, 0, data.format, data.symb);
    errs_counter += is_valid_result(data);
    test_result(stdout, false, 4);

    return errs_counter;
}

int one_hi_spec(strings data)
{  
    int errs_counter = 0;

    data.number = 100;
    strcpy(data.format, "mama%hifood");
    data.return_my = my_snprintf(data.dest_my, 50, data.format, data.number);
    data.return_origin = snprintf(data.dest_origin, 50, data.format, data.number);
    errs_counter += is_valid_result(data);
    test_result(stdout, false, 1);

    return errs_counter;
}

int flangs_hi_spec(strings data)
{
    int errs_counter = 0;

    data.number = 100;
    strcpy(data.format, "%himamaname%hi");
    data.return_my = my_snprintf(data.dest_my, 50, data.format, data.number, data.number + 1);
    data.return_origin = snprintf(data.dest_origin, 50, data.format, data.number, data.number + 1);
    errs_counter += is_valid_result(data);
    test_result(stdout, false, 2);

    return errs_counter;
}

int split_hi_spec(strings data)
{
    int errs_counter = 0;

    data.number = 100;
    strcpy(data.format, "mama%hi%hi%hiname");
    data.return_my = my_snprintf(data.dest_my, 50, data.format, data.number, data.number + 1, data.number + 2);
    data.return_origin = snprintf(data.dest_origin, 50, data.format, data.number, data.number + 1, data.number + 2);
    errs_counter += is_valid_result(data);
    test_result(stdout, false, 3);

    return errs_counter;
}

int zero_capability_hi_spec(strings data)
{
    int errs_counter = 0;

    data.number = 100;
    strcpy(data.format, "%himamaname");
    data.return_my = my_snprintf(data.dest_my, 0, data.format, data.number);
    data.return_origin = snprintf(data.dest_origin, 0, data.format, data.number);
    errs_counter += is_valid_result(data);
    test_result(stdout, false, 4);

    return errs_counter;
}

int one_i_spec(strings data)
{  
    int errs_counter = 0;

    data.number = 100;
    strcpy(data.format, "mama%ifood");
    data.return_my = my_snprintf(data.dest_my, 50, data.format, data.number);
    data.return_origin = snprintf(data.dest_origin, 50, data.format, data.number);
    errs_counter += is_valid_result(data);
    test_result(stdout, false, 1);

    return errs_counter;
}

int flangs_i_spec(strings data)
{
    int errs_counter = 0;

    data.number = 100;
    strcpy(data.format, "%imamaname%i");
    data.return_my = my_snprintf(data.dest_my, 50, data.format, data.number, data.number + 1);
    data.return_origin = snprintf(data.dest_origin, 50, data.format, data.number, data.number + 1);
    errs_counter += is_valid_result(data);
    test_result(stdout, false, 2);

    return errs_counter;
}

int split_i_spec(strings data)
{
    int errs_counter = 0;

    data.number = 100;
    strcpy(data.format, "mama%i%i%iname");
    data.return_my = my_snprintf(data.dest_my, 50, data.format, data.number, data.number + 1, data.number + 2);
    data.return_origin = snprintf(data.dest_origin, 50, data.format, data.number, data.number + 1, data.number + 2);
    errs_counter += is_valid_result(data);
    test_result(stdout, false, 3);

    return errs_counter;
}

int zero_capability_i_spec(strings data)
{
    int errs_counter = 0;

    data.number = 100;
    strcpy(data.format, "%imamaname");
    data.return_my = my_snprintf(data.dest_my, 0, data.format, data.number);
    data.return_origin = snprintf(data.dest_origin, 0, data.format, data.number);
    errs_counter += is_valid_result(data);
    test_result(stdout, false, 4);

    return errs_counter;
}

int one_hx_spec(strings data)
{  
    int errs_counter = 0;

    data.number = 100;
    strcpy(data.format, "mama%hxfood");
    data.return_my = my_snprintf(data.dest_my, 50, data.format, data.number);
    data.return_origin = snprintf(data.dest_origin, 50, data.format, data.number);
    errs_counter += is_valid_result(data);
    test_result(stdout, false, 1);

    return errs_counter;
}

int flangs_hx_spec(strings data)
{
    int errs_counter = 0;

    data.number = 100;
    strcpy(data.format, "%hxmamaname%hx");
    data.return_my = my_snprintf(data.dest_my, 50, data.format, data.number, data.number + 1);
    data.return_origin = snprintf(data.dest_origin, 50, data.format, data.number, data.number + 1);
    errs_counter += is_valid_result(data);
    test_result(stdout, false, 2);

    return errs_counter;
}

int split_hx_spec(strings data)
{
    int errs_counter = 0;

    data.number = 100;
    strcpy(data.format, "mama%hx%hx%hxname");
    data.return_my = my_snprintf(data.dest_my, 50, data.format, data.number, data.number + 1, data.number + 2);
    data.return_origin = snprintf(data.dest_origin, 50, data.format, data.number, data.number + 1, data.number + 2);
    errs_counter += is_valid_result(data);
    test_result(stdout, false, 3);

    return errs_counter;
}

int zero_capability_hx_spec(strings data)
{
    int errs_counter = 0;

    data.number = 100;
    strcpy(data.format, "%hxmamaname");
    data.return_my = my_snprintf(data.dest_my, 0, data.format, data.number);
    data.return_origin = snprintf(data.dest_origin, 0, data.format, data.number);
    errs_counter += is_valid_result(data);
    test_result(stdout, false, 4);

    return errs_counter;
}

int one_x_spec(strings data)
{  
    int errs_counter = 0;

    data.number = 100;
    strcpy(data.format, "mama%xfood");
    data.return_my = my_snprintf(data.dest_my, 50, data.format, data.number);
    data.return_origin = snprintf(data.dest_origin, 50, data.format, data.number);
    errs_counter += is_valid_result(data);
    test_result(stdout, false, 1);

    return errs_counter;
}

int flangs_x_spec(strings data)
{
    int errs_counter = 0;

    data.number = 100;
    strcpy(data.format, "%xmamaname%x");
    data.return_my = my_snprintf(data.dest_my, 50, data.format, data.number, data.number + 1);
    data.return_origin = snprintf(data.dest_origin, 50, data.format, data.number, data.number + 1);
    errs_counter += is_valid_result(data);
    test_result(stdout, false, 2);

    return errs_counter;
}

int split_x_spec(strings data)
{
    int errs_counter = 0;

    data.number = 100;
    strcpy(data.format, "mama%x%x%xname");
    data.return_my = my_snprintf(data.dest_my, 50, data.format, data.number, data.number + 1, data.number + 2);
    data.return_origin = snprintf(data.dest_origin, 50, data.format, data.number, data.number + 1, data.number + 2);
    errs_counter += is_valid_result(data);
    test_result(stdout, false, 3);

    return errs_counter;
}

int zero_capability_x_spec(strings data)
{
    int errs_counter = 0;

    data.number = 100;
    strcpy(data.format, "%xmamaname");
    data.return_my = my_snprintf(data.dest_my, 0, data.format, data.number);
    data.return_origin = snprintf(data.dest_origin, 0, data.format, data.number);
    errs_counter += is_valid_result(data);
    test_result(stdout, false, 4);

    return errs_counter;
}

int c_spec_testing(const strings data)
{
    int errs_counter = 0;

    printf("1) с_spec_testing:\n\n");

    printf("One specifier:\n");
    errs_counter += one_c_spec(data);

    printf("Specifier on flangs:\n");
    errs_counter += flangs_c_spec(data);

    printf("Split specifier:\n");
    errs_counter += split_c_spec(data);

    printf("Zero capability:\n");
    errs_counter += zero_capability_c_spec(data);
    
    printf("\n");
    return errs_counter;
}

int hi_spec_testing(const strings data)
{
    int errs_counter = 0;

    printf("2) hi_spec_testing:\n\n");

    printf("One specifier:\n");
    errs_counter += one_hi_spec(data);

    printf("Specifier on flangs:\n");
    errs_counter += flangs_hi_spec(data);

    printf("Split specifier:\n");
    errs_counter += split_hi_spec(data);

    printf("Zero capability:\n");
    errs_counter += zero_capability_hi_spec(data);
    
    printf("\n");
    return errs_counter;
}

int i_spec_testing(const strings data)
{
    int errs_counter = 0;

    printf("3) i_spec_testing:\n\n");

    printf("One specifier:\n");
    errs_counter += one_i_spec(data);

    printf("Specifier on flangs:\n");
    errs_counter += flangs_i_spec(data);

    printf("Split specifier:\n");
    errs_counter += split_i_spec(data);

    printf("Zero capability:\n");
    errs_counter += zero_capability_i_spec(data);
    
    printf("\n");
    return errs_counter;
}

int hx_spec_testing(const strings data)
{
    int errs_counter = 0;

    printf("4) hx_spec_testing:\n\n");

    printf("One specifier:\n");
    errs_counter += one_hx_spec(data);

    printf("Specifier on flangs:\n");
    errs_counter += flangs_hx_spec(data);

    printf("Split specifier:\n");
    errs_counter += split_hx_spec(data);

    printf("Zero capability:\n");
    errs_counter += zero_capability_hx_spec(data);
    
    printf("\n");
    return errs_counter;
}

int x_spec_testing(const strings data)
{
    int errs_counter = 0;

    printf("5) x_spec_testing:\n\n");

    printf("One specifier:\n");
    errs_counter += one_x_spec(data);

    printf("Specifier on flangs:\n");
    errs_counter += flangs_x_spec(data);

    printf("Split specifier:\n");
    errs_counter += split_x_spec(data);

    printf("Zero capability:\n");
    errs_counter += zero_capability_x_spec(data);
    
    printf("\n");
    return errs_counter;
}

int main()
{
    int errs_counter = 0;
    strings streams = { NULL, NULL, NULL, 0, 0, 0, 0 };

    if (allocate_memory(&streams) != OK)
    {
        show_error(ALLOC_ERROR);
        return ALLOC_ERROR;
    }

    errs_counter += c_spec_testing(streams);
    errs_counter += hi_spec_testing(streams);
    errs_counter += i_spec_testing(streams);
    errs_counter += hx_spec_testing(streams);
    errs_counter += x_spec_testing(streams);
    free_struct(streams);

    if (errs_counter)
    {
        printf("%d - errors detected\n", errs_counter);
        return errs_counter;
    }

    printf("module testing passed\n");
    return OK;
}