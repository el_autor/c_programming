#ifndef _TESTING_H_

#define _TESTING_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <errno.h>
#include "snprintf.h"

#define MEMORY_SIZE 50

#define OK 0
#define WRONG_RETURN_CODE 501
#define DATA_MISMATCH 502
#define ALLOC_ERROR 503


typedef struct
{
    char *format;
    char *dest_my;
    char *dest_origin;
    int return_my;
    int return_origin;
    char symb;
    int number;
} strings;

void show_error(const int code);

#endif