#ifndef _SNPRINTF_H_

#define _SNPRINTF_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#define OK 0
#define ERRORS_DETECTED 1
#define ASCII_DELTA 48

typedef struct
{
    size_t inputed_symbs;
    char *symb;
    size_t capacity;
} input_params;

int my_snprintf(char *s, size_t n, const char *format, ...);

#endif 