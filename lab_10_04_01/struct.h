#ifndef _STRUCT_H_

#define _STRUCT_H_

typedef struct
{
    char *group;
    char *surname;
    char *birth;
    int total_marks;
    double *marks;
} student_data;

#endif