#ifndef _SHOW_H_

#define _SHOW_H_

#include "struct.h"
int file_output(const char *const filename, const student_data *const data, const int size);

#endif