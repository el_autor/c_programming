#include "testing.h"

int test_result(const bool inverse, const int test_number)
{
    bool flag;

    if (inverse == false)
    {
        if (errno)
        {
            printf("TEST %d.....FAILED\n", test_number);
            flag = true;
        }
        else
        {
            printf("TEST %d.....PASSED\n", test_number);
            flag = false;
        }
    }
    else
    {
        if (!errno)
        {
            printf("TEST %d.....FAILED\n", test_number);
            flag = true;
        }
        else
        {
            printf("TEST %d.....PASSED\n", test_number);
            flag = false;
        }
    }

    errno = OK;
    return flag;
}

int fill_file(const char *const filename, const char *const message)
{
    FILE *file = fopen(filename, "w");
    if (file)
    {
        fprintf(file, "%s", message);
        fclose(file);
        return OK;
    }

    return ERRORS_DETECTED; 
}

int one_record_input()
{
    int errs_counter = 0;
    int data_size = 0;
    student_data *record = NULL;
    FILE *stream = NULL;

    if (fill_file(TEST_FILE, "ИУ7-31Б\n PES\n 2000.04.01\n 2\n 1.0 2.1\n none\n") == OK)
    {
        if ((stream = fopen(TEST_FILE, "r")))
        {
            if ((record = input_data(stream, &data_size)))
            {
                if (!(data_size == 1) || strcmp(record->group, "ИУ7-31Б") || strcmp(record->surname, "PES")\
                    || strcmp(record->birth, "2000.04.01") || record->total_marks != 2\
                    || (fabs(record->marks[0] - 1.0) > EPS) || fabs(record->marks[1] - 2.1) > EPS)
                {
                    errno = DATA_MISMATCH;
                    printf("Несовпадение данных\n");
                }
            
                free_all(record, data_size);
            }
            else
            {
                printf("Неверно введены данные\n");
            }

            fclose(stream);
        }
        else
        {
            printf("Файл c тестовыми данными не открыт\n");
        }
    }
    else
    {
        errno = FILE_NOT_FILLED; 
        printf("Файл не заполнен тестовыми данными\n");
    }

    errs_counter += test_result(false, 1);
   
    return errs_counter;
}

int two_record_input()
{
    int errs_counter = 0;
    int data_size = 0;
    student_data *record = NULL;
    FILE *stream = NULL; 

    if (fill_file(TEST_FILE, "ИУ7-31Б\n PES\n 2000.04.01\n 2\n 1.0 2.1\n ИУ8-34Б\n KOT\n 0700.01.02\n 1\n 1.1\n none\n") == OK)
    {
        if ((stream = fopen(TEST_FILE, "r")))
        {
            if ((record = input_data(stream, &data_size)))
            {
                if (data_size != 2)
                {
                    errno = DATA_MISMATCH;
                    printf("Несовпадение количества считанных записей\n"); 
                }
            
                free_all(record, data_size);
            }
            else
            {
                printf("Неверно введены данные\n");
            }

            fclose(stream);
        }
        else
        {
            printf("Файл c тестовыми данными не открыт\n");
        }
    }
    else
    {
        errno = FILE_NOT_FILLED;
        printf("Файл не заполнен тестовыми данными\n");
    }

    errs_counter += test_result(false, 2);

    return errs_counter;
}

int wrong_marks_number()
{
    int errs_counter = 0;
    int data_size = 0;
    student_data *record = NULL;
    FILE *stream = NULL; 

    if (fill_file(TEST_FILE, "ИУ7-31Б\n ПЕС\n 01.04.2000\n 0\n 1.0 2.1\n none\n") == OK)
    {
        if ((stream = fopen(TEST_FILE, "r")))
        {
            if ((record = input_data(stream, &data_size)))
            {
                printf("Cчитаны неверные данные\n");
            }

            fclose(stream);
        }
        else
        {
            printf("Файл c тестовыми данными не открыт\n");
        }
    }
    else
    {
        errno = FILE_NOT_FILLED;
        printf("Файл не заполнен тестовыми данными\n");
    }

    errs_counter += test_result(true, 3);

    return errs_counter;
}

int negative_mark()
{
    int errs_counter = 0;
    int data_size = 0;
    student_data *record = NULL;
    FILE *stream = NULL; 

    if (fill_file(TEST_FILE, "ИУ7-31Б\n ПЕС\n 01.04.2000\n 2\n -1.0 2.1\n none\n") == OK)
    {
        if ((stream = fopen(TEST_FILE, "r")))
        {
            if ((record = input_data(stream, &data_size)))
            {
                printf("Cчитаны неверные данные\n");
            }

            fclose(stream);
        }
        else
        {
            printf("Файл c тестовыми данными не открыт\n");
        }
    }
    else
    {
        errno = FILE_NOT_FILLED;
        printf("Файл не заполнен тестовыми данными\n");
    }

    errs_counter += test_result(true, 4); 

    return errs_counter;
}

int wrong_date_format_1()
{
    int errs_counter = 0;
    int data_size = 0;
    student_data *record = NULL;
    FILE *stream = NULL; 

    if (fill_file(TEST_FILE, "ИУ7-31Б\n ПЕС\n 0104.2000\n 2\n 1.0 2.1\n none\n") == OK)
    {
        if ((stream = fopen(TEST_FILE, "r")))
        {
            if ((record = input_data(stream, &data_size)))
            {
                printf("Cчитаны неверные данные\n");
            }

            fclose(stream);
        }
        else
        {
            printf("Файл c тестовыми данными не открыт\n");
        }
    }
    else
    {
        errno = FILE_NOT_FILLED;
        printf("Файл не заполнен тестовыми данными\n");
    }

    errs_counter += test_result(true, 5);

    return errs_counter;
}

int wrong_date_format_2()
{
    int errs_counter = 0;
    int data_size = 0;
    student_data *record = NULL;
    FILE *stream = NULL;

    if (fill_file(TEST_FILE, "ИУ7-31Б\n ПЕС\n 01.04.2000.\n 2\n 1.0 2.1\n none\n") == OK)
    {
        if ((stream = fopen(TEST_FILE, "r")))
        {
            if ((record = input_data(stream, &data_size)))
            {
                printf("Cчитаны неверные данные\n");
            }

            fclose(stream);
        }
        else
        {
            printf("Файл c тестовыми данными не открыт\n");
        }
    }
    else
    {
        errno = FILE_NOT_FILLED;
        printf("Файл не заполнен тестовыми данными\n");
    }

    errs_counter += test_result(true, 6);

    return errs_counter;
}

int wrong_month()
{
    int errs_counter = 0;
    int data_size = 0;
    student_data *record = NULL;
    FILE *stream = NULL; 

    if (fill_file(TEST_FILE, "ИУ7-31Б\n ПЕС\n 01.13.2000\n 2\n 1.0 2.1\n none\n") == OK)
    {
        if ((stream = fopen(TEST_FILE, "r")))
        {
            if ((record = input_data(stream, &data_size)))
            {
                printf("Cчитаны неверные данные\n");
            }

            fclose(stream);
        }
        else
        {
            printf("Файл c тестовыми данными не открыт\n");
        }
    }
    else
    {
        errno = FILE_NOT_FILLED;
        printf("Файл не заполнен тестовыми данными\n");
    }

    errs_counter += test_result(true, 7);

    return errs_counter;
}

int not_full_record()
{
    int errs_counter = 0;
    int data_size = 0;
    student_data *record = NULL;
    FILE *stream = NULL;

    if (fill_file(TEST_FILE, "ИУ7-31Б\n") == OK)
    {
        if ((stream = fopen(TEST_FILE, "r")))
        {
            if ((record = input_data(stream, &data_size)))
            {
                printf("Cчитаны неверные данные\n");
            }

            fclose(stream);
        }
        else
        {
            printf("Файл c тестовыми данными не открыт\n");
        }
    }
    else
    {
        errno = FILE_NOT_FILLED;
        printf("Файл не заполнен тестовыми данными\n");
    }

    errs_counter += test_result(true, 8);

    return errs_counter;
}

int empty_input()
{
    int errs_counter = 0;
    int data_size = 0;
    student_data *record = NULL;
    FILE *stream = NULL; 

    if (fill_file(TEST_FILE, "") == OK)
    {
        if ((stream = fopen(TEST_FILE, "r")))
        {
            if ((record = input_data(stream, &data_size)))
            {
                printf("Cчитаны неверные данные\n");
            }

            fclose(stream);
        }
        else
        {
            printf("Файл c тестовыми данными не открыт\n");
        }
    }
    else
    {
        errno = FILE_NOT_FILLED;
        printf("Файл не заполнен тестовыми данными\n");
    }

    errs_counter += test_result(true, 9);

    return errs_counter;
}

int not_full_record_2()
{
    int errs_counter = 0;
    int data_size = 0;
    student_data *record = NULL;
    FILE *stream = NULL; 

    if (fill_file(TEST_FILE, "ИУ7-31Б\n ПЕС\n 01.12.2000") == OK)
    {
        if ((stream = fopen(TEST_FILE, "r")))
        {
            if ((record = input_data(stream, &data_size)))
            {
                printf("Cчитаны неверные данные\n");
            }

            fclose(stream);
        }
        else
        {
            printf("Файл c тестовыми данными не открыт\n");
        }
    }
    else
    {
        errno = FILE_NOT_FILLED;
        printf("Файл не заполнен тестовыми данными\n");
    }

    errs_counter += test_result(true, 10);

    return errs_counter;
}

int not_full_record_3()
{
    int errs_counter = 0;
    int data_size = 0;
    student_data *record = NULL;
    FILE *stream = NULL; 

    if (fill_file(TEST_FILE, "ИУ7-31Б\n ПЕС\n 01.12.2000\n 2\n") == OK)
    {
        if ((stream = fopen(TEST_FILE, "r")))
        {
            if ((record = input_data(stream, &data_size)))
            {
                printf("Cчитаны неверные данные\n");
            }

            fclose(stream);
        }
        else
        {
            printf("Файл c тестовыми данными не открыт\n");
        }
    }
    else
    {
        errno = FILE_NOT_FILLED;
        printf("Файл не заполнен тестовыми данными\n");
    }

    errs_counter += test_result(true, 11);

    return errs_counter;
}

int not_full_record_4()
{
    int errs_counter = 0;
    int data_size = 0;
    student_data *record = NULL;
    FILE *stream = NULL; 

    if (fill_file(TEST_FILE, "ИУ7-31Б\n ПЕС\n 01.12.2000\n 2\n 1.0 2.1\n") == OK)
    {
        if ((stream = fopen(TEST_FILE, "r")))
        {
            if ((record = input_data(stream, &data_size)))
            {
                printf("Cчитаны неверные данные\n");
            }

            fclose(stream);
        }
        else
        {
            printf("Файл c тестовыми данными не открыт\n");
        }
    }
    else
    {
        errno = FILE_NOT_FILLED;
        printf("Файл не заполнен тестовыми данными\n");
    }

    errs_counter += test_result(true, 12);
    return errs_counter;
}

int record_are_equal(const student_data source, const student_data dest)
{
    if (strcmp(source.group, dest.group))
    {
        return ERRORS_DETECTED;
    }

    if (strcmp(source.birth, dest.birth))
    {
        return ERRORS_DETECTED;
    }

    if (strcmp(source.surname, dest.surname))
    {
        return ERRORS_DETECTED;
    }

    if (source.total_marks != dest.total_marks)
    {
        return ERRORS_DETECTED;
    }

    for (int i = 0; i < source.total_marks; i++)
    {
        if (fabs(source.marks[i] - dest.marks[i]) > EPS)
        {
            return ERRORS_DETECTED;
        }
    }

    return OK;
}

int data_are_equal(const student_data *const source, const student_data *const destination, const int size)
{
    for (int i = 0; i < size; i++)
    {
        if (record_are_equal(source[i], destination[i]) != OK)
        {
            return ERRORS_DETECTED;
        }
    }

    return OK;
}

int valid_result(const int key)
{
    int source_size = 0, dest_size = 0;
    student_data *source = NULL, *destination = NULL;
    FILE *f_source = NULL, *f_destination = NULL;

    if (!(f_source = fopen(TEST_FILE, "r")))
    {
        return ERRORS_DETECTED;
    }

    if (!(f_destination = fopen(RESULT_FILE, "r")))
    {
        fclose(f_source);
        return ERRORS_DETECTED;
    }

    if (!(source = input_data(f_source, &source_size)))
    {
        return ERRORS_DETECTED;
    }

    if (!(destination = input_data(f_destination, &dest_size)))
    {
        fclose(f_source);
        return ERRORS_DETECTED;
    }

    fclose(f_destination);

    switch (key)
    {
        case STUDENTS:
            if (!(source = remove_students(source, &source_size)))
            {
                fclose(f_source);
                printf("Ошибки при удалении записей\n");
            }

            if (source_size != dest_size)
            {
                fclose(f_source);
                printf("Несовпадение данных\n");
                errno = DATA_MISMATCH;
                return ERRORS_DETECTED;
            }
            break;
        case MARKS:
            remove_marks(source, source_size);
            break;        
        case SORT:
            if (sort(f_source, source, source_size) != OK)
            {
                fclose(f_source);
                return (errno = KEY_INPUT_ERROR);
            }
    }
    
    fclose(f_source);

    if (data_are_equal(source, destination, source_size) != OK)
    {
        printf("Несовпадение данных\n");
        errno = DATA_MISMATCH;
        return ERRORS_DETECTED;
    }

    free_all(source, source_size);
    free_all(destination, dest_size);
      
    return OK;
}

int delete_one_record()
{
    int errs_counter = 0;

    if (fill_file(TEST_FILE, INPUT_2_1) == OK && fill_file(RESULT_FILE, RESULT_2_1) == OK)
    {
        if (valid_result(STUDENTS) != OK)
        {
            errno = DATA_MISMATCH;
        }
    }
    else
    {
        errno = FILE_NOT_FILLED;
        printf("Файл не заполнен тестовыми данными\n");
    }
    
    errs_counter += test_result(false, 1);

    return errs_counter;
}

int delete_two_records()
{
    int errs_counter = 0;

    if (fill_file(TEST_FILE, INPUT_2_2) == OK && fill_file(RESULT_FILE, RESULT_2_2) == OK)
    {
        if (valid_result(STUDENTS) != OK)
        {
            errno = DATA_MISMATCH;
        }
    }
    else
    {
        errno = FILE_NOT_FILLED;
        printf("Файл не заполнен тестовыми данными\n");
    }
    
    errs_counter += test_result(false, 2);      

    return errs_counter;
}

int no_deletion()
{
    int errs_counter = 0;

    if (fill_file(TEST_FILE, INPUT_2_3) == OK && fill_file(RESULT_FILE, RESULT_2_3) == OK)
    {
        if (valid_result(STUDENTS) != OK)
        {
            errno = DATA_MISMATCH;
        }
    }
    else
    {
        errno = FILE_NOT_FILLED;
        printf("Файл не заполнен тестовыми данными\n");
    }
    
    errs_counter += test_result(false, 3);   

    return errs_counter;
}

int nothing_to_delete()
{
    int errs_counter = 0;

    if (fill_file(TEST_FILE, INPUT_3_1) == OK && fill_file(RESULT_FILE, RESULT_3_1) == OK)
    {
        if (valid_result(MARKS) != OK)
        {
            errno = DATA_MISMATCH;
        }
    }
    else
    {
        errno = FILE_NOT_FILLED;
        printf("Файл не заполнен тестовыми данными\n");
    }
    
    errs_counter += test_result(false, 1);

    return errs_counter;
}

int remove_1_from_one()
{
    int errs_counter = 0;

    if (fill_file(TEST_FILE, INPUT_3_2) == OK && fill_file(RESULT_FILE, RESULT_3_2) == OK)
    {
        if (valid_result(MARKS) != OK)
        {
            errno = DATA_MISMATCH;
        }
    }
    else
    {
        errno = FILE_NOT_FILLED;
        printf("Файл не заполнен тестовыми данными\n");
    }
    
    errs_counter += test_result(false, 2);
    
    return errs_counter;
}

int remove_1_from_all()
{
    int errs_counter = 0;

    if (fill_file(TEST_FILE, INPUT_3_3) == OK && fill_file(RESULT_FILE, RESULT_3_3) == OK)
    {
        if (valid_result(MARKS) != OK)
        {
            errno = DATA_MISMATCH;
        }
    }
    else
    {
        errno = FILE_NOT_FILLED;
        printf("Файл не заполнен тестовыми данными\n");
    }
    
    errs_counter += test_result(false, 3);

    return errs_counter;    
}

int group_sorting()
{
    int errs_counter = 0;

    if (fill_file(TEST_FILE, INPUT_4_1) == OK && fill_file(RESULT_FILE, RESULT_4_1) == OK)
    {
        if (valid_result(SORT) != OK)
        {
            errno = DATA_MISMATCH;
        }
    }
    else
    {
        errno = FILE_NOT_FILLED;
        printf("Файл не заполнен тестовыми данными\n");
    }
    
    errs_counter += test_result(false, 1);
    
    return errs_counter;
}

int surname_sorting()
{
    int errs_counter = 0;

    if (fill_file(TEST_FILE, INPUT_4_2) == OK && fill_file(RESULT_FILE, RESULT_4_2) == OK)
    {
        if (valid_result(SORT) != OK)
        {
            errno = DATA_MISMATCH;
        }
    }
    else
    {
        errno = FILE_NOT_FILLED;
        printf("Файл не заполнен тестовыми данными\n");
    }
    
    errs_counter += test_result(false, 2);
    
    return errs_counter;
}

int grade_sorting()
{
    int errs_counter = 0;

    if (fill_file(TEST_FILE, INPUT_4_3) == OK && fill_file(RESULT_FILE, RESULT_4_3) == OK)
    {
        if (valid_result(SORT) != OK)
        {
            errno = DATA_MISMATCH;
        }
    }
    else
    {
        errno = FILE_NOT_FILLED;
        printf("Файл не заполнен тестовыми данными\n");
    }
    
    errs_counter += test_result(false, 3);
    
    return errs_counter;
}

int input_data_testing()
{
    int errs_counter = 0;

    printf("1) input_data testing:\n\n");

    printf("One record input: \n");
    errs_counter += one_record_input();

    printf("Two records input: \n");
    errs_counter += two_record_input();   

    printf("Wrong marks number: \n");
    errs_counter += wrong_marks_number();

    printf("Negative mark: \n");
    errs_counter += negative_mark();

    printf("Wrong date format - one point: \n");
    errs_counter += wrong_date_format_1();

    printf("Wrong date format - point at end: \n");
    errs_counter += wrong_date_format_2();

    printf("Wrong month: \n");
    errs_counter += wrong_month();

    printf("Not full record - only group: \n");
    errs_counter += not_full_record();

    printf("Empty input: \n");
    errs_counter += empty_input();

    printf("Not full record - no marks number and marks: \n");
    errs_counter += not_full_record_2();

    printf("Not full record - no marks: \n");
    errs_counter += not_full_record_3();

    printf("Not full record - no <none> word: \n");
    errs_counter += not_full_record_4();

    printf("\n");

    return errs_counter;
}

int remove_students_testing()
{
    int errs_counter = 0;

    printf("2) remove_students testing\n\n");

    printf("Delete one record: \n");
    errs_counter += delete_one_record();

    printf("Delete two records: \n");
    errs_counter += delete_two_records();

    printf("No deletion: \n");
    errs_counter += no_deletion();

    printf("\n");

    return errs_counter;
}

int remove_marks_testing()
{
    int errs_counter = 0;

    printf("3) remove_marks testing\n\n");

    printf("Nothing to delete: \n");
    errs_counter += nothing_to_delete();

    printf("Remove 1 mark from one record: \n");
    errs_counter += remove_1_from_one();

    printf("Remove 1 mark from all records: \n");
    errs_counter += remove_1_from_all();

    printf("\n");

    return errs_counter;
}

int sort_testing()
{
    int errs_counter = 0;

    printf("4) sort testing\n\n");

    printf("Group sorting: \n");
    errs_counter += group_sorting();

    printf("Surname sorting: \n");
    errs_counter += surname_sorting();

    printf("Grade sorting: \n");
    errs_counter += grade_sorting();

    printf("\n");
    
    return errs_counter;
}

int main()
{
    int errs_counter = 0;

    errs_counter += input_data_testing();
    errs_counter += remove_students_testing();
    errs_counter += remove_marks_testing();
    errs_counter += sort_testing();

    if (!errs_counter)
    {
        printf("module testing passed!\n");
        return OK;
    }   
    
    printf("%d - errors detected\n", errs_counter);

    return ERRORS_DETECTED;
}