#!/bin/zsh

make ../app.exe

for ((i = 1; i <= 5; i++))
do
    rm students.txt
    touch students.txt
    ../app.exe < in_$i.txt
    if (diff -w -q out_$i.txt students.txt)
    then 
        echo "Test $i.....PASSED"
    else
        echo "Test $i.....FAILED"
    fi
done
