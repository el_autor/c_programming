#ifndef _APP_H_

#define _APP_H_

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include "struct.h"
#include "remove.h"
#include "input.h"
#include "show.h"
#include "free.h"
#include "keys.h"
#include "sort.h"
#include "errors.h"

#endif