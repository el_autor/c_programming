#include <stdio.h>
#include <errno.h>
#include "struct.h"
#include "show.h"

#define FILE_OPEN_ERR 403
#define OK 0
#define ERROR 1

void show_record(const student_data *const data, const int index, FILE *const file)
{
    fprintf(file, "%s\n%s\n%s\n%d ", data[index].group, data[index].surname, data[index].birth, data[index].total_marks);
    
    for (int i = 0; i < data[index].total_marks; i++)
    {
        fprintf(file, "%lf ", data[index].marks[i]);
    }

    fprintf(file, "\n");
}

int file_output(const char *const filename, const student_data *const data, const int size)
{
    FILE *output_file = NULL;

    if ((output_file = fopen(filename, "w")))
    {
        for (int i = 0; i < size; i++)
        {
            show_record(data, i, output_file);
        }

        fclose(output_file);
    }
    else
    {
        errno = FILE_OPEN_ERR;
        return ERROR;
    }
    
    return OK;
}