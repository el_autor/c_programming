#ifndef _TESTING_H_

#define _TESTING_H

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "struct.h"
#include "math.h"
#include "input.h"
#include "remove.h"
#include "free.h"
#include "sort.h"
#include "errors.h"

#define TEST_FILE "testing.txt"
#define RESULT_FILE "result.txt"

#define INPUT_2_1 "ИУ7-31Б\n PES\n 2003.01.04\n 2\n 1.0 2.1\n ИУ7-31Б\n KOT\n 0700.02.01\n 1\n 1.1\n none\n"
#define RESULT_2_1 "ИУ7-31Б\n PES\n 2003.01.04\n 2\n 1.0 2.1\n none\n"

#define INPUT_2_2 "ИУ7-31Б\n PES\n 2003.01.04\n 2\n 1.0 2.1\n ИУ7-31Б\n KOT\n 0700.02.01\n 1\n 1.1\n ИУ7-31Б\n VASA\n 2000.01.02\n 2\n 4.1 2.2\n none\n"
#define RESULT_2_2 "ИУ7-31Б\n PES\n 2003.01.04\n 2\n 1.0 2.1\n none\n"

#define INPUT_2_3 "ИУ7-31Б\n PES\n 2003.01.04\n 2\n 1.0 2.1\n none\n"
#define RESULT_2_3 "ИУ7-31Б\n PES\n 2003.01.04\n 2\n 1.0 2.1\n none\n"

#define INPUT_3_1 "ИУ7-31Б\n PES\n 2003.01.04\n 2\n 7 8\n ИУ7-43343Б\n KOT\n 0700.02.01\n 1\n 6\n none"
#define RESULT_3_1 "ИУ7-31Б\n PES\n 2003.01.04\n 2\n 7 8\n ИУ7-43343Б\n KOT\n 0700.02.01\n 1\n 6\n none"

#define INPUT_3_2 "ИУ7-31Б\n PES\n 2003.01.04\n 2\n 2 5\n ИУ7-43343Б\n KOT\n 0700.02.01\n 1\n 6\n none"
#define RESULT_3_2 "ИУ7-31Б\n PES\n 2003.01.04\n 1\n 5\n ИУ7-43343Б\n KOT\n 0700.02.01\n 1\n 6\n none"

#define INPUT_3_3 "ИУ7-31Б\n PES\n 2003.01.04\n 2\n 2 7\n ИУ7-43343Б\n KOT\n 0700.02.01\n 2\n 6 3.99\n none"
#define RESULT_3_3 "ИУ7-31Б\n PES\n 2003.01.04\n 1\n 7\n ИУ7-43343Б\n KOT\n 0700.02.01\n 1\n 6\n none"

#define INPUT_4_1 "ИУ7-31Б\n PES\n 2003.01.04\n 2\n 2 7\n ИУ7-43343Б\n KOT\n 0700.02.01\n 2\n 6 3.99\n ИУ7-31Б\n VASA\n 2000.01.02\n 2\n 4.1 2.2\n none GROUP"
#define RESULT_4_1 "ИУ7-31Б\n PES\n 2003.01.04\n 2\n 2 7\n ИУ7-31Б\n VASA\n 2000.01.02\n 2\n 4.1 2.2\n ИУ7-43343Б\n KOT\n 0700.02.01\n 2\n 6 3.99\n none"

#define INPUT_4_2 "ИУ7-31Б\n PES\n 2003.01.04\n 2\n 2 7\n ИУ7-31Б\n VASA\n 2000.01.02\n 2\n 4.1 2.2\n  ИУ7-43343Б\n KOT\n 0700.02.01\n 2\n 6 3.99\n none SURNAME"
#define RESULT_4_2 "ИУ7-43343Б\n KOT\n 0700.02.01\n 2\n 6 3.99\n ИУ7-31Б\n PES\n 2003.01.04\n 2\n 2 7\n ИУ7-31Б\n VASA\n 2000.01.02\n 2\n 4.1 2.2\n none"

#define INPUT_4_3 "ИУ7-31Б\n PES\n 2003.01.04\n 2\n 2 7\n ИУ7-43343Б\n KOT\n 0700.02.01\n 2\n 6 3.99\n ИУ7-31Б\n VASA\n 2000.01.02\n 2\n 4.1 2.2\n none GRADE"
#define RESULT_4_3 "ИУ7-31Б\n VASA\n 2000.01.02\n 2\n 4.1 2.2\n ИУ7-31Б\n PES\n 2003.01.04\n 2\n 2 7\n ИУ7-43343Б\n KOT\n 0700.02.01\n 2\n 6 3.99\n none"

#define EPS 0.000001

#define SORT 3
#define MARKS 2
#define STUDENTS 1

#endif