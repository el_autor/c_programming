#include "remove.h"

int person_is_up_17(const char *const birth)
{
    int now_day = 1, now_month = 11, now_year = 2019;
    int birth_day = 0, birth_month = 0, birth_year = 0;

    sscanf(birth, "%d.%d.%d", &birth_year, &birth_month, &birth_day);

    if (birth_year > now_year - AGE)
    {
        return NO;
    }
    else if (birth_year == now_year - AGE && birth_month > now_month)
    {
        return NO;
    }
    else if (birth_year == now_year - AGE && birth_month == now_month && birth_day > now_day)
    {
        return NO;
    }

    return YES;
}

void move(student_data *const data, const int start_index, const int size)
{
    for (int i = start_index; i < size - 1; i++)
    {
        data[i] = data[i + 1];
    }
}

void move_marks(double *const marks, const int index, const int size)
{
    for (int i = index; i < size - 1; i++)
    {
        marks[i] = marks[i + 1];
    }
}

void del_marks_student(student_data *const record)
{
    for (int i = 0; i < record->total_marks; i++)
    {
        if (record->marks[i] < MARK_LIMIT)
        {
            move_marks(record->marks, i, record->total_marks);
            (record->total_marks)--;
            i--;
        }
    }
}

student_data *remove_students(student_data *const data, int *const size)
{
    for (int i = 0; i < *size; i++)
    {
        if (!(strcmp(data[i].group, DELETION_FACTOR)) && person_is_up_17(data[i].birth))
        {
            free_record(data + i);
            move(data, i, *size);
            i--;
            (*size)--;
        }
    }
    
    return data;
}

void remove_marks(student_data *const data, const int size)
{
    for (int i = 0; i < size; i++)
    {
        del_marks_student(data + i);
    }
}