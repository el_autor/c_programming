#ifndef _ERRORS_H_

#define _ERRORS_H_

#define OK 0
#define ERRORS_DETECTED 1
#define NO_DATA 1
#define MEM_ALLOC_ERR 400
#define INPUT_ERROR 401

// Ошибки input_data_testing
#define DATA_MISMATCH 402
#define FILE_NOT_FILLED 403

// Ошибки sort_testing
#define KEY_INPUT_ERROR 404

#endif