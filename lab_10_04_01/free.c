#include "free.h"
#include <stdlib.h>

void free_record(student_data *const data)
{
    free(data->group);
    free(data->surname);
    free(data->birth);
    free(data->marks);
}

void free_all(student_data *const data, const int size)
{
    for (int i = 0; i < size; i++)
    {
        free_record(data + i);
    }

    free(data);
}