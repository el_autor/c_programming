#include "input.h"

int surname_is_valid(const char *const buffer)
{
    size_t size = strlen(buffer);

    for (size_t i = 0; i < size; i++)
    {
        if (buffer[i] < 65 || buffer[i] > 122)
        {
            return ERRORS_DETECTED;
        }
        else if (buffer[i] > 90 && buffer[i] < 97)
        {
            return ERRORS_DETECTED;
        }
    }

    return OK;
}

int is_leap_year(const int year)
{
    if (year % 400 == 0)
    {
        return OK;
    }
    else if (year % 100 == 0)
    {
        return ERRORS_DETECTED;
    }
    else if (year % 4 == 0)
    {
        return OK;
    }

    return ERRORS_DETECTED;
}

int true_format(const int day, const int month, const int year)
{
    int month_length[MONTH_TOTAL] = { 31, 0, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

    if (day <= 0 || day > 31 || month <= 0 || month > 12 || year <= 0 || year > CURRENT_YEAR)
    {
        return ERRORS_DETECTED;
    }

    if (month == 2)
    {
        if (is_leap_year(year) == OK)
        {
            if (day > 29)
            {
                return ERRORS_DETECTED;
            }
        }
        else if (day > 28)
        {
            return ERRORS_DETECTED;
        }
    }
    else if (day > month_length[month - 1])
    {
        return ERRORS_DETECTED;
    }    

    return OK;
}

int get_grade(const int grade)
{
    int start = 1;

    for (int i = 0; i < grade; i++)
    {
        start *= 10;
    }

    return start;
}

int get_year(FILE *const stream, char *const buffer)
{
    char symb = 0;
    int year = 0;

    for (int i = 0; i < YEAR_SIZE; i++)
    {
        if (fscanf(stream, "%c", &symb) != 1)
        {
            return WRONG_DATE;
        }
        else if (symb < 48 || symb > 57)
        {
            return WRONG_DATE;
        }
        
        buffer[i] = symb;
        year += get_grade(YEAR_SIZE - 1 - i) * (symb - 48); 
    }

    return year;
}

int get_month(FILE *const stream, char *const buffer)
{
    char symb = 0;
    int month = 0;

    for (int i = 0; i < MONTH_SIZE; i++)
    {
        if (fscanf(stream, "%c", &symb) != 1)
        {
            return WRONG_DATE;
        }
        else if (symb < 48 || symb > 57)
        {
            return WRONG_DATE;
        }

        buffer[YEAR_SIZE + 1 + i] = symb;
        month += get_grade(MONTH_SIZE - 1 - i) * (symb - 48);
    }

    return month;
}

int get_day(FILE *const stream, char *const buffer)
{
    char symb = 0;
    int day = 0;

    for (int i = 0; i < DAY_SIZE; i++)
    {
        if (fscanf(stream, "%c", &symb) != 1)
        {
            return WRONG_DATE;
        }
        else if (symb < 48 || symb > 57)
        {
            return WRONG_DATE;
        }

        day += get_grade(DAY_SIZE - 1 - i) * (symb - 48);
        buffer[YEAR_SIZE + 1 + MONTH_SIZE + 1 + i] = symb;
    }

    return day;
}

int date_is_valid(FILE *const stream, char *const buffer)
{
    int day = 0, month = 0, year = 0;
    char symb = 0;

    //printf("here1\n");

    while (fscanf(stream, "%c", &symb) == 1 && (symb == ' ' || symb == '\n'));
    //printf("here2\n");

    if (errno != OK)
    {
        return ERRORS_DETECTED;
    }

    ungetc(symb, stream);

    if ((year = get_year(stream, buffer)) == WRONG_DATE)
    {
        return ERRORS_DETECTED;
    }

    //printf("here3\n");

    if (fscanf(stream, "%c", &symb) != 1 || symb != '.')
    {
        return ERRORS_DETECTED;
    }

    buffer[YEAR_SIZE] = '.'; 

    if ((month = get_month(stream, buffer)) == WRONG_DATE)
    {
        return ERRORS_DETECTED;
    }

    //printf("here4\n");

    if (fscanf(stream, "%c", &symb) != 1 || symb != '.')
    {
        return ERRORS_DETECTED;
    }

    buffer[YEAR_SIZE + 1 + MONTH_SIZE] = '.';

    if ((day = get_day(stream, buffer)) == WRONG_DATE)
    {
        return ERRORS_DETECTED;
    }

    //printf("here5\n");

    if (fscanf(stream, "%c", &symb) != 1)
    {
        return ERRORS_DETECTED;
    }
    else if (symb != ' ' && symb != '\n')
    {
        return ERRORS_DETECTED;
    }

    buffer[YEAR_SIZE + 1 + MONTH_SIZE + 1 + DAY_SIZE] = '\0';
    //printf("here6\n");

    //printf("%d %d %d\n", year, month, day);

    if (true_format(day, month, year) == OK)
    {
        //printf("here7\n");
        return OK;
    }

    return ERRORS_DETECTED;
}

void init_null(student_data *const data)
{
    data->birth = NULL;
    data->group = NULL;
    data->surname = NULL;
    data->total_marks = 0;
    data->marks = NULL;
}

student_data *add_memory(student_data *old_memory, int *const current_size)
{
    student_data *local = realloc(old_memory, sizeof(student_data) * (*current_size) * 2);

    if (!local)
    {
        return NULL;
    }
    
    // Инициализируем выделенныю память
    for (int i = *current_size; i < (*current_size) * 2; i++)
    {
        init_null(local + i);
    }

    *current_size *= 2;

    return local;
}

student_data *input_data(FILE *const stream, int *const data_size)
{
    student_data *local_storage = NULL;
    *data_size = 0;
    int alloced_memory = 0;
    char buffer_string[MAX_SIZ];
    int string_size = 0, total_marks = 0;

    // Если места не будет хватать, то аллоцируемая память увеличивается по степеням двойки
    if (!(local_storage = malloc(sizeof(student_data) * 1)))
    {
        errno = MEM_ALLOC_ERR;
        return NULL;
    }

    init_null(local_storage);

    alloced_memory = 1;

    while (CONTIINUM)
    {   
        if (fscanf(stream, "%s", buffer_string) != 1)
        {
            free_all(local_storage, *data_size);
            errno = INPUT_ERROR;
            return NULL;
        }

        if (!strcmp(buffer_string, STOP_FACTOR))
        {   
            return local_storage;
        }

        if (*data_size == alloced_memory)
        {
            student_data *checkout = NULL;

            if ((checkout = add_memory(local_storage, &alloced_memory)) == NULL)
            {
                free_all(local_storage, *data_size);
                errno = MEM_ALLOC_ERR;

                return NULL;
            }

            local_storage = checkout;
        }

        string_size = strlen(buffer_string);
        local_storage[*data_size].group = malloc(sizeof(char) * string_size + 1);
        strcpy(local_storage[*data_size].group, buffer_string);

        if (fscanf(stream, "%s", buffer_string) != 1 || surname_is_valid(buffer_string) != OK)
        {
            errno = INPUT_ERROR;

            free(local_storage[*data_size].group);
            free_all(local_storage, *data_size);

            return NULL;
        }
        
        string_size = strlen(buffer_string);
        local_storage[*data_size].surname = malloc(sizeof(char) * string_size + 1);
        strcpy(local_storage[*data_size].surname, buffer_string);

        if (date_is_valid(stream, buffer_string) != OK)
        {
            errno = INPUT_ERROR;

            free(local_storage[*data_size].group);
            free(local_storage[*data_size].surname);
            free_all(local_storage, *data_size);

            return NULL;
        }

        string_size = strlen(buffer_string);
        local_storage[*data_size].birth = malloc(sizeof(char) * string_size + 1);
        strcpy(local_storage[*data_size].birth, buffer_string);

        if (fscanf(stream, "%d", &total_marks) == !1 || total_marks < 1)
        {
            errno = INPUT_ERROR;

            free(local_storage[*data_size].group);
            free(local_storage[*data_size].surname);
            free(local_storage[*data_size].birth);
            free_all(local_storage, *data_size);

            return NULL;
        }

        local_storage[*data_size].total_marks = total_marks;
        local_storage[*data_size].marks = malloc(sizeof(double) * total_marks);

        for (int i = 0; i < total_marks; i++)
        {
            if (fscanf(stream, "%lf", local_storage[*data_size].marks + i) != 1 || (local_storage[*data_size].marks[i]) < 0)
            {
                errno = INPUT_ERROR;

                free(local_storage[*data_size].group);
                free(local_storage[*data_size].surname);
                free(local_storage[*data_size].birth);
                free(local_storage[*data_size].marks);
                free_all(local_storage, *data_size);

                return NULL;
            }
        }

        (*data_size)++;
    }
}