#ifndef _INPUT_H_

#define _INPUT_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "struct.h"
#include "errno.h"
#include "input.h"
#include "free.h"
#include "errors.h"

#define STOP_FACTOR "none"
#define MAX_SIZ 256
#define CONTIINUM 1
#define EPS 0.000001

#define YEAR_SIZE 4
#define MONTH_SIZE 2
#define DAY_SIZE 2 
#define CURRENT_YEAR 2019
#define MONTH_TOTAL 12

#define WRONG_DATE -1

student_data *input_data(FILE *const stream, int *const data_size);

#endif