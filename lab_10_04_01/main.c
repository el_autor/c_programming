#include "app.h"

#define OUTPUT_FILE "students.txt"

int main()
{
    student_data *data = NULL;
    int data_size = 0;

    if (!(data = input_data(stdin, &data_size)))
    {
        return errno;
    }

    if (!data_size)
    {
        free(data);
        return NO_DATA;
    }

    if (!(data = remove_students(data, &data_size)))
    {
        free_all(data, data_size);
        return errno;
    }

    if (!data_size)
    {
        free(data);
        return NO_DATA;
    }

    remove_marks(data, data_size);

    if (sort(stdin, data, data_size) != OK)
    {
        // Память при ошибке освобоэдается внутри sort 
        return errno;
    }

    if (file_output(OUTPUT_FILE, data, data_size) != OK)
    {
        free_all(data, data_size);
        return errno;
    }

    free_all(data, data_size);
    
    return OK;
}