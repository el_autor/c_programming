#include <stdio.h>
#include <math.h>

#define CORRECT_SCANF 2
#define ODZ 1
#define CORRECT_EXIT 0
#define WRONG_INPUT 1
#define ONE 1
#define TWO 2
#define ZERO 0

float count_sum(float x, float eps, float real_value)
{
    float close_value = x;
    float part = x;
    int i = 1;
    while (fabs(real_value - close_value) > eps)
    {
        part *= (x * x * i * i) / ((i + ONE) * (i + TWO));
        close_value += part;
        i += 2;
    }
    return close_value;
}

int main()
{
    float x, eps;
    float real_value, close_value;
    if (scanf("%f %f", &x, &eps) == CORRECT_SCANF && fabs(x) < ODZ && eps > ZERO && eps <= ONE)
    {
        real_value = asin(x);
        close_value = count_sum(x, eps, real_value);
        printf("%f %f ", close_value, real_value);
        printf("%f ", fabs(close_value - real_value));
        printf("%f", fabs((close_value - real_value) / real_value));
        return CORRECT_EXIT;
    }
    return WRONG_INPUT;
}
