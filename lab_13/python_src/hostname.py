import ctypes

if __name__ == "__main__":

    # int _hostname_(const char *const source, char *const new_hostname);

    lib = ctypes.CDLL('../build/hostname.so')

    buffer = "cucuy"
    hostname = "some_trash"

    src_buffer = ctypes.create_string_buffer(str.encode(buffer))
    src_new_hostname = ctypes.create_string_buffer(str.encode(hostname))

    # Test 1
    if (lib._hostname_(src_buffer, src_new_hostname)):
        print("TEST 1 FAILED")
        print("Can't change hostname to", buffer, '\'', sep = '')
    else:
        print("TEST 1 PASSED")
        print("Hostname changed to '", buffer, '\'', sep = '')

    # Test 2
    if (lib._hostname_(ctypes.c_char_p(0), src_new_hostname)):
        print("TEST 2 FAILED")
        print("Can't get hostname")
    else:
        if (buffer == bytes.decode(src_new_hostname.value)):
            print("TEST 2 PASSED")
            print("Your current hostname '", bytes.decode(src_new_hostname.value), '\'', sep = '')
        else:
            print("TEST 2 FAILED")
            print("Expected '", buffer, "' ,but '", bytes.decode(src_new_hostname.value), "' got", sep = '')
    