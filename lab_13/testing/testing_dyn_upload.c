#include "../headers/testing_dyn_upload.h"

int is_equal(const char *const first, const char *const second)
{
    if (strcmp(first, second))
    {
        printf("Expected hostname notorious, but %s is current\n", first);
        return ERRORS_DETECTED;
    }

    return OK;
}

int getname_test(int (*hostname_util)(const char *const, char *const), char *const hostname)
{
    if ((*hostname_util)(NULL, hostname) != OK)
    {
        return ERRORS_DETECTED;
    }

    if (is_equal(hostname, "notorious") != OK)
    {
        return ERRORS_DETECTED;
    }

    return OK;
}

int setname_test(int (*hostname_util)(const char *const, char *const), char *const hostname)
{
    if ((*hostname_util)("notorious", hostname) != OK)
    {
        return ERRORS_DETECTED;
    }

    if (is_equal(hostname, "notorious") != OK)
    {
        return ERRORS_DETECTED;
    }

    return OK;
}


int start_dash(int (*hostname_util)(const char *const, char *const), char *const hostname)
{
    if ((*hostname_util)("-notorious", hostname) == OK)
    {
        return ERRORS_DETECTED;
    }

    return OK;
}

int end_dash(int (*hostname_util)(const char *const, char *const), char *const hostname)
{
    if ((*hostname_util)("notorious-", hostname) == OK)
    {
        return ERRORS_DETECTED;
    }

    return OK;
}

int random_error(int (*hostname_util)(const char *const, char *const), char *const hostname)
{
    if ((*hostname_util)("notor...ious", hostname) == OK)
    {
        return ERRORS_DETECTED;
    }

    return OK;
}


int analize_result(FILE *const stream, const int code)
{
    if (!code)
    {
        fprintf(stdout, "TEST PASSED!\n");
    }
    else
    {
        fprintf(stdout, "TEST FAILED!\n");
    }

    return code;
}

int main()
{
    void *lib = NULL;
    int (*hostname_util)(const char *const, char *const) = NULL;
    
    if (!(lib = dlopen("./libhostname.so", RTLD_LAZY)))
    {
        printf("lib not loaded\n");
        return ERRORS_DETECTED;
    }

    if (!(hostname_util = dlsym(lib, "_hostname_")))
    {
        printf("can't get function\n");
        return ERRORS_DETECTED;
    }

    int err_counter = 0;
    char current_hostname[MAX_HOSTNAME_LENGTH], testing_hostname[MAX_HOSTNAME_LENGTH];
    gethostname(current_hostname, MAX_HOSTNAME_LENGTH);    

    fprintf(stdout, "1) Testing change hostname\n");
    err_counter += analize_result(stdout, setname_test(hostname_util, testing_hostname));

    fprintf(stdout, "2) Testing get hostname\n");
    err_counter += analize_result(stdout, getname_test(hostname_util, testing_hostname));
    
    fprintf(stdout, "3) Testing invalid hostname set(name start with '-')\n");
    err_counter += analize_result(stdout, start_dash(hostname_util, testing_hostname));

    fprintf(stdout, "4) Testing invalid hostname set(name end with '-')\n");
    err_counter += analize_result(stdout, end_dash(hostname_util, testing_hostname));
    
    fprintf(stdout, "5) Testing invalid hostname set(random)\n");
    err_counter += analize_result(stdout, random_error(hostname_util, testing_hostname));

    sethostname(current_hostname, strlen(current_hostname));

    if (err_counter)
    {
        printf("Testing failed with %d errors\n", err_counter);
        return ERRORS_DETECTED;
    }

    printf("Testing passed!\n");
    printf("Your standart hostname \"%s\" recovered", current_hostname);
    return SUCCESS;
}