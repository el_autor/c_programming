#include "../headers/testing.h"

int is_equal(const char *const first, char *const second)
{
    if (strcmp(first, second))
    {
        printf("Expected hostname notorious, but %s is current\n", first);
        return ERRORS_DETECTED;
    }

    return OK;
}

int getname_test(char *const hostname)
{
    if (_hostname_(NULL, hostname) != OK)
    {
        return ERRORS_DETECTED;
    }

    if (is_equal(hostname, "notorious") != OK)
    {
        return ERRORS_DETECTED;
    }

    return OK;
}

int setname_test(char *const hostname)
{
    if (_hostname_("notorious", hostname) != OK)
    {
        return ERRORS_DETECTED;
    }

    if (is_equal(hostname, "notorious") != OK)
    {
        return ERRORS_DETECTED;
    }

    return OK;
}

int start_dash(char *const hostname)
{
    if (_hostname_("-notorious", hostname) == OK)
    {
        return ERRORS_DETECTED;
    }

    return OK;
}

int end_dash(char *const hostname)
{
    if (_hostname_("notorious-", hostname) == OK)
    {
        return ERRORS_DETECTED;
    }

    return OK;
}

int random_error(char *const hostname)
{
    if (_hostname_("notor...ious", hostname) == OK)
    {
        return ERRORS_DETECTED;
    }

    return OK;
}

int analize_result(FILE *const stream, const int code)
{
    if (!code)
    {
        fprintf(stdout, "TEST PASSED!\n");
    }
    else
    {
        fprintf(stdout, "TEST FAILED!\n");
    }

    return code;
}

int main()
{
    int err_counter = 0;
    char current_hostname[MAX_HOSTNAME_LENGTH], test_hostname[MAX_HOSTNAME_LENGTH];
    gethostname(current_hostname, MAX_HOSTNAME_LENGTH);    

    fprintf(stdout, "1) Testing set hostname\n");
    err_counter += analize_result(stdout, setname_test(test_hostname));

    fprintf(stdout, "2) Testing get hostname\n");
    err_counter += analize_result(stdout, getname_test(test_hostname));

    fprintf(stdout, "3) Testing invalid hostname set(name start with '-')\n");
    err_counter += analize_result(stdout, start_dash(test_hostname));

    fprintf(stdout, "4) Testing invalid hostname set(name end with '-')\n");
    err_counter += analize_result(stdout, end_dash(test_hostname));
    
    fprintf(stdout, "5) Testing invalid hostname set(random)\n");
    err_counter += analize_result(stdout, random_error(test_hostname));

    sethostname(current_hostname, strlen(current_hostname));

    if (err_counter)
    {
        printf("Testing failed with %d errors\n", err_counter);
        return ERRORS_DETECTED;
    }

    printf("Testing passed!\n");
    printf("Your standart hostname \"%s\" recovered", current_hostname);
    return SUCCESS;
}