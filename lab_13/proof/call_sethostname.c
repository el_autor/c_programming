#include <stdio.h>
#include <dlfcn.h>

#define OK 0
#define ERRORS_DETECTED 1

int main()
{
    void *lib = NULL;
    int (*check_gethostname)(char *, size_t) = NULL;
    
    if (!(lib = dlopen("/lib/x86_64-linux-gnu/libc.so.6", RTLD_LAZY)))
    {
        printf("lib not loaded\n");
        return ERRORS_DETECTED;
    }

    if (!(check_gethostname = dlsym(lib, "gethostname")))
    {
        printf("can't get function\n");
        return ERRORS_DETECTED;
    }
    
    printf("Function successfully founded!\n");
    return OK;
}
