#ifndef _MY_HOSTNAME_H_

#define _MY_HOSTNAME_H_

#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#define OK 0
#define ERROR 1
#define WRONG_FORMAT 1
#define NAME_NOT_SET 2
#define NAME_NOT_GET 3
#define EMPTY_HOSTNAME 4

#define MAX_HOSTNAME_LENGTH 30

int _hostname_(const char *const buffer, char *const hostname);

#endif 