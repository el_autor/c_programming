#ifndef _TESTING_H_

#define _TESTING_H_

#include "../headers/my_hostname.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SUCCESS 0
#define ERRORS_DETECTED 1

#define WORDS_SIZE 30

#endif