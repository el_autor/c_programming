#include <stdio.h>
#include <math.h>

#define SQUARE 2
#define CORRECT 0

int main()
{
    float x1, y1, x2, y2, x3, y3; // координаты точек
    float a, b, c; // периметр и стороны треугольника
    puts("Input coordinates with ""Enter"":");
    scanf("%f%f %f%f %f%f", &x1, &y1, &x2, &y2, &x3, &y3);
    a = sqrt(pow(x1 - x2, SQUARE) + pow(y1 - y2, SQUARE));
    b = sqrt(pow(x2 - x3, SQUARE) + pow(y2 - y3, SQUARE));
    c = sqrt(pow(x3 - x1, SQUARE) + pow(y3 - y1, SQUARE));
    if (a == b + c || b == c + a || c == a + b)
        puts("Triangle not exist!");
    else
        printf("Perimeter - %.5f", a + b + c);
    return CORRECT;
}
