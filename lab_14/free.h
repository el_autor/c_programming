#ifndef _FREE_H_

#define _FREE_H_

#include "struct.h"
void free_all(student_data *const data, const int size);
void free_record(student_data *const data);

#endif