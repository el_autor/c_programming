#ifndef _SORT_H_

#define _SORT_H_

#include <stdio.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include "sort.h"
#include "input.h"
#include "struct.h"
#include "errors.h"

#define GROUP 1
#define SURNAME 2
#define GRADE 3

int sort(const int key, student_data *const data, const int size);

#endif
