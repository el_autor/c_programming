#include "sort.h"

struct node
{
    student_data record;
    struct node *left;
    struct node *right;
};

int group_comparator(student_data *const elem_1, student_data *const elem_2)
{
    return strcmp(elem_1->group, elem_2->group);
}

int surname_comparator(student_data *const elem_1, student_data *const elem_2)
{
    return strcmp(elem_1->surname, elem_2->surname);
}

double calc_middle(student_data *const record)
{
    double middle = 0;

    for (int i = 0; i < record->total_marks; i++)
    {
        middle += record->marks[i];
    }

    return middle / record->total_marks;
}

int grade_comparator(student_data *const elem_1, student_data *const elem_2)
{
    return (calc_middle(elem_1) >= calc_middle(elem_2)) ? 1 : -1;
}

struct node *addnode(struct node *root, student_data *record, int (*comparator)(student_data *, student_data *))
{
    int compare = 0;

    if (root == NULL)
    {
        root = malloc(sizeof(struct node));
        root->record = *record;
        root->left = root->right = NULL;
    }
    else if ((compare = (*comparator)(record, &(root->record))) >= 0)
    {
        root->right = addnode(root->right, record, comparator);
    } 
    else
    {
        root->left = addnode(root->left, record, comparator);
    }
    
    return root;
}

void fill_data(int *const current_elem, student_data *data, struct node *root)
{
    if (root != NULL)
    {
        fill_data(current_elem, data, root->left);
        data[(*current_elem)++] = root->record;
        fill_data(current_elem, data, root->right);
        free(root); 
    }
}

void binary_tree_sort(student_data *const data, const int size, int (*comparator)(student_data *, student_data*))
{
    struct node *root = NULL;
    int current_size = 0;

    for (int i = 0; i < size; i++)
    {
        root = addnode(root, data + i, comparator);
    }

    fill_data(&current_size, data, root);
}


int sort(const int key, student_data *const data, const int size)
{
    if (key == GROUP)
    {
        binary_tree_sort(data, size, group_comparator);
    }
    else if (key == SURNAME)
    {
        binary_tree_sort(data, size, surname_comparator);   
    }
    else if (key == GRADE)
    {
        binary_tree_sort(data, size, grade_comparator);
    }   

    return OK;
}