#ifndef _ERRORS_H_

#define _ERRORS_H_

#define OK 0
#define ERRORS_DETECTED 1
#define NO_DATA 1
#define MEM_ALLOC_ERR 200
#define INPUT_ERROR 201
#define LIBUI_NOT_INIT 202

// Ошибки input_data_testing
#define DATA_MISMATCH 202
#define FILE_NOT_FILLED 203

// Ошибки sort_testing
#define KEY_INPUT_ERROR 204

#endif