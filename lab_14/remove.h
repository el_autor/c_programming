#ifndef _REMOVE_H_

#define _REMOVE_H_

#include <string.h>
#include <stdio.h>
#include <errno.h>
#include "remove.h"
#include "free.h"
#include "struct.h"

student_data *remove_students(student_data *const data, int *const size);
void remove_marks(student_data *const data, const int size);

#define DELETION_FACTOR "ИУ7-31Б"
#define MARK_LIMIT 4.0

#define AGE 17
#define YES 1
#define NO 0

#endif