#include "app.h"

uiInitOptions options;
const char *err;
uiBox *vBox, *vBoxResult, *hBoxUp, *hBoxDown;
uiButton *addData, *delMarks, *delStudents, *sortBy, *showButton;
uiTab *mainTab;
uiRadioButtons *radioButtons;
uiWindow *mainWindow;
uiMultilineEntry *multiEntry, *readMultiEntry;
student_data *data = NULL;
int data_size = 0;

void make_sort(uiButton *button, void *but_data)
{
    if (!data)
    {
        uiMsgBoxError(mainWindow, "Error", "No data available!");
        return;         
    }

    sort(uiRadioButtonsSelected(radioButtons) + 1, data, data_size);
}

void delete_students(uiButton *button, void *but_data)
{
    if (!data)
    {
        uiMsgBoxError(mainWindow, "Error", "No data available!");
        return;        
    }

    data = remove_students(data, &data_size);
}

void delete_marks(uiButton *button, void *but_data)
{
    if (!data)
    {
        uiMsgBoxError(mainWindow, "Error", "No data available!");
        return;        
    }

    remove_marks(data, data_size);    
}

void create_data(uiButton *button, void *but_data)
{
    FILE *stream = fopen(INPUT_FILE, "w+");
    char *text = uiMultilineEntryText(multiEntry);

    if (data)
    {
        free_all(data, data_size);
        data = NULL;
    }

    fprintf(stream, "%s", text);
    fseek(stream, SEEK_SET, 0);

    errno = 0;
    data = input_data(stream, &data_size);
    fclose(stream);

    if (errno)
    { 
        uiMsgBoxError(mainWindow, "Error", "Wrong input!");
        return;
    }
}

void show_data(uiButton *button, void *but_data)
{
    FILE *stream = NULL;
    char line[MAX_SIZ];
    char output[MAX_SIZ * 5];

    if (!data)
    {
        uiMsgBoxError(mainWindow, "Error", "No data available!");
        return;
    }

    file_output(OUTPUT_FILE, data, data_size);
    stream = fopen(OUTPUT_FILE, "r");
    output[0] = '\0';

    while (fgets(line, MAX_SIZ, stream))
    {
        strcat(output, line);
    }

    uiMultilineEntrySetText(readMultiEntry, output);
    fclose(stream); 
}

int onClosing(uiWindow *w, void *arg_data)
{
    free_all(data, data_size);
    uiQuit();
    return EXIT_SUCCESS;
}

int onShouldQuit(void *arg_data)
{
    uiWindow *mainWindow = uiWindow(arg_data);

    free_all(data, data_size);
    uiControlDestroy(uiControl(mainWindow));
    return errno;
}

int main()
{
    memset(&options, 0, sizeof(uiInitOptions));
    err = uiInit(&options);

    if (err)
    {
        uiFreeInitError(err);
        return LIBUI_NOT_INIT;
    }

    mainWindow = uiNewWindow("Bauman students database", 510, 350, 1);
    uiWindowOnClosing(mainWindow, onClosing, NULL);
    uiOnShouldQuit(onShouldQuit, mainWindow);

    vBox = uiNewVerticalBox();
    uiBoxSetPadded(vBox, 1);

    hBoxUp = uiNewHorizontalBox();
    uiBoxSetPadded(hBoxUp, 1);
    uiBoxAppend(vBox, uiControl(hBoxUp), 0);

    addData = uiNewButton("Apply data");
    uiButtonOnClicked(addData, create_data, NULL);
    delMarks = uiNewButton("Delete marks");
    uiButtonOnClicked(delMarks, delete_marks, NULL);
    delStudents = uiNewButton("Delete students");
    uiButtonOnClicked(delStudents, delete_students, NULL);
    sortBy = uiNewButton("Sort");
    uiButtonOnClicked(sortBy, make_sort, NULL);
    showButton = uiNewButton("Show results");
    uiButtonOnClicked(showButton, show_data, NULL);
    radioButtons = uiNewRadioButtons();

    uiRadioButtonsAppend(radioButtons, "Group");
    uiRadioButtonsAppend(radioButtons, "Surname");
    uiRadioButtonsAppend(radioButtons, "Grade");

    uiBoxAppend(hBoxUp, uiControl(addData), 0);
    uiBoxAppend(hBoxUp, uiControl(delMarks), 0);
    uiBoxAppend(hBoxUp, uiControl(delStudents), 0);
    uiBoxAppend(hBoxUp, uiControl(sortBy), 0);
    uiBoxAppend(hBoxUp, uiControl(radioButtons), 0);
    uiBoxAppend(hBoxUp, uiControl(showButton), 0);

    uiBoxAppend(vBox, uiControl(uiNewLabel("")), 0);
    uiBoxAppend(vBox, uiControl(uiNewHorizontalSeparator()), 0);
    uiBoxAppend(vBox, uiControl(uiNewLabel("Data input")), 0);

    uiWindowSetChild(mainWindow, uiControl(vBox));

    mainTab = uiNewTab();
    uiWindowSetChild(mainWindow, uiControl(mainTab));
    uiWindowSetMargined(mainWindow, 1);
    uiTabAppend(mainTab, "Program options", uiControl(vBox));
    uiTabSetMargined(mainTab, 0, 1);

    hBoxDown = uiNewHorizontalBox();
    uiBoxSetPadded(hBoxDown, 1);
    uiBoxAppend(vBox, uiControl(hBoxDown), 1);
    multiEntry = uiNewMultilineEntry();

    uiBoxAppend(hBoxDown, uiControl(multiEntry), 1);

    vBoxResult = uiNewVerticalBox();
    uiBoxSetPadded(vBoxResult, 1);

    readMultiEntry = uiNewMultilineEntry();
    uiMultilineEntrySetReadOnly(readMultiEntry, 1);
    uiBoxAppend(vBoxResult, uiControl(readMultiEntry), 1);

    uiTabAppend(mainTab, "View results", uiControl(vBoxResult));
    uiTabSetMargined(mainTab, 1, 1);
    
    uiControlShow(uiControl(mainWindow));
    uiMain();
    return EXIT_SUCCESS;    
}